#include "AppServer.h"
#include <zmqpp/zmqpp.hpp>
#include "ControlPacket.h"
#include "AcqServer.h"

#define CTRL_MODULE_PORT 4831


using namespace std;


int main(int argc, char *argv[])
{
    // temporary trick to avoid warning
    (void)argc;
    (void)argv;

    // connect to Control Module
    zmqpp::context context;
    zmqpp::socket socket(context, zmqpp::socket_type::request);
    connect_to_server(socket, CTRL_MODULE_PORT);

    // initialize a core
    ctrl_pkt pkt1 = { "cmd_init_core", "../lib/Core.so.signed", "1"};
    zmqpp::message *msg1 = pack_data(pkt1);
    socket.send(*msg1);
    delete(msg1);
    // check the return code
    ctrl_pkt reply1;
    acquire_pkt(socket, reply1);
    PRINTCPP(LOG_LEVEL_DESCRIBE, "Return code from control module : " << reply1.cmd  << ", " << reply1.arg1 << endl);

    // initialize a first data task
/*    ctrl_pkt pkt2 = { "cmd_init_data_task", "../lib/DT1.so.signed", "0"}";
    zmqpp::message *msg2 = pack_data(pkt2);
    socket.send(*msg2);
    delete(msg2);

    // check the return code
    ctrl_pkt reply2;
    acquire_pkt(socket, reply2);
    cout << "Return code from control module : " << reply2.cmd  << ", " << reply2.arg1 << endl;

    // initialize a second data task
    // TODO : fix unnecessary int member of ctrl_pkt
    ctrl_pkt pkt3 = { "cmd_init_data_task", "../lib/DT2.so.signed", "0"};
    zmqpp::message *msg3 = pack_data(pkt3);
    socket.send(*msg3);
    delete(msg3);

    // check the return code
    ctrl_pkt reply3;
    acquire_pkt(socket, reply3);
    cout << "Return code from control module : " << reply3.cmd  << ", " << reply3.arg1 << endl;

    // initialize a data collector
    // TODO : fix unnecessary int member of ctrl_pkt
    ctrl_pkt pkt4 = { "cmd_init_data_collector", "../lib/DC1.so.signed", "0"};
    zmqpp::message *msg4 = pack_data(pkt4);
    socket.send(*msg4);
    delete(msg4);

    // check the return code
    ctrl_pkt reply4;
    acquire_pkt(socket, reply4);
    cout << "Return code from control module : " << reply4.cmd  << ", " << reply4.arg1 << endl;
    */
    // TODO :  destroy the enclave

	socket.close();
    
    PRINTCPP(LOG_LEVEL_DESCRIBE, "AppServer successfully returned." << endl);
    return 0;
}

