#include "DC1.h"
#include "DataCollectorCom.h"

#include "request.h"
#include "httprequestparser.h"
#include "response.h"
#include "httpresponseparser.h"
#include "urlparser.h"
#include "common.h"


#define MAX_COLLECT_PKTS 10

using httpparser::HttpRequestParser;
using httpparser::Request;
using httpparser::HttpResponseParser;
using httpparser::Response;


/* http lib */

const char *http_get()
{
    Request req = RequestDsl()
        .method("GET")
        .uri("index.html")
        .version(1, 1)
        .header("Authorization", "Basic ZmxvOjE5ODg=");

    std::string res = req.inspect();

    return strdup(res.c_str());
}

const char *http_post(const char *dataname)
{
    char data[64];
    sprintf(data, "{\"file\":\"testing3.csv\",\"data\":\"%s\"}", dataname);
    char len[4];
    sprintf(len, "%zu", strlen(data));
    Request req = RequestDsl()
            .method("POST")
            .uri("/")
            .version(1, 1)
            .header("Content-Length", len)
            .content(data);

    std::string res = req.inspect();

    return strdup(res.c_str());
}

std::string get_content_len(uint8_t *data)
{
    std::string str = std::string(reinterpret_cast<char *>(data));
    size_t first = str.find_last_of("Content-Length: ");
    size_t last = str.find('\n');
    std::string substr = "";

    if (first != std::string::npos && last != std::string::npos)
    {
        substr = str.substr(first, last-first);
        substr.erase(std::remove(substr.begin(), substr.end(), ' '), substr.end());
    }
    else
    {
        PRINT(LOG_LEVEL_ERRORS, "No content length found\n");
    }

    return substr;
}


/* converter */

// TODO : to refactorize the parsing of string
funcres_t *convert_data(int nb_pkts, uint8_t **data)
{
    // read and convert data
    int i, j, idx = 0;
    std::vector<std::vector<std::string> > sdata(20);

    std::string s = "";
    for (i = 1; i < nb_pkts; i++)
        s += std::string(reinterpret_cast<char *>(data[i]));
    PRINT(LOG_LEVEL_DEBUG, "Full data to convert %s\n", s.c_str());
    size_t pos1 = 0, pos2 = 0;
    std::string token1, token2;
    std::string delim1 = "\n\n", delim2 = ",";
    while ((pos1 = s.find(delim1)) != std::string::npos) {
        token1 = s.substr(0, pos1);
        // printf("debug : token : %s\n", token1.c_str());
        s.erase(0, pos1 + delim1.length());
        while ((pos2 = token1.find(delim2)) != std::string::npos) 
        {
            token2 = token1.substr(0, pos2);
            // printf("debug : token2 : %s\n", token2.c_str());
            token1.erase(0, pos2 + delim2.length());
            sdata[idx].push_back(token2);
        }

        sdata[idx].push_back(token1);
        idx++;
    }
    while ((pos2 = s.find(delim2)) != std::string::npos) 
    {
        token2 = s.substr(0, pos2);
        s.erase(0, pos2 + delim2.length());
        sdata[idx].push_back(token2);
    }
    sdata[idx].push_back(s);
    idx++;

    // debug
    int nb_columns = sdata[0].size();
    int nb_rows = idx;
    PRINT(LOG_LEVEL_DEBUG, "nb_columns = %d, nb_rows = %d\n", nb_columns, nb_rows);
    // for (auto &v : sdata)
    // {
    //     printf("Data converted : \n");
    //     for (auto &val : v)
    //         printf("%s ", val.c_str());
    //     printf("\n");
    // }

    // save converted data in funcres_t
    funcres_t *res = (funcres_t *)malloc(sizeof(funcres_t));
    res->nb_columns = nb_columns;
    res->nb_rows = nb_rows;
    res->a = (int *)malloc(sizeof(int) * nb_rows);
    res->b = (int *)malloc(sizeof(int) * nb_rows);
    res->c = (float *)malloc(sizeof(float) * nb_rows);

    for (i = 0; i < nb_rows; i++)
    {
        for (j = 0; j < nb_columns; j++)
        {
            if (j == 0)
                res->a[i] = std::stoi(sdata[i][j]);
            else if (j == 1)
                res->b[i] = std::stoi(sdata[i][j]);
            else
                res->c[i] = std::stof(sdata[i][j]);
        }
    }
    // debug
    // printf("Converted data : \n");
    // for (i = 0; i < nb_rows; i++)
    //     printf("%d, %d, %.2f\n", res->a[i], res->b[i], res->c[i]);

    return res;
}


/* collector */

int start_collector(unsigned int unit_collect_id) {
    size_t max_collect_data_size = 500;
    size_t data_size = 0;
    int idx = 0;
    int header_size = strlen("Content-Length: ");
    // void collected_data[max_collect_data_size];
    const char *url = "localhost";

    PRINT(LOG_LEVEL_DESCRIBE, "Starting collector DC1\n");

    // ask to core the establisment of tls tunnel
    establish_connection(unit_collect_id, url);

    // build and submit http request packet to remote target
    // const char *req1 = http_get();
    const char *req1 = http_get();
    const char *req2 = http_post("all");
    uint8_t *u8_req1 = const_cast<uint8_t *>(reinterpret_cast<const uint8_t *>(req1));
    uint8_t *u8_req2 = const_cast<uint8_t *>(reinterpret_cast<const uint8_t *>(req2));

    // TODO : use dedicated sub function + no need rows and columns -> to be fixed in serializer
    // std::vector<size_t> req_sizes = { 
    //     sizeof(*req1) * strlen(req1),
    //     sizeof(*req2) * strlen(req2)
    // };

    std::vector<size_t> req_size1 = 
        { sizeof(*req1) * strlen(req1)};
    char req_str[] = "http_req";
    ctrl_pkt *http_pkt1 = new_ctrl_pkt(
        0, 0, req_str, req_size1,
        std::tuple<unsigned int, size_t, uint8_t *>
        (
            strlen(req1),
            sizeof(uint8_t),
            {u8_req1}));

    std::vector<size_t> req_size2 = 
        { sizeof(*req1) * strlen(req2)};
    ctrl_pkt *http_pkt2 = new_ctrl_pkt(
        0, 0, req_str, req_size2,
        std::tuple<unsigned int, size_t, uint8_t *>
        (
            strlen(req2),
            sizeof(uint8_t),
            {u8_req2}
        ));

    // submit_req(unit_collect_id, http_pkt1);
    submit_req(unit_collect_id, http_pkt2);

    int size = 0;
    int read_size = 40;
    int content_len = max_collect_data_size;
    int found = 0;
    int nb_pkts = 0;
    uint8_t **collected_data = (uint8_t **)malloc(sizeof(uint8_t *) * MAX_COLLECT_PKTS);
    for (int i = 0; i < MAX_COLLECT_PKTS; i ++)
        collected_data[i] = (uint8_t *)malloc(sizeof(uint8_t) * 1024);

    // retrieve data
    while (data_size < max_collect_data_size && data_size < content_len)
    {
        PRINT(LOG_LEVEL_DESCRIBE, "Acquiring data in DC1\n");
        
        size = acquire_data(unit_collect_id, collected_data[idx], read_size);
        
        if (size > 0)
        {
            if (found == 0)
            {
                std::string len_str = get_content_len(collected_data[idx]);
                if (!len_str.empty())
                {
                    content_len = std::stoi(len_str);
                    found = 1;
                    // print("Content length = %d\n", content_len);
                }
            }

            PRINT(LOG_LEVEL_DESCRIBE, "\nDC1 has acquired %ld bytes : \n\t\n%s", size, (char *)collected_data[idx]);
            data_size += size;
            idx++;

            if (data_size + 1 - header_size == content_len)
                PRINT(LOG_LEVEL_DESCRIBE, "\nThe collector has read %d bytes\n", content_len);
            else
                nb_pkts++;
        }
        else
        {
            PRINT(LOG_LEVEL_DEBUG, "No data acquired -> continue\n");
        }
    }

    // TODO : new struct to handle columns and rows
    funcres_t *conv_data = convert_data(nb_pkts, collected_data);
    // build and push a new packet composed by all collected data
    std::vector<size_t> data_sizes = {
        sizeof(int) * conv_data->nb_rows,
        sizeof(int) * conv_data->nb_rows,
        sizeof(float) * conv_data->nb_rows
    };
    ctrl_pkt *pkt = new_ctrl_pkt(conv_data->nb_columns,
        conv_data->nb_rows,
        "scrap_data_DC1",
        data_sizes,
        std::tuple<unsigned int, size_t, int *>(
            conv_data->nb_rows,
            sizeof(int),
            conv_data->a),
        std::tuple<unsigned int, size_t, int *>(
            conv_data->nb_rows,
            sizeof(int),
            conv_data->b),
        std::tuple<unsigned int, size_t, float *>(
            conv_data->nb_rows,
            sizeof(float),
            conv_data->c));

    push_data_pkt(unit_collect_id, pkt);

    return 1;
}


int trusted_main(unsigned int sock_id)
{
    register_data_collector(sock_id);
    int res = start_collector(sock_id);

    return res;
}