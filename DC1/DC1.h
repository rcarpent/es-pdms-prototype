#ifndef _DC1_H_
#define _DC1_H_


typedef struct {
	unsigned int nb_columns;
	unsigned int nb_rows;
	int *a;
	int *b;
	float *c;
} funcres_t;

funcres_t *convert_data(int nb_pkts, uint8_t **data);

#endif
