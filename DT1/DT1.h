#ifndef _DT1_H_
#define _DT1_H_


typedef struct {
	unsigned int nb_columns;
	unsigned int nb_rows;
	int *a;
	int *b;
	float *c;
} funcres_t;

funcres_t *func1db(unsigned int nb_columns, unsigned int nb_rows, int *a, int *b, float *c);
funcres_t *func2db(unsigned int nb_columns, unsigned int nb_rows, int *a, int *b, float *c);
funcres_t *func3db(unsigned int nb_columns, unsigned int nb_rows, int *a, int *b, float *c);

#endif
