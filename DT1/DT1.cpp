#include "DT1.h"
#include "CommonEnclave.h"


funcres_t *func1db(unsigned int nb_columns, unsigned int nb_rows, int *a, int *b, float *c) 
{
    PRINT(LOG_LEVEL_DESCRIBE, "DT1 : in function func1db\n");
    PRINT(LOG_LEVEL_DESCRIBE, "Arguments : %d, %d, %d, %d, %f, %f\n", a[0], a[1], b[0], b[1], c[0], c[1]);
    PRINT(LOG_LEVEL_DESCRIBE, "Processing the sum of %d and %d\n", a[0], b[0]);
    PRINT(LOG_LEVEL_DESCRIBE, "Processing the product of %d and %d\n", a[1], b[1]);
    PRINT(LOG_LEVEL_DESCRIBE, "Processing the sum of %f and %f\n", c[0], c[1]);
    funcres_t *res = (funcres_t *)malloc(sizeof(funcres_t));
    res->nb_columns = 3;
    res->nb_rows = 1;
    res->a = (int *)malloc(sizeof(int));
    res->b = (int *)malloc(sizeof(int));
    res->c = (float *)malloc(sizeof(float));
    res->a[0] = a[0] + b[0];
    res->b[0] = a[1] * b[1];
    res->c[0] = c[0] + c[1];
    PRINT(LOG_LEVEL_DESCRIBE, "Result of func1db to submit : [%d, %d, %f]\n", res->a[0], res->b[0], res->c[0]);

    return res;
}

funcres_t *func2db(unsigned int nb_columns, unsigned int nb_rows, int *a, int *b, float *c)
{
    PRINT(LOG_LEVEL_DESCRIBE, "DT1 : in function func2db\n");
    funcres_t *res = (funcres_t *)malloc(sizeof(funcres_t));
    res->nb_columns = 3;
    res->nb_rows = 1;
    res->a = (int *)malloc(sizeof(int));
    res->b = (int *)malloc(sizeof(int));
    res->c = (float *)malloc(sizeof(float));
    res->a[0] = a[0] + 50;
    res->b[0] = 0;
    res->c[0] = 0;
    PRINT(LOG_LEVEL_DESCRIBE, "Result of func1db to submit : [%d, %d, %f]\n", res->a[0], res->b[0], res->c[0]);

    return res;
}

funcres_t *func3db(unsigned int nb_columns, unsigned int nb_rows, int *a, int *b, float *c) 
{
    PRINT(LOG_LEVEL_DESCRIBE, "DT1 : in function func3db\n");
    funcres_t *res = (funcres_t *)malloc(sizeof(funcres_t));
    res->nb_columns = 3;
    res->nb_rows = 1;
    res->a = (int *)malloc(sizeof(int));
    res->b = (int *)malloc(sizeof(int));
    res->c = (float *)malloc(sizeof(float));
    res->a[0] = a[0] * 4;
    res->b[0] = 0;
    res->c[0] = 0;
    PRINT(LOG_LEVEL_DESCRIBE, "Result of func1db to submit : [%d, %d, %f]\n", res->a[0], res->b[0], res->c[0]);

    return res;
}

int trusted_main(unsigned int sock_id)
{
    PRINT(LOG_LEVEL_DESCRIBE, "Nothing to do in main of DT1\n");
    return 1;
}