#ifndef CONTROL_PACKET_H_
#define CONTROL_PACKET_H_

#include <string>
#include <msgpack.hpp>
#include <boost/circular_buffer.hpp>
#include <iostream>
#include <tuple>
#include <vector>
#include <numeric>
#include <random>
#include "../Include/log.h"

#define MAX_ARGS_SIZE 512
#define MAX_FNAME_SIZE 32


// TODO : fix all the recursive variadic function -> dirty implementation for the moment

/* serialization */

template <typename T>
void serialize_pod_p(char **params, unsigned int param_idx, std::tuple<unsigned int, size_t, T> t) 
{
    static_assert(std::is_pod<T>::value, "T must be POD");
    static_assert(std::is_pointer<T>::value, "T must be a pointer or reference");
    PRINTCPP(LOG_LEVEL_DEBUG, "Allocate " << std::get<1>(t) * std::get<0>(t) << " byte(s) in params[" << param_idx << "]" << std::endl);
    params[param_idx] = (char *)malloc(std::get<1>(t) * std::get<0>(t));
    assert(params[param_idx] != NULL);
    
    char *p1 = reinterpret_cast<char *>(std::get<2>(t));
    size_t offset = 0;
    size_t size = sizeof(*(std::get<2>(t)));
    PRINTCPP(LOG_LEVEL_DEBUG, "Size of an element : " << size << std::endl);

    for (unsigned int i = 0; i < std::get<0>(t); i++)
    {
        PRINTCPP(LOG_LEVEL_DEBUG, "Copy " << std::get<1>(t) << " byte(s) into params[" << param_idx << "]" << std::endl);
        auto val = p1;
        memcpy(params[param_idx] + offset, (char *)val, size);
        PRINTCPP(LOG_LEVEL_DEBUG, "Parameter added in params[" << param_idx << "]" << " : " << *(reinterpret_cast<T>(val)) << std::endl);
        p1 += size;
        //debug
        //std::cout << "Offset = " << offset << std::endl;
        offset += size;
    }
}

template<typename T, typename... Args>
void serialize_pod_p(char **params, unsigned int param_idx, std::tuple<unsigned int, size_t, T> t, Args... args)
{
    serialize_pod_p(params, param_idx, t);
    serialize_pod_p(params, ++param_idx, args...);
}

// TODO :
//     - refactorize to avoid redundant type passing by parameter and deduction mechanism
//     - respect templated style
//     - check seed and random device
template<typename T>
std::tuple<unsigned int, size_t, T> build_random_t(std::string type, unsigned int nb_elts, int minBound, int maxBound)
{
    size_t size;
    std::random_device re;
    T val;

    if (!type.compare("int"))
    {
        size = sizeof(int);
        std::uniform_int_distribution<int> gen(minBound, maxBound);
        val = (T)malloc(size * nb_elts);
        assert(val != NULL);
        
        for (unsigned int i = 0; i < nb_elts; i++)
            val[i] = gen(re);
    }

    else if (!type.compare("uchar"))
    {
        size = sizeof(unsigned char);
        std::uniform_int_distribution<int> gen(1, 255);
        val = (T)malloc(size * nb_elts);
        assert(val != NULL);
        for (unsigned int i = 0; i < nb_elts; i++) 
            val[i] = static_cast<unsigned char>(gen(re));
    }

    else if (!type.compare("float"))
    {
        size = sizeof(float);
        std::uniform_real_distribution<double> gen((float)minBound, (float)maxBound);
        val = (T)malloc(size * nb_elts);
        assert(val != NULL);

        for (unsigned int i = 0; i < nb_elts; i++)
            val[i] = gen(re);
    }

    else if (!type.compare("double"))
    {
        size = sizeof(double);
        std::uniform_real_distribution<double> gen((double)minBound, (double)maxBound);
        val = (T)malloc(size * nb_elts);
        assert(val != NULL);

        for (unsigned int i = 0; i < nb_elts; i++)
            val[i] = gen(re);
    }

    else
    {
        throw std::string("Unknown type");
    }

    PRINTCPP(LOG_LEVEL_DEBUG, "Values generated in random tuple of type " << type << " : ");
    for (unsigned int i = 0; i < nb_elts; i++)
        PRINTCPP(LOG_LEVEL_DEBUG, val[i] << ", ");
    PRINTCPP(LOG_LEVEL_DEBUG, std::endl);

    return std::make_tuple(nb_elts, size, val);
}

/* control packets */

// structures of control packets
typedef struct 
{
    std::string cmd;
    std::string arg1;
    std::string arg2;
    MSGPACK_DEFINE(cmd, arg1, arg2);
} ctrl_pkt;

typedef struct  
{
    size_t size_fname;
    size_t size_params;
    unsigned int nb_params;
    char params[];

} ctrl_pkt_to_send;

typedef struct  
{
    size_t size_fname;
    size_t size_params;
    unsigned int nb_params;
    char *params;

} ctrl_pkt_to_core;

// circular buffer of control packets
typedef boost::circular_buffer<uint8_t *> circular_buffer;

// TODO : move from header file
static inline ctrl_pkt_to_send *params_to_ctrl_pkt(unsigned int nb_func_args, char *fname, char **params, std::vector<size_t> sizes)
{
    unsigned int v_length = (unsigned int)sizes.size();
    size_t size_fname = strlen(fname) + 1;
    size_t size = 0;
    unsigned int i;
    
    // note : accumulate is not present in intel sgx lib
    for (i = 0; i < v_length; i++)
        size += sizes[i];

    ctrl_pkt_to_send *pkt = (ctrl_pkt_to_send *)malloc(sizeof(ctrl_pkt_to_send) + sizeof(char) * (size_fname + size));
    assert(pkt != NULL);
    pkt->size_fname = size_fname;
    pkt->size_params = size;
    pkt->nb_params = nb_func_args;
    size_t offset = 0;
    memcpy(pkt->params, fname, size_fname);
    offset += size_fname;

    for (i = 0; i < v_length; i++)
    {
        memcpy(pkt->params + offset, params[i], sizes[i]);
        offset += sizes[i];
    }

    return pkt;
}

// TODO : to refactorize to avoid redundant memcpy
// TODO : fix recursion call
template<typename T, typename... Args>
ctrl_pkt_to_send *new_ctrl_pkt(unsigned int nb_func_args, char *fname, std::vector<size_t> sizes,\
 std::tuple<unsigned int, size_t, T> t, Args... args)
{
    unsigned int param_idx = 0;
    char **params = (char **)malloc(sizeof(char *) * nb_func_args);
    assert(params != NULL);
    serialize_pod_p(params, param_idx, t);
    serialize_pod_p(params, ++param_idx, args...);
    ctrl_pkt_to_send * pkt = params_to_ctrl_pkt(nb_func_args, fname, params, sizes);

    // free params after their copy
    for (unsigned int i = 0; i < nb_func_args; i++)
    {
        free(params[i]);
    }
    free(params);

    return pkt;
}

// temporary fix to handle a single parameter
// TODO :  to refactorize with associated recursive variadic function
template<typename T>
ctrl_pkt_to_send *new_ctrl_pkt(unsigned int nb_func_args, char *fname, std::vector<size_t> sizes,\
 std::tuple<unsigned int, size_t, T> t)
{
    unsigned int param_idx = 0;
    char **params = (char **)malloc(sizeof(char *) * nb_func_args);
    assert(params != NULL);
    serialize_pod_p(params, param_idx, t);
    ctrl_pkt_to_send * pkt = params_to_ctrl_pkt(nb_func_args, fname, params, sizes);

    // free params after their copy
    for (unsigned int i = 0; i < nb_func_args; i++)
    {
        free(params[i]);
    }
    free(params);

    return pkt;
}

#endif
