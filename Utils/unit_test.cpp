// #include <iostream>
// #include <string.h>
// #include <vector>
// #include <numeric>
// #include <random>
#include "ControlPacket.h"

// typedef struct  
// {
//     size_t size_fname;
//     size_t size_params;
//     unsigned int nb_params;
//     char params[];

// } ctrl_pkt_to_send;

// // TODO :
// //     - refactorize to avoid redundant type passing by parameter and deduction mechanism
// //     - respect templated style
// //     - check seed and random device
// template<typename T>
// std::tuple<unsigned int, size_t, T> build_random_t(std::string type, unsigned int nb_elts, int minBound, int maxBound)
// {
// 	size_t size;
//     std::random_device re;
//     T val;

// 	if (!type.compare("int"))
// 	{
// 		size = sizeof(int);
// 		std::uniform_int_distribution<int> gen(minBound, maxBound);
// 		val = (T)malloc(size * nb_elts);
		
// 		for (unsigned int i = 0; i < nb_elts; i++)
// 			val[i] = gen(re);
// 	}

// 	else if (!type.compare("uchar"))
// 	{
// 		size = sizeof(unsigned char);
// 		std::uniform_int_distribution<int> gen(1, 255);
// 		val = (T)malloc(size * nb_elts);
// 		for (unsigned int i = 0; i < nb_elts; i++) 
// 			val[i] = static_cast<unsigned char>(gen(re));
// 	}

// 	else if (!type.compare("float"))
// 	{
// 		size = sizeof(float);
// 		std::uniform_real_distribution<double> gen((float)minBound, (float)maxBound);
// 		val = (T)malloc(size * nb_elts);

// 		for (unsigned int i = 0; i < nb_elts; i++)
// 			val[i] = gen(re);
// 	}

// 	else if (!type.compare("double"))
// 	{
// 		size = sizeof(double);
// 		std::uniform_real_distribution<double> gen((double)minBound, (double)maxBound);
// 		val = (T)malloc(size * nb_elts);

// 		for (unsigned int i = 0; i < nb_elts; i++)
// 			val[i] = gen(re);
// 	}

// 	else
// 	{
// 		throw std::string("Unknown type");
// 	}

// 	std::cout << "Values generated in random tuple of type " << type << " : ";
// 	for (unsigned int i = 0; i < nb_elts; i++)
// 		std::cout << val[i] << ", ";
// 	std::cout << std::endl;

// 	return std::make_tuple(nb_elts, size, val);
// }

// ctrl_pkt_to_send *params_to_ctrl_pkt(unsigned int nb_func_args, char *fname, char **params, std::vector<size_t> sizes)
// {        
// 	unsigned int v_length = sizes.size();
//     size_t size_fname = strlen(fname);
//     size_t size = std::accumulate(sizes.begin(), sizes.end(), 0);
//     ctrl_pkt_to_send *pkt = (ctrl_pkt_to_send *)malloc(sizeof(ctrl_pkt_to_send) + sizeof(char) * (size_fname + size));
//     pkt->size_fname = size_fname;
//     pkt->size_params = size;
//     pkt->nb_params = nb_func_args;
//     size_t offset = 0;
//     memcpy(pkt->params, fname, size_fname);
//     offset += size_fname;

//     for (unsigned int i = 0; i < v_length; i++)
//     {
//     	memcpy(pkt->params + offset, params[i], sizes[i]);
//      offset += sizes[i];
// 	}
//		return pkt;
// }

// template <typename T>
// void serialize_pod_p(char **params, unsigned int param_idx, std::tuple<unsigned int, size_t, T> t) 
// {
// 	static_assert(std::is_pod<T>::value, "T must be POD");
// 	static_assert(std::is_pointer<T>::value, "T must be a pointer or reference");
// 	std::cout << "Allocate " << std::get<1>(t) * std::get<0>(t) << " byte(s) in params[" << param_idx << "]" << std::endl;
// 	params[param_idx] = (char *)malloc(sizeof(char *) * std::get<1>(t));
	
// 	char *p1 = reinterpret_cast<char *>(std::get<2>(t));
// 	size_t offset = 0;
// 	size_t size = sizeof(*(std::get<2>(t)));

// 	for (unsigned int i = 0; i < std::get<0>(t); i++)
// 	{
// 	std::cout << "Copy " << std::get<1>(t) << " byte(s) into params[" << param_idx << "]" << std::endl;
// 		auto val = p1;
// 		memcpy(params[param_idx] + offset, (char *)val, size);
// 		std::cout << "Parameter added in params[" << param_idx << "]" << " : " << *(reinterpret_cast<T>(val)) << std::endl;
// 		p1 += size;
// 		offset += size;
// 	}
// }

// template<typename T, typename... Args>
// void serialize_pod_p(char **params, unsigned int param_idx, std::tuple<unsigned int, size_t, T> t, Args... args)
// {
// 	serialize_pod_p(params, param_idx, t);
// 	serialize_pod_p(params, ++param_idx, args...);
// }

// template<typename T, typename... Args>
// ctrl_pkt_to_send *new_ctrl_pktV0(unsigned int nb_func_args, char *fname, char **params, std::vector<size_t> sizes,\
//  unsigned int param_idx, std::tuple<unsigned int, size_t, T> t, Args... args)
// {
// 	serialize_pod_p(params, param_idx, t);
// 	serialize_pod_p(params, ++param_idx, args...);
// 	ctrl_pkt_to_send * pkt = params_to_ctrl_pkt(nb_func_args, fname, params, sizes);
// }

// TODO : to refactorize to avoid redundant memcpy
// template<typename T, typename... Args>
// ctrl_pkt_to_send *new_ctrl_pkt(unsigned int nb_func_args, char *fname, std::vector<size_t> sizes,\
//  std::tuple<unsigned int, size_t, T> t, Args... args)
// {
// 	unsigned int param_idx = 0;
// 	char **params = (char **)malloc(sizeof(char *) * nb_func_args);
// 	serialize_pod_p(params, param_idx, t);
// 	serialize_pod_p(params, ++param_idx, args...);
// 	ctrl_pkt_to_send * pkt = params_to_ctrl_pkt(nb_func_args, fname, params, sizes);

// 	// free params after their copy
// 	for (unsigned int i = 0; i < nb_func_args; i++)
// 	{
// 		free(params[i]);
// 	}
// 	free(params);

// 	return pkt;
// }


// units tests
int main()
{
	int a = 345;
	int b[2] = {1024, 4};
	double c = 99;
	float d[2] = {34567.6778, 2232.222};
	uint8_t *e = (uint8_t *)"Hello";
	uint8_t *e1 = (uint8_t *)"Hello";
	uint8_t *e2 = (uint8_t *)"World";
	uint8_t *e3 = (uint8_t *)"HowAreYou?";
	uint8_t f[4] = {7, 33, 44, 254};
	char fname[] = "func1";
	unsigned int nb_func_args = 6;
	unsigned int param_idx = 0;

	// unit test 1
	char **params = (char **)malloc(sizeof(char *) * nb_func_args);
	// serialize_pod_p(params, param_idx, std::make_tuple((unsigned int)2, (size_t)8, b));
	//serialize_pod_p(params, param_idx, std::make_tuple((unsigned int)1, (size_t)4, &a));
	//serialize_pod_p(params, param_idx, std::make_tuple((unsigned int)1, sizeof(c), c));
	// serialize_pod_p(params, param_idx, std::make_tuple((unsigned int)5, sizeof(char), e));
	//serialize_pod_p(params, param_idx, std::make_tuple((unsigned int)4, sizeof(*f), f));

	// unit test 1.12
	serialize_pod_p(params, param_idx,
		std::make_tuple((unsigned int)5, sizeof(*e1), e1),
		std::make_tuple((unsigned int)5, sizeof(*e2), e2),
		std::make_tuple((unsigned int)10, sizeof(*e3), e3));

	std::vector<size_t> sizes = { 5 * sizeof(*e1),  5 * sizeof(*e2), 10 * sizeof(*e3)};
	ctrl_pkt_to_send *pkt = params_to_ctrl_pkt(3, fname, params, sizes);

	char *data = reinterpret_cast<char *>(pkt);
	size_t size = sizeof(pkt->size_fname) + sizeof(pkt->size_params) + sizeof(pkt->nb_params);
	char funcname[32];
	memcpy(funcname, data + size, pkt->size_fname);
	std::cout << "funcname = " << funcname << std::endl;
	char func_params[pkt->size_params];
	memcpy(func_params, data + size + pkt->size_fname, pkt->size_params);
	std::cout << "func_params : " << (uint8_t *)func_params << std::endl;

	// unit test 2
	// serialize data
	// serialize_pod_p(params, param_idx, std::make_tuple((unsigned int)1, sizeof(a), &a),\
	//  std::make_tuple((unsigned int)2, sizeof(*b), b), std::make_tuple((unsigned int)1, sizeof(c), &c),\
	//  std::make_tuple((unsigned int)2, sizeof(*d), d), std::make_tuple((unsigned int)5, sizeof(*e), e),\
	//  std::make_tuple((unsigned int)4, sizeof(*f), f));

	// build control packet
	// std::vector<size_t> sizes = {sizeof(a), 2 * sizeof(*b), 1 * sizeof(c), 2 * sizeof(*d), 5 * sizeof(*e),  4 * sizeof(*f)};
	//ctrl_pkt_to_send *pkt = params_to_ctrl_pkt(nb_func_args, fname, params, sizes);

	// ctrl_pkt_to_send *pkt = new_ctrl_pktV0(nb_func_args, fname, params, sizes, param_idx, std::make_tuple((unsigned int)1, sizeof(a), &a),\
	//  std::make_tuple((unsigned int)2, sizeof(*b), b), std::make_tuple((unsigned int)1, sizeof(c), &c),\
	//  std::make_tuple((unsigned int)2, sizeof(*d), d), std::make_tuple((unsigned int)5, sizeof(*e), e),\
	//  std::make_tuple((unsigned int)4, sizeof(*f), f

	// unit test 3
	// build control packet -> last version
	// unsigned int a_nb_elts = 1, b_nb_elts = 2, c_nb_elts = 1, d_nb_elts = 2, e_nb_elts = 5, f_nb_elts = 4;
	// size_t a_size = sizeof(a), b_size = sizeof(*b), c_size = sizeof(c), d_size = sizeof(*d), e_size = sizeof(*e), f_size = sizeof(*f);
	// std::vector<size_t> sizes = {a_nb_elts * a_size , b_nb_elts * b_size, c_nb_elts * c_size,\
	//  d_nb_elts * d_size, e_nb_elts * e_size,  f_nb_elts * f_size};

	// ctrl_pkt_to_send *pkt1 = new_ctrl_pkt(nb_func_args, fname, sizes, std::make_tuple(a_nb_elts, a_size, &a),\
	//  std::make_tuple(b_nb_elts, b_size, b), std::make_tuple(c_nb_elts, c_size, &c),\
	//  std::make_tuple(d_nb_elts, d_size, d), std::make_tuple(e_nb_elts, e_size, e),\
	//  std::make_tuple(f_nb_elts, f_size, f));

	// unit test 4
	// build random tuple
	// auto v = build_random_t<int *>("int", 10, 1, 20);
	// std::cout << "Display values of random tuple generated : ";
	// for (unsigned int i = 0; i < 10; i++)
	// 	std::cout << std::get<2>(v)[i] << " ";
	// std::cout << std::endl;

	// unit test 5
	// build control packet with random tuples
	// unsigned int v1_nb_elts = 2, v2_nb_elts = 3, v3_nb_elts = 4, v4_nb_elts = 5;
	// auto v1 = build_random_t<int *>("int", v1_nb_elts, 1, 200);
	// auto v2 = build_random_t<unsigned char *>("uchar", v2_nb_elts, 1, 255);
	// auto v3 = build_random_t<float *>("float", v3_nb_elts, 1, 200);
	// //auto v4 = build_random_t<double *>("double", v4_nb_elts, -100, 100);
	// auto v4 = build_random_t<double *>("double", v4_nb_elts, 1, 200);
	// ctrl_pkt_to_send *pkt = new_ctrl_pkt(nb_func_args, fname, sizes, v1, v2, v3, v4);

	// return 1;
}