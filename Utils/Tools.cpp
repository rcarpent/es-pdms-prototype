#include "Tools.h"
#include <regex>
#include <fstream>
#include <sstream>
#include <iostream>

#include <string>
#include <vector>
#include <stdio.h>
#include <string.h>
#include <map>
#include <algorithm>


using namespace std;

#define LIB_RELATIVE_PATH "../lib/"

//TODO : refactorize to use a specific configuration folder and associated environment variable

string remove_signed_enclave_path(string enclave_path)
{
    string subname = regex_replace(enclave_path, regex(".so.signed"), "");
    size_t found = subname.find_last_of("/\\");
    subname = subname.substr(found + 1);
    return subname;
}

string get_ulibname(string enclave_name)
{
    //return regex_replace(enclave_name, regex("so.signed"), "untrusted.so");
    return LIB_RELATIVE_PATH + enclave_name + ".untrusted.so";
}

// specifig to OE
string get_enclave_create_fname(string enclave_name)
{
    // string subname = remove_signed_enclave_path(enclave_name);
    // string oe_create_fname = "oe_create_" + subname + "_enclave";
    string oe_create_fname = "oe_create_" + enclave_name + "_enclave";
    return oe_create_fname;
}

// deprecated because of CPUID emulation applied by OE SDK (i.e at compile time) 
string get_enclave_pubkey_name(string enclave_name)
{
    stringstream pubkey_name_ss;
    string subname = regex_replace(enclave_name, regex(".so.signed"), "");
    size_t found = subname.find_last_of("/\\");
    subname = subname.substr(found + 1);
    pubkey_name_ss << subname << "_pubkey.pem";
    return pubkey_name_ss.str();
}

string read_pubkey_content(string pubkey_name)
{
    char *pPath;
    pPath = getenv("PDMS_PUBKEYS_FOLDER");
    if (pPath != NULL)
        PRINTCPP(LOG_LEVEL_DESCRIBE, "The current path for pubkeys is : " << pPath << endl);
    string line;
    stringstream pubkey_pathname_ss;
    pubkey_pathname_ss << pPath << "/" << pubkey_name;
    stringstream pubkey_ss;
    ifstream pubkey_file(pubkey_pathname_ss.str(), std::ios::in);

    if (!pubkey_file.is_open())
    {   
        string err = "Error during opening of file " + pubkey_pathname_ss.str();
        throw err;
    }

    while (getline(pubkey_file, line)) {
        pubkey_ss << line << "\n";
    }

    pubkey_file.close();

    return pubkey_ss.str();
}

// csv parsing
vector <vector <string> > read_file(
    const char *filename,
    string &tablename,
    string &format)
{  
    vector <vector <string> > data;
    ifstream infile(filename);

    if(!infile.is_open())
    {
        PRINTCPP(LOG_LEVEL_ERRORS, "File " << filename << " not found." << endl);
    }
    while (infile)
    {
        string s;
        if (!getline(infile, s))
            break;

        if (s.find("tablename") != string::npos)
        {
            tablename = s.substr(10);
            PRINTCPP(LOG_LEVEL_DESCRIBE, "Read data of table : " << tablename << endl);
            continue;
        }

        if (s.find("format") != string::npos)
        {
            format = s.substr(7);
            PRINTCPP(LOG_LEVEL_DESCRIBE, "Format of table : " << format << endl);
            continue;
        }

        PRINTCPP(LOG_LEVEL_FULL, "Read line : " << s << endl);

        istringstream ss(s);
        vector<string> record;

        while (ss)
        {
            string s;

            if (!getline(ss, s, ','))
                break;

            // cout << "Push value " << s << " in record" << endl;
            record.push_back(s);
        }

        data.push_back(record);
    }

    if (!infile.eof())
    {
        PRINTCPP(LOG_LEVEL_ERRORS, "Parsing finished without eof!" << endl);
    }

    infile.close();
    return data;
}

// convert string data to char * data
int convert_db_data(
    const char *filename,
    char **tablename_ptr,
    char **format_ptr,
    char ***types_ptr,
    char ****data_ptr,
    unsigned int *size_tablename,
    unsigned int *size_format,
    unsigned int *rows,
    unsigned int *columns)
{
    size_t pos = 0;
    int i, j, start = 0, end = 0;
    string token;
    string delimiter1 = ",";
    string delimiter2 = " ";
    string substr1, substr2;
    vector<string> types_str;

    string tablename_str, format_str;
    auto data_str = read_file(filename, tablename_str, format_str);

    int nb_columns = count(format_str.begin(), format_str.end(), ',') + 1;
    int nb_rows = data_str.size();

    char *tablename = (char*)malloc(sizeof(char) * (tablename_str.length() + 1));
    assert(tablename != NULL);

    char **types = (char**)malloc(sizeof(char *) * nb_columns);
    assert(types != NULL);

    char *format = (char *)malloc(sizeof(char) * (format_str.length() + 1));
    assert(format != NULL);

    char ***data = (char ***)malloc(sizeof(char **) * nb_rows);
    assert(data != NULL);

    PRINTCPP(LOG_LEVEL_DEBUG, "Table is constitued of " << nb_columns <<
        " columns and " << nb_rows << " rows" << endl);

    string bind_idx = "";
    for (int idx = 1; idx < nb_columns + 1; idx++)
    {
        bind_idx += "?" + to_string(idx);
        if (idx != nb_columns)
            bind_idx += " ,";
    }

    string format_idx = "(" + bind_idx + ")";

    PRINTCPP(LOG_LEVEL_DEBUG, "Format to bind : " << format_idx << endl);
    PRINTCPP(LOG_LEVEL_DEBUG, "Fill table " << tablename_str << " with the format " << format_str << endl);

    auto get_last_substr = [] (string s, string delimiter)
    {
        int idx = s.find(delimiter);
        string substr1 = s.substr(idx + 1);

        int end = substr1.find(delimiter);
        string substr2 = substr1.substr(0, end);

        return substr2;
    };

    start = 0;
    end = format_str.find(delimiter1);

    while (end != string::npos)
    {
        substr1 = format_str.substr(start, end - start);
        start = end + delimiter1.length();
        end = format_str.find(delimiter1, start);
        substr2 = get_last_substr(substr1, delimiter2);
        types_str.push_back(substr2);
    }
    string temp = format_str.substr(start, end);
    types_str.push_back(get_last_substr(temp, delimiter2));

    PRINTCPP(LOG_LEVEL_DEBUG, "Types : ");
    for (auto &t : types_str)
        PRINTCPP(LOG_LEVEL_DEBUG, t << ", ");
    PRINTCPP(LOG_LEVEL_DEBUG, endl);

    // parameters of marshmalling to trusted zone
    for (i = 0; i < nb_rows; i++)
    {
        data[i] = (char **)malloc(sizeof(char *) * nb_columns);
        assert(data[i] != NULL);
        for (j = 0; j < nb_columns; j++)
        {
            data[i][j] = (char *)malloc(sizeof(char) * (data_str[i][j].length() + 1));
            assert(data[i][j] != NULL);
            strcpy(data[i][j], (data_str[i][j]).c_str());
        }
    }

    PRINTCPP(LOG_LEVEL_FULL, "Content of params to marshmal : " << endl);
    for (i = 0; i < nb_rows; i++)
    {
        for (j = 0; j < nb_columns; j++)
        {
            PRINTCPP(LOG_LEVEL_FULL, "\t" << data[i][j] << ", ");
        }
        PRINTCPP(LOG_LEVEL_FULL, endl);
    }

    for (i = 0; i < nb_columns; i++)
    {
        types[i] = (char *)malloc(sizeof(char) * (types_str[i].length() + 1));
        strcpy(types[i], types_str[i].c_str());
    }

    strcpy(tablename, tablename_str.c_str());
    strcpy(format, format_str.c_str());
    *size_tablename = strlen(tablename) + 1;
    *size_format = strlen(format) + 1;
    *rows = nb_rows;
    *columns = nb_columns;

    *tablename_ptr = tablename;
    *format_ptr = format;
    *types_ptr = types;
    *data_ptr = data;

    return 0;
}
