#pragma once

#include <string>
#include "assert.h"
#include "../Include/log.h"

std::string remove_signed_enclave_path(std::string enclave_path);
std::string get_ulibname(std::string enclave_name);
std::string get_enclave_create_fname(std::string enclave_name);
std::string get_enclave_pubkey_name(std::string enclave_name);
std::string read_pubkey_content(std::string pubkey_name);
int convert_db_data(
	const char *filename,
    char **tablename_ptr,
    char **format_ptr,
    char ***types_ptr,
    char ****data_ptr,
    unsigned int *size_tablename,
    unsigned int *size_format,
    unsigned int *rows,
    unsigned int *columns);