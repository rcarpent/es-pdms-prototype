#include <zmqpp/zmqpp.hpp>
#include "ControlPacket.h"
#include "Cmd.h"
#include "AcqServer.h"

#define CTRL_MODULE_PORT 4831


using namespace std;

void send_pkt(zmqpp::socket& socket, char* fname, int db_request_mode, int db_first_param, int db_second_param);

vector<string> commands = {
    "cmd_init",
    "cmd_deinit",
    "cmd_start",
    "cmd_restart",
    "cmd_stop",
    "cmd_load_table",
    "cmd_load_enc_table",
    "cmd_compute", // Only with Core
    "cmd_exec_sql",
    "cmd_free_sql",
    "cmd_compute_stored",
    "cmd_custom_load",
    "execute_dt_function",
    "cmd_save_benchmark_results"};
vector<string> etypes = {
    "core",
    "data_task",
    "data_collector"};

int main(int argc, char *argv[])
{
    if (argc < 4)
    {
        PRINTCPP(LOG_LEVEL_ERRORS, "Usage : command enclave_type(or all) enclave_name(or all)" << endl);
        PRINTCPP(LOG_LEVEL_ERRORS, "\tcommands : cmd_init/cmd_deinit/cmd_stop/cmd_restart/cmd_load_table/cmd_load_enc_table" << endl);
        PRINTCPP(LOG_LEVEL_ERRORS, "\ttypes : core/data_task/data_collector" << endl);
        return 1;
    }

    string cmd(argv[1]);
    PRINTCPP(LOG_LEVEL_DESCRIBE, "Command = " << cmd << endl);
    auto found1 = find(commands.begin(), commands.end(), cmd);

    if (found1 == commands.end())
    {
        PRINTCPP(LOG_LEVEL_ERRORS, "Unknow command" << endl);
        return 1;
    }

    if(cmd.substr(0, 3).compare("cmd") == 0)
    {
        // cmd_* are relayed to core
        string etype(argv[2]);
        PRINTCPP(LOG_LEVEL_DESCRIBE, "Type = " << etype << endl);
        auto found2 = find(etypes.begin(), etypes.end(), etype); 

        if (found2 == etypes.end())
        {
            PRINTCPP(LOG_LEVEL_ERRORS, "Unknow type" << endl);
            return 1;
        }

        // TODO : check if ename exists
        string ename(argv[3]);
        PRINTCPP(LOG_LEVEL_DESCRIBE, "Ename = " << ename << endl);

        string opt_arg = "";
        if (argc >= 5) 
            opt_arg = string(argv[4]); // db table associated to enclave 
        for(int i = 5 ; i < argc ; i++)
        {
            opt_arg += "|" + string(argv[i]);
        }

        // connect to Control Module
        zmqpp::context context;
        zmqpp::socket socket(context, zmqpp::socket_type::request);
        connect_to_server(socket, CTRL_MODULE_PORT);

        // Send command
        string cmd_to_send = cmd + "_" + etype;
        ctrl_pkt pkt = { cmd_to_send, ename, opt_arg};
        zmqpp::message *msg = pack_data(pkt);
        socket.send(*msg);
        delete(msg);
        // check the return code
        ctrl_pkt reply;
        acquire_pkt(socket, reply);
        PRINTCPP(LOG_LEVEL_DESCRIBE, "Return code from control module : " << reply.cmd  << ", " << reply.arg1 << endl);

        socket.close();
        
        PRINTCPP(LOG_LEVEL_DESCRIBE, "Command " << cmd_to_send << "(" << ename << ") successfully returned." << endl);
    } 
    else if(cmd.compare("execute_dt_function") == 0)
    {
        /*
            ./Cmd execute_dt_function {datatask_path} {function_name} {db_request_mode} [db_first_param] [db_second_param] ...
            For now, only db_first_param is supported.
        */
    
        char* datatask_path = argv[2];
        char* fname = argv[3];
        int db_request_mode = stoi(argv[4]);

        std::vector<std::tuple<unsigned int, unsigned long, int*>> db_params;
        db_params.push_back(build_random_t<int *>("int", 1, db_request_mode, db_request_mode)); // db request mode -> use of vector only to be compatible with serialization

        std::vector<size_t> sizes;
        sizes.push_back(sizeof(int)); //At least the size of db_request_mode

        for(int i = 5 ; i < argc ; i++) //db params provided
        {
            db_params.push_back(build_random_t<int *>("int", 1, stoi(argv[i]), stoi(argv[i])));
            sizes.push_back(sizeof(int));
        }

        // register acqunit
        zmqpp::context context;
        zmqpp::socket socket(context, zmqpp::socket_type::push);
        register_new_acqunit(socket, datatask_path);

        ctrl_pkt_to_send *pkt;
        if(db_params.size() == 1){ //Temporary until ctrl_pkt can handle more than 2 parameters.
            pkt = new_ctrl_pkt(db_params.size(), fname, sizes, db_params[0]);
        } else if(db_params.size() == 2){
            pkt = new_ctrl_pkt(db_params.size(), fname, sizes, db_params[0], db_params[1]);
        } else {
            pkt = new_ctrl_pkt(db_params.size(), fname, sizes, db_params[0], db_params[1], db_params[2]);
        }
        size_t size_pkt = sizeof(ctrl_pkt_to_send) + strlen(fname) + sizeof(int) * db_params.size();

        // send packet
        zmqpp::message msg = zmqpp::message(size_pkt);
        msg.add_raw(reinterpret_cast<char *>(pkt), size_pkt);
        socket.send(msg);
        socket.close();
        free(pkt);
        for (unsigned int i = 0 ; i < db_params.size() ; i++)
        {
            free(get<2>(db_params[i]));
        }
    }

    
    return 0;
}