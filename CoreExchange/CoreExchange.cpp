#include <stdlib.h>
#include "CoreExchange.h"
#include <cstdarg>
#include <chrono>
#include <string.h>
#include <thread>
#include <functional>
#include <pthread.h>
#include "Tools.h"

#define MAX_OCALL_PARAM_LEN 64
#define MAX_OCALL_SUB_PARAM_LEN 32
#define MAX_OCALL_NB_PARAMS 24

using namespace std;

map<string, pair<int, int>> dt_acqunits_ports;
vector<unsigned int> dt_ids(MAX_NB_ENCLAVES, 0);


/* UntrustedLib */

unsigned int UntrustedLib::load_ulib(const char *ulibname)
{
    void *handle = dlopen(ulibname, RTLD_NOW|RTLD_LOCAL);

    if (!handle)
    {
        PRINTCPP(LOG_LEVEL_ERRORS, "Error during loading of " << ulibname << " : " <<  dlerror() << endl);
        return 0;
    }
    else
        PRINTCPP(LOG_LEVEL_DESCRIBE, "Success to load " << ulibname << endl);
   
   this->ulib = handle;

   return 1;
}

// TODO : to refactorize using template
unsigned int UntrustedLib::load_ufunc(vector<string> &ufuncnames)
{
    for (auto it = ufuncnames.begin(); it != ufuncnames.end(); it++)
    {
        // debug
        // cout << "Loading of function " << *it << endl;
        void *f = dlsym(this->ulib, (*it).c_str());
        const char *dlsym_error = dlerror();

        if (dlsym_error) 
        {        
            PRINTCPP(LOG_LEVEL_ERRORS, "Cannot load symbol " << *it << " : " << dlsym_error << endl);
            return 0;
        }

        if ((*it).find("oe_create_") != string::npos)
        {
            this->oe_create_enclave = (oe_create_enclave_t)f;
        }
        else if ((*it).find("oe_terminate_enclave") != string::npos)
        {
            this->oe_terminate_enclave = (oe_terminate_enclave_t)f;
        }
        else if ((*it).find("set_enclave_name") != string::npos)
        {
            this->set_enclave_name = (set_enclave_name_t)f;
        }
        else if ((*it).find("add_new_dispatcher") != string::npos)
        {
            this->add_new_dispatcher = (add_new_dispatcher_t)f;
        }
        else if (!(*it).compare("register_sec_sock"))
        {
            this->register_sec_sock = (register_sec_sock_t)f;
        }
        else if (!(*it).compare("retrieve_ctrl_pkt"))
        {
            this->retrieve_ctrl_pkt = (retrieve_pkt_t)f;
        }
        else if (!(*it).compare("retrieve_data_pkt"))
        {
            this->retrieve_data_pkt = (retrieve_pkt_t)f;
        }
        else if (!(*it).compare("exchange_pkt"))
        {
            this->exchange_pkt = (exchange_pkt_t)f;
        }
        else if (!(*it).compare("get_enclave_format_settings"))
        {
            this->get_enclave_format_settings = (get_enclave_format_settings_t)f;
        }
        else if (!(*it).compare("get_targeted_evidence_with_public_key"))
        {
            this->get_targeted_evidence_with_public_key = (get_targeted_evidence_with_public_key_t)f;
        }
        else if (!(*it).compare("verify_evidence_and_set_public_key"))
        {
            this->verify_evidence_and_set_public_key = (verify_evidence_and_set_public_key_t)f;
        }
        else if (!(*it).compare("collect_data_pkt"))
        {
            this->collect_data_pkt = (collect_data_pkt_t)f;
        }
        else if (!(*it).compare("load_table_db"))
        {
            this->load_table_db = (load_table_db_t)f;
        }
        else if (!(*it).compare("load_enc_table_db"))
        {
            this->load_enc_table_db = (load_enc_table_db_t)f;
        }
        else if (!(*it).compare("exec_core_computation"))
        {
            this->exec_core_computation = (exec_core_computation_t)f;
        }
        else if (!(*it).compare("exec_sql_and_store"))
        {
            this->exec_sql_and_store = (exec_sql_and_store_t)f;
        }
        else if (!(*it).compare("free_stored_sql"))
        {
            this->free_stored_sql = (free_stored_sql_t)f;
        }
        else if (!(*it).compare("compute_on_stored_data"))
        {
            this->compute_on_stored_data = (compute_on_stored_data_t)f;
        }
        else if (!(*it).compare("custom_data_load"))
        {
            this->custom_data_load = (custom_data_load_t)f;
        }
        else if (!(*it).compare("trusted_main"))
        {
            this->trusted_main = (trusted_main_t)f;
        }
        else {
            PRINTCPP(LOG_LEVEL_ERRORS, *it << " : unknown ufunction" << endl);
            return 0;
        }
    }

    PRINTCPP(LOG_LEVEL_DESCRIBE, "Success to load all the functions of untrusted library" << endl);

    return 1;
}


/* Enclave Handler */

EnclaveHandler::EnclaveHandler(string ename, oe_enclave_t *eid) 
{
    this->enclave_name = remove_signed_enclave_path(ename);
    this->enclave_pathname = ename;
    this->enclave_id = eid; 
}

EnclaveHandler::~EnclaveHandler()
{
    this->enclave_name = "";
    this->enclave_type = "";
    this->enclave_pathname = "";
    this->pubkey_name = "";
    this->enclave_id = NULL;
    this->trusted_id = 0;
    this->enclave_p_id = &(this->enclave_id);
    this->ulib = NULL;
}

int EnclaveHandler::deinit()
{
    if (enclave_id == NULL)
    {
        PRINTCPP(LOG_LEVEL_ERRORS, "No EnclaveHandler to deinitialize" << endl);
        return 0;
    }

    // stop enclave threads
    this->stop_enclave_threads();

    // delete enclave threads
    this->del_enclave_threads();
    
    // close shared library
    if (dlclose(this->ulib) != 0)
    {
        char *err = dlerror();
        PRINTCPP(LOG_LEVEL_ERRORS, "Error : closing of " << this->enclave_name <<
            "'s' shared library failed : " << err << " -> " << 
            "destruction of enclave is not aborted" << endl);
    }

    // destroy enclave
    oe_result_t result = 
        this->ulib->oe_terminate_enclave(this->enclave_id);

    if (result != OE_OK)
    {
        PRINTCPP(LOG_LEVEL_ERRORS, "Error : deinitialization of " << this->enclave_name <<
            " failed : " << oe_result_str(result) << endl);
        return -1;
    }

    return 1;
}

int EnclaveHandler::is_thread_present(string th_name)
{
    int ret = 0;
    auto it = this->enclave_threads.find(th_name);

    if (it != this->enclave_threads.end())
    {
        PRINTCPP(LOG_LEVEL_DEBUG, "Thread " << th_name << " is present " << endl);
        ret = 1;
    }
    else
        ret = 0;

    return ret;
}
void EnclaveHandler::add_enclave_thread(thread &th, string th_name)
{
    auto th_handler = th.native_handle();
    this->enclave_threads[th_name] = make_pair(move(th), move(th_handler));
}

void EnclaveHandler::del_enclave_thread(std::string th_name)
{
    auto it = this->enclave_threads.find(th_name);

    if (it == this->enclave_threads.end())
    {
        PRINTCPP(LOG_LEVEL_ERRORS, "Thread " << th_name << " doesn't exit" << endl);
    }
    else
    {
        this->enclave_threads.erase(th_name);
    }
}

void EnclaveHandler::del_enclave_threads()
{
    this->enclave_threads.clear();
}

void EnclaveHandler::start_enclave_threads()
{
    PRINTCPP(LOG_LEVEL_DESCRIBE, "Start all the threads of " << this->enclave_name << endl);
    for (auto &th : this->enclave_threads)
    {
        th.second.first.detach();
    }
}

void EnclaveHandler::stop_enclave_threads()
{
    PRINTCPP(LOG_LEVEL_DESCRIBE, "Stop all the threads of " << this->enclave_name << endl);
    for (auto &th : this->enclave_threads)
    {
        pthread_cancel(th.second.second);
    }
    this->del_enclave_threads();
}

/* CoreExchange */

int CoreExchange::init()
{
    string core_name = get_core_name();
    string ulibname = get_ulibname(get_core_name());
    PRINTCPP(LOG_LEVEL_DESCRIBE, "Load the core untrusted library " << ulibname << " associated to the Core " << get_core_name() << endl);
    PRINTCPP(LOG_LEVEL_DESCRIBE, "Load the core untrusted library " << ulibname << endl);
    get_core_ehandler()->set_ulib(new UntrustedLib(get_core_id()));  

    // load the untrusted functions
    string oe_create_fname = get_enclave_create_fname(core_name);
    get_core_ehandler()->get_ulib()->set_oe_create_fname(oe_create_fname);
    vector<string> ufunc = core_func;
    ufunc.push_back(oe_create_fname);
    get_core_ehandler()->get_ulib()->load_ulib(ulibname.c_str());
    get_core_ehandler()->get_ulib()->load_ufunc(ufunc);

    
    string core_pathname = get_core_pathname();
    uint32_t flags = 0;
    #ifdef FLAG_DEBUG_ENCLAVE
        flags |= OE_ENCLAVE_FLAG_DEBUG;
    #endif
    oe_result_t result = get_core_ehandler()->get_ulib()->oe_create_enclave(
        core_pathname.c_str(), OE_ENCLAVE_TYPE_SGX, flags, NULL, 0, get_core_p_id());
    if (result != OE_OK)
    {
        PRINTCPP(LOG_LEVEL_ERRORS, oe_create_fname << " : result = " << result << "(" << oe_result_str(result) << ")" << endl);
        return -1;
    }

    // set enclave name
    get_core_ehandler()->get_ulib()->set_enclave_name(get_core_id(), core_name.c_str(), strlen(core_name.c_str()) + 1);

    // launch the core functionalities
    int err;
    get_core_ehandler()->get_ulib()->trusted_main(get_core_id(), &err, get_core_trusted_id());
    
    if (!err)
    {
        PRINTCPP(LOG_LEVEL_ERRORS, "Error during initialisation of core functionalities" << endl);
        return -1;
    }

    return 1;
}

int CoreExchange::deinit()
{
    int res = 0;

    // destroy the Data Task
    auto ids = get_data_tasks_ids();
    for (auto &id : ids)
    {
        res = data_tasks_ids[id]->deinit();
        if (!res)
        {
           PRINTCPP(LOG_LEVEL_ERRORS, "Warning : Core will be not deinitialized properly" <<
            " because of the failed destruction of Data Task " << id << endl); 
        }
    }

    // destroy the Core
    res = this->get_core_ehandler()->deinit();
    return res;
}

int CoreExchange::init_data_task(int dt_id, string ename, string dt_type)
{
    int err = 0;

    // load the data task untrusted library
    data_tasks_ids[dt_id] = new EnclaveHandler(ename, NULL);
    data_tasks_ids[dt_id]->set_enclave_type(dt_type);
    data_tasks_ids[dt_id]->set_trusted_id(dt_id); //to be able to ecall data task associated dispatcher
    string ulibname = get_ulibname(data_tasks_ids[dt_id]->get_enclave_name());
    string dt_name = data_tasks_ids[dt_id]->get_enclave_name();
    PRINTCPP(LOG_LEVEL_DESCRIBE, "Load the untrusted library " << ulibname << " associated to the " << dt_type << " "  << dt_name << endl);
    //data_tasks_u_lib.push_back(new UntrustedLib(data_tasks_ids[dt_id]->get_enclave_id()));
    data_tasks_ids[dt_id]->set_ulib(new UntrustedLib(data_tasks_ids[dt_id]->get_enclave_id()));

    // load the untrusted functions
    string oe_create_fname = get_enclave_create_fname(dt_name);
    data_tasks_ids[dt_id]->get_ulib()->set_oe_create_fname(oe_create_fname);
    vector<string> ufunc;
    if (!dt_type.compare("DataTask"))
        ufunc = dt_func;
    else if (!dt_type.compare("DataCollector"))
        ufunc = dc_func;
    else
    {
        PRINTCPP(LOG_LEVEL_ERRORS, "Unknown type of enclave -> abort" << endl);
        return -1;
    }
    ufunc.push_back(oe_create_fname);
    data_tasks_ids[dt_id]->get_ulib()->load_ulib(ulibname.c_str());
    data_tasks_ids[dt_id]->get_ulib()->load_ufunc(ufunc);

    // load the data task enclave
    string dt_pathname = data_tasks_ids[dt_id]->get_enclave_pathname();
    uint32_t flags = 0;
    #ifdef FLAG_DEBUG_ENCLAVE
        flags |= OE_ENCLAVE_FLAG_DEBUG;
    #endif
    oe_result_t result = data_tasks_ids[dt_id]->get_ulib()->oe_create_enclave(
        dt_pathname.c_str(), OE_ENCLAVE_TYPE_SGX, flags, NULL, 0, data_tasks_ids[dt_id]->get_enclave_p_id());
    if (result != OE_OK)
    {
        PRINTCPP(LOG_LEVEL_ERRORS, oe_create_fname << " : result = " << result << "(" << oe_result_str(result) << ")" << endl);
        return -1;
    }

    PRINTCPP(LOG_LEVEL_DESCRIBE, "Success to create enclave " << dt_name <<  endl);

    // set enclave name
    data_tasks_ids[dt_id]->get_ulib()->set_enclave_name(data_tasks_ids[dt_id]->get_enclave_id(),\
     dt_name.c_str(), strlen(dt_name.c_str()) + 1);

    // TODO : fix ids of sender/receiver
    //sec_socks[dt_id] = init_sec_sock(&core_id, &(data_tasks_ids[dt_id]));
    sec_socks[dt_id] = init_sec_sock(0, 1);
    PRINTCPP(LOG_LEVEL_DESCRIBE, "New secure socket has been initialized between the Core and " << dt_type << " " << dt_name << endl);
    update_nb_data_tasks();

    // registering the secure socket
    int retcode;
    PRINTCPP(LOG_LEVEL_DESCRIBE, "Registering the secure socket with the Core" << endl);
    get_core_ehandler()->get_ulib()->register_sec_sock(get_core_id(), &retcode, sec_socks[dt_id]);
    if (!retcode)
    {
        PRINTCPP(LOG_LEVEL_ERRORS, "Error during registering of secure socket with the Core" << endl);
        return 0;
    }

    PRINTCPP(LOG_LEVEL_DESCRIBE, "Registering the secure socket with the " << dt_type << endl);
    data_tasks_ids[dt_id]->get_ulib()->register_sec_sock(data_tasks_ids[dt_id]->get_enclave_id(), &retcode, sec_socks[dt_id]);

    if (!retcode)
    {
        PRINTCPP(LOG_LEVEL_ERRORS, "Error during registering of secure socket with the Core" << endl);
        return 0;
    }

    // registering dispatchers
    string core_name = get_core_name();
    PRINTCPP(LOG_LEVEL_DESCRIBE, "Register a new dispatcher for communication from Core " << core_name << " to " << dt_type << " " << dt_name <<  endl);
    const char *core_name_c_str = core_name.c_str();
    string pubkey_name_core = get_enclave_pubkey_name(core_name);
    string pubkey_core = read_pubkey_content(pubkey_name_core);
    const char *pubkey_core_str = pubkey_core.c_str();
    data_tasks_ids[dt_id]->get_ulib()->add_new_dispatcher(
        data_tasks_ids[dt_id]->get_enclave_id(),
        get_core_trusted_id(),
        core_name_c_str, 
        strlen(core_name_c_str) + 1,
        pubkey_core_str, 
        strlen(pubkey_core_str) + 1);
    PRINTCPP(LOG_LEVEL_DESCRIBE, "Register a new dispatcher for communication from Core " << core_name << " to " << dt_type << " " << dt_name <<  endl);
    const char *dt_name_c_str = dt_name.c_str();
    string pubkey_name_dt = get_enclave_pubkey_name(dt_name);
    string pubkey_dt = read_pubkey_content(pubkey_name_dt);
    const char *pubkey_dt_str = pubkey_dt.c_str();
    get_core_ehandler()->get_ulib()->add_new_dispatcher(
        get_core_id(),
        data_tasks_ids[dt_id]->get_trusted_id(),
        dt_name_c_str,
        strlen(dt_name_c_str) + 1,
        pubkey_dt_str, 
        strlen(pubkey_dt_str) + 1);

    PRINTCPP(LOG_LEVEL_DESCRIBE, "Local attestation between Core and the " << dt_type << " " << dt_name << endl);
    int res1 = local_attestation(get_core_ehandler(), data_tasks_ids[dt_id]);
    int res2 = local_attestation(data_tasks_ids[dt_id], get_core_ehandler());

    if (!res1 && !res2)
    {
        PRINTCPP(LOG_LEVEL_DESCRIBE, "Success of mutual attestation between " << get_core_name() << " and " << dt_name << endl); 
    }
  
    PRINTCPP(LOG_LEVEL_DESCRIBE, "Loading " << dt_type << " functionalities" << endl);
 
    if (!dt_type.compare("DataTask"))
    {
        data_tasks_ids[dt_id]->get_ulib()->trusted_main(data_tasks_ids[dt_id]->get_enclave_id(), &err, dt_id);
        if (!err)
        {
            PRINTCPP(LOG_LEVEL_ERRORS, "Error during initialisation of " << dt_type << " functionalities" << endl);
            return -1;
        }
    }
    else
    {
        // note : no immediate return for DC because of loop
        call_trusted_main(dt_id);
    }

    return 1;
}

int CoreExchange::deinit_data_task(int dt_id)
{
    PRINTCPP(LOG_LEVEL_DESCRIBE, "Deinitialize the Data Task " << dt_id << endl);

    // destroy the data task
    int res = data_tasks_ids[dt_id]->deinit();
    if (res < 0)
    {
        PRINTCPP(LOG_LEVEL_ERRORS, "Error during destruction of Data Task " << dt_id << endl);
    }
    else if (res == 0)
    {
        PRINTCPP(LOG_LEVEL_ERRORS, "No Data Task to deinitialize" << endl);
    }
    else
    {
        // delete the data task handler
        delete data_tasks_ids[dt_id];
        dt_ids[dt_id] = 0;

        PRINTCPP(LOG_LEVEL_DESCRIBE, "Data Task " << dt_id << " has been deinitialized" << endl);
    }

    return res;
}

vector<int> CoreExchange::get_data_tasks_ids()
{
    vector<int> active_dt_ids;
    vector<unsigned int>::iterator it = dt_ids.begin();
    auto f = [](int x){ return x == 1; };

    while ((it = find_if(it, dt_ids.end(), f)) != dt_ids.end())
    {
        active_dt_ids.push_back(distance(dt_ids.begin(), it));
        it++;
    }

    //return (move(active_dt_ids));
    return active_dt_ids;
}

void CoreExchange::call_dt_main(unsigned int dt_id)
{
    int err = 0;
    data_tasks_ids[dt_id]->get_ulib()->trusted_main(data_tasks_ids[dt_id]->get_enclave_id(), &err, get_core_trusted_id());
}

// TODO : pass 'err' as parameter of binded function
void CoreExchange::call_trusted_main(unsigned int dt_id)
{
    std::function<void(void)> func = std::bind(&CoreExchange::call_dt_main, this, dt_id);

    auto th = thread([func]()
    { 
        func();
    });
    th.detach();
}

// TODO : fix auto-starting of threads
void CoreExchange::start_data_task(int dt_id)
{
    if (dt_ids[dt_id] == 0)
    {
        PRINTCPP(LOG_LEVEL_ERRORS, "Can't start Data Task " << dt_id <<
            " -> not initialized" << endl);
            return;
    }

    string dt_type = data_tasks_ids[dt_id]->get_enclave_type();
    string dt_name = data_tasks_ids[dt_id]->get_enclave_name();

    PRINTCPP(LOG_LEVEL_DESCRIBE, "Start " << dt_type << " " << dt_name << endl);

    if (!data_tasks_ids[dt_id]->is_thread_present("exchanger_th"))
    {
        PRINTCPP(LOG_LEVEL_DESCRIBE, "Launching packets exchanger" << endl);
        enclave_exchange_handler(dt_id);
    }


    if (!dt_type.compare("DataCollector"))
    {
        if (!data_tasks_ids[dt_id]->is_thread_present("collector_th"))
        {
            // launch packets collect handler
            PRINTCPP(LOG_LEVEL_DESCRIBE, "Launching packets collect" << endl);
            enclave_collect_handler(dt_id, 500);
        }
    }

    data_tasks_ids[dt_id]->start_enclave_threads();
    PRINTCPP(LOG_LEVEL_DESCRIBE, dt_name << " has been started" << endl);
}

void CoreExchange::stop_data_task(int dt_id)
{
    string dt_type = data_tasks_ids[dt_id]->get_enclave_type();
    string dt_name = data_tasks_ids[dt_id]->get_enclave_name();

    PRINTCPP(LOG_LEVEL_DESCRIBE, "Stop " << dt_type << " " << dt_name << endl);

    data_tasks_ids[dt_id]->stop_enclave_threads();
    PRINTCPP(LOG_LEVEL_DESCRIBE, dt_name << " has been stopped" << endl);
}

void CoreExchange::push_pkt_to_buf(unsigned int dt_id, buf_elt *pkt) 
{
    ctrl_pkt_buf[dt_id].push_back(pkt);
    update_nb_ctrl_pkt();
}

buf_elt *CoreExchange::pop_pkt_from_buf(unsigned int dt_id) 
{
    buf_elt *pkt = ctrl_pkt_buf[dt_id].at(0);
    return pkt;
}

/* ECall functions */

// TODO : catch errors
int CoreExchange::push_pkt_to_enclave(unsigned int sock_id, unsigned int dt_id, buf_elt *pkt)
{
    PRINTCPP(LOG_LEVEL_DESCRIBE, "Push packet to Core" << endl);
    int ret = 0;

    get_core_ehandler()->get_ulib()->exchange_pkt(get_core_id(), &ret, sock_id, pkt);

    if (ret)
        ctrl_pkt_buf[dt_id].pop_front();
    
    return ret;
}

// TODO : checks
int CoreExchange::pull_pkt_from_enclave(unsigned int id, string type)
{
    int ret = 0;
    if (!type.compare("control"))
    {
        //cout << "Pulling control packet from Data Task " << data_tasks_ids[id]->get_enclave_name() << endl;
        data_tasks_ids[id]->get_ulib()->retrieve_ctrl_pkt(data_tasks_ids[id]->get_enclave_id(), &ret, get_core_trusted_id());
    }

    else if (!type.compare("data"))
    {
        //cout << "Pulling data packet from Data Task" << data_tasks_ids[id]->get_enclave_name() << endl;
        get_core_ehandler()->get_ulib()->retrieve_data_pkt(get_core_id(), &ret, id);
    }

    else {
        PRINTCPP(LOG_LEVEL_ERRORS, "Unknown type of pkt" << endl);
        ret = 0;
    }

    return ret;
}

void CoreExchange::push_and_pull(unsigned int dt_id)
{
    if (ctrl_pkt_buf[dt_id].size())
    {
        buf_elt *pkt = pop_pkt_from_buf(dt_id);
        if (push_pkt_to_enclave(dt_id, dt_id, pkt) > 0) 
        {
            PRINTCPP(LOG_LEVEL_DESCRIBE, "Success to marshal packet to Core" << endl);
        }
        else
        {
            PRINTCPP(LOG_LEVEL_ERRORS, "Error during the push of control packet to Core" << endl);
        }
    }

    if (is_pkt_available_to_whom(sec_socks[dt_id]))
    {
        if (pull_pkt_from_enclave(dt_id, "control") < 0)
        {
            PRINTCPP(LOG_LEVEL_ERRORS, "Error during pulling of packet from Data Task" << endl);
        }
        else
        {
            PRINTCPP(LOG_LEVEL_ERRORS, "Succeeded in pulling the packet from Data Task" << endl);
        }
    }
    
    if (is_pkt_available_to_who(sec_socks[dt_id]))
    {
        if (pull_pkt_from_enclave(dt_id, "data") < 0)
        {
            PRINTCPP(LOG_LEVEL_ERRORS, "Error during pulling of result packet from Core" << endl);
        }
        else
        {
            PRINTCPP(LOG_LEVEL_ERRORS, "Succeeded in pulling the result packet from Core" << endl);
        }
   }
}

int CoreExchange::collect(unsigned int dt_id)
{
    int ret = 0;
    int ret2 = 0;

    get_core_ehandler()->get_ulib()->collect_data_pkt(get_core_id(), &ret, dt_id);
    data_tasks_ids[dt_id]->get_ulib()->retrieve_data_pkt(
        data_tasks_ids[dt_id]->get_enclave_id(),
        &ret2,
        get_core_trusted_id());

    return ret;
}

// return code is in "main" style -> TODO : to refactorize
int CoreExchange::local_attestation(EnclaveHandler *ea, EnclaveHandler *eb)
{
    oe_result_t result = OE_OK;
    int ret = 1;
    uint8_t *pem_key = NULL;
    size_t pem_key_size = 0;
    uint8_t *evidence = NULL;
    size_t evidence_size = 0;
    uint8_t *format_settings = NULL;
    size_t format_settings_size = 0;
    const char *ea_name = ea->get_enclave_name().c_str();
    const char *eb_name = eb->get_enclave_name().c_str();

    auto free_params = 
        [&]() ->void { 
            free(pem_key);
            free(evidence);
            free(format_settings);
        };

    PRINTCPP(LOG_LEVEL_DESCRIBE, "Attestation of " << eb_name << " to " << ea_name << endl);

    PRINTCPP(LOG_LEVEL_DESCRIBE, "Requesting " << ea_name << " target info" << endl);
    result = ea->get_ulib()->get_enclave_format_settings(
        ea->get_enclave_id(),
        &ret,
        eb->get_trusted_id(),
        &format_settings,
        &format_settings_size);
    if ((result != OE_OK) || (ret != 0))
    {
        PRINTCPP(LOG_LEVEL_ERRORS, "get_format_settings failed : " << oe_result_str(result) << endl);
        if (ret == 0)
        {
            free_params();
            return 0;
        }
    }

    result = eb->get_ulib()->get_targeted_evidence_with_public_key(
        eb->get_enclave_id(),
        &ret,
        ea->get_trusted_id(),
        format_settings,
        format_settings_size, 
        &pem_key,
        &pem_key_size,
        &evidence,
        &evidence_size);

    if ((result != OE_OK) || (ret != 0))
    {
        PRINTCPP(LOG_LEVEL_ERRORS, "get_evidence_report_with_pubkey failed : " << oe_result_str(result));
        if (ret == 0)
        {
            free_params();
            return 0;
        }
    }

    // debug
    // cout << eb->get_enclave_name() << "'s public key : " << endl << pem_key << endl;

    PRINTCPP(LOG_LEVEL_DESCRIBE, "verify_evidence_and_set_public_key in " << ea_name << endl);
    result = ea->get_ulib()->verify_evidence_and_set_public_key(
        ea->get_enclave_id(), 
        &ret,
        eb->get_trusted_id(),
        pem_key,
        pem_key_size,
        evidence,
        evidence_size);
    if ((result != OE_OK) || (ret != 0))
    {
        PRINTCPP(LOG_LEVEL_ERRORS, "verify_evidence_and_set_public_key failed : " << oe_result_str(result) << endl);
        if (ret == 0)
        {
            free_params();
            return 0;
        }
    }

    PRINTCPP(LOG_LEVEL_DESCRIBE, "Success of attestation of " << eb_name << " by " << ea_name << endl);

    free_params();
    return ret;
}

void CoreExchange::enclave_exchange_handler(unsigned int dt_id)
{
    std::function<void(void)> func = std::bind(&CoreExchange::push_and_pull, this, dt_id);

    auto th = thread([func]()
    { 
        while (true)
        {
            func();
        }
    });
    data_tasks_ids[dt_id]->add_enclave_thread(th, "exchanger_th");
}

void CoreExchange::enclave_collect_handler(unsigned int dt_id, unsigned int interval)
{
    std::function<int(void)> func = std::bind(&CoreExchange::collect, this, dt_id);

    auto th = thread([func, interval]()
    { 
        while (true)
        {
            auto x = std::chrono::steady_clock::now() + std::chrono::milliseconds(interval);
            func();
            std::this_thread::sleep_until(x);
        }
    });
    data_tasks_ids[dt_id]->add_enclave_thread(th, "collector_th");
}

int CoreExchange::load_table_db(const char *filename)
{
    int ret;
    /* Allocations are in convert_db_data */
    char *tablename;
    char *format;
    char **types;
    char ***data;
 
    unsigned int size_tablename;
    unsigned int size_format;
    unsigned int nb_rows;
    unsigned int nb_columns;

    PRINTCPP(LOG_LEVEL_DESCRIBE, "Reading csv file" << endl);
    convert_db_data(
        filename,
        &tablename,
        &format,
        &types,
        &data,
        &size_tablename,
        &size_format,
        &nb_rows,
        &nb_columns);

    PRINTCPP(LOG_LEVEL_DESCRIBE, "Loading table " << filename << " in Core database" << endl);
    get_core_ehandler()->get_ulib()->load_table_db(
        get_core_id(),
        &ret,
        tablename,
        format,
        types,
        data,
        size_tablename,
        size_format,
        nb_rows,
        nb_columns);


    // freeing
    free(tablename);
    free(format);

    for (unsigned int i = 0; i < nb_columns; i++)
        free(types[i]);
    free(types);
    for (unsigned int i = 0; i < nb_rows; i++)
    {
        for (unsigned int j = 0; j < nb_columns; j++)
        {
            free(data[i][j]);
        }
        free(data[i]);
    }
    free(data);
    
    //TODO : to debug -> free corruption raised by compiler
    // for (i = 0; i < MAX_OCALL_NB_PARAMS; i++)
    // {
    //     for (int j = 0; j < MAX_OCALL_SUB_PARAM_LEN; j++)
    //         free(data[i][j]);
    // }
    //     free(data[i]);
    // }
    // free(data);

    return ret;
}

int CoreExchange::load_enc_table_db(const char *filename)
{
    int ret;
    PRINTCPP(LOG_LEVEL_DESCRIBE, "Loading encrypted table " << filename << " in Core database" << endl);
    get_core_ehandler()->get_ulib()->load_enc_table_db(
        get_core_id(),
        &ret,
        filename);

    if (!ret)
    {
        PRINTCPP(LOG_LEVEL_ERRORS, "Loading of encrypted table failed" << endl);
    }
    else
    {
        PRINTCPP(LOG_LEVEL_DESCRIBE, "Encryted table has been loaded in Core database" << endl);
    }
    return ret;
}

int CoreExchange::exec_core_computation(const char* params)
{
    int ret;
    get_core_ehandler()->get_ulib()->exec_core_computation(
        get_core_id(),
        &ret,
        params,
        strlen(params)+1);
    return ret;
}

int CoreExchange::exec_sql_and_store(const char* params)
{
    int ret;
    get_core_ehandler()->get_ulib()->exec_sql_and_store(
        get_core_id(),
        &ret,
        params,
        strlen(params)+1);
    return ret;
}

int CoreExchange::free_stored_sql()
{
    int ret;
    get_core_ehandler()->get_ulib()->free_stored_sql(
        get_core_id(),
        &ret);
    return ret;
}

int CoreExchange::compute_on_stored_data(const char* params)
{
    int ret;
    get_core_ehandler()->get_ulib()->compute_on_stored_data(
        get_core_id(),
        &ret,
        params,
        strlen(params)+1);
    return ret;
}

char ***parsedData = NULL;
unsigned int nb_rows_stored = 0;
unsigned int nb_columns_stored = 0;
int CoreExchange::custom_data_load(std::string params)
{
    int ret, flagFirst = 0;
    std::istringstream ss(params);
	std::string token;
	std::vector<std::string> vec;
	while(std::getline(ss, token, '|')) {
		vec.push_back(token);
	}

    const char* filename = vec[0].c_str();
    unsigned int maxRows = stoi(vec[1]);
    if(vec.size() == 3 && vec[2] == "FREE")
    {
        for (unsigned int i = 0; i < nb_rows_stored; i++)
        {
            for (unsigned int j = 0; j < nb_columns_stored; j++)
            {
                free(parsedData[i][j]);
            }
            free(parsedData[i]);
        }
        free(parsedData);
        parsedData = NULL;
        nb_rows_stored = 0;
        nb_columns_stored = 0;
        return 1;
    }


    char *tablename;
    char *format;
    char **types;

    unsigned int size_tablename;
    unsigned int size_format;
    if (parsedData == NULL)
    {
        /* Allocations are in convert_db_data */

        PRINTCPP(LOG_LEVEL_DESCRIBE, "Reading csv file" << endl);
        convert_db_data(
            filename,
            &tablename,
            &format,
            &types,
            &parsedData,
            &size_tablename,
            &size_format,
            &nb_rows_stored,
            &nb_columns_stored);
        flagFirst = 1;
    }

    get_core_ehandler()->get_ulib()->custom_data_load(
        get_core_id(),
        &ret,
        parsedData,
        maxRows,
        nb_columns_stored);

    // freeing
    if(flagFirst)
    {
        free(tablename);
        free(format);

        for (unsigned int i = 0; i < nb_columns_stored; i++)
            free(types[i]);
        free(types);
    }

    return ret;
}

/* Tools */

int get_available_id(vector<unsigned int> ids)
{
    int id = 0;
    auto it = find(ids.begin(), ids.end(), 0);
 
    if (it != ids.end()) 
    {
        id = distance(ids.begin(), it);
        PRINTCPP(LOG_LEVEL_DEBUG, "Id " << id << " is available" << endl);
    }
    else {
        PRINTCPP(LOG_LEVEL_ERRORS, "No id available" << endl);
        id = -1;
    }

    return id;
}

int get_id_by_name(map<string, pair<int, int> > mports, string ename)
{
    // check if key exist
    auto it = mports.find(ename);

    if (it == mports.end())
    {
        PRINTCPP(LOG_LEVEL_ERRORS, "Enclave " << ename << " doesn't exist" << endl);
        return -1;
    }

    int id = mports[ename].second;
    PRINTCPP(LOG_LEVEL_DEBUG, "Id of enclave " << ename << " : " << id << endl);
    
    return id;
}