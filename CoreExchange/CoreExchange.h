#ifndef _CORE_EXCHANGE_H_
#define _CORE_EXCHANGE_H_

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <vector>
#include <string>
#include <boost/circular_buffer.hpp>
#include "ControlPacket.h"
#include <msgpack.hpp>
#include <zmqpp/zmqpp.hpp>
#include "SecureSocket.h"
#include <dlfcn.h>
#include <openenclave/host.h>

#define MAX_DATA_TASKS 5
#define MAX_NB_ENCLAVES 10
        

// TODO : to refactorize using auto-generation of typedef

/* enclave properties */
typedef void (*set_enclave_name_t)(oe_enclave_t *, const char *, size_t);

/* attributes of enclave dispatcher */
typedef void (*add_new_dispatcher_t)(oe_enclave_t *, unsigned int, const char *, size_t,
    const char *, size_t);

/* creation of data tasks enclave */
typedef oe_result_t (*oe_create_enclave_t)(const char *,
    oe_enclave_type_t, uint32_t, const oe_enclave_setting_t *,
    uint32_t setting_count, oe_enclave_t **);

/* local attestation */
typedef oe_result_t (*get_enclave_format_settings_t)(oe_enclave_t *, int *, unsigned int, uint8_t **, size_t *);
typedef oe_result_t (*get_targeted_evidence_with_public_key_t)(
    oe_enclave_t *, int *, unsigned int, uint8_t *, size_t, uint8_t **, size_t *, uint8_t **, size_t *);
typedef oe_result_t (*verify_evidence_and_set_public_key_t)(oe_enclave_t *, int *, unsigned int, uint8_t *, size_t, uint8_t *, size_t);

/* registering of socket */
typedef oe_result_t (*register_sec_sock_t)(oe_enclave_t *, int *, secure_socket *);

/* packet exchanging */
typedef oe_result_t (*exchange_pkt_t)(oe_enclave_t *, int *, unsigned int, buf_elt *pkt);
typedef oe_result_t (*retrieve_pkt_t)(oe_enclave_t *, int *, unsigned int);

/* launching of core functionalities */
typedef oe_result_t (*trusted_main_t)(oe_enclave_t *, int *, unsigned int);

/* data collection */
typedef oe_result_t (*collect_data_pkt_t)(oe_enclave_t *, int *, unsigned int);

// TODO : think about a clever solution for oe shared library
/* oe inheritance adapted for dynamic loading */
typedef oe_result_t (*oe_terminate_enclave_t)(oe_enclave_t *);

/* database */
typedef oe_result_t (*load_table_db_t)(
    oe_enclave_t *,
    int *,
    char *,
    char *,
    char **,
    char ***,
    unsigned int,
    unsigned int,
    unsigned int,
    unsigned int);
typedef oe_result_t (*load_enc_table_db_t)(
    oe_enclave_t *,
    int *,
    const char *);

typedef oe_result_t (*exec_core_computation_t)(
    oe_enclave_t*,
    int*,
    const char *,
    unsigned int
);

typedef oe_result_t (*exec_sql_and_store_t)(
    oe_enclave_t*,
    int*,
    const char *,
    unsigned int
);

typedef oe_result_t (*free_stored_sql_t)(
    oe_enclave_t*,
    int*
);

typedef oe_result_t (*compute_on_stored_data_t)(
    oe_enclave_t*,
    int*,
    const char *,
    unsigned int
);

typedef oe_result_t (*custom_data_load_t)(
    oe_enclave_t*,
    int*,
    char ***,
    unsigned int,
    unsigned int
);

/* enclaves' functions */
// core
static std::vector<std::string> core_func = {
    "oe_terminate_enclave",
    "set_enclave_name", 
    "add_new_dispatcher",
    "register_sec_sock",
    "exchange_pkt",
    "retrieve_data_pkt",
    "collect_data_pkt",
    "get_enclave_format_settings",
    "get_targeted_evidence_with_public_key",
    "verify_evidence_and_set_public_key",
    "load_table_db",
    "load_enc_table_db",
    "exec_core_computation",
    "exec_sql_and_store",
    "free_stored_sql",
    "compute_on_stored_data",
    "custom_data_load",
    "trusted_main"};
// data task
static std::vector<std::string> dt_func = {
    "oe_terminate_enclave",
    "set_enclave_name", 
    "add_new_dispatcher",
    "register_sec_sock",
    "retrieve_ctrl_pkt",
    "get_enclave_format_settings", 
    "get_targeted_evidence_with_public_key",
    "verify_evidence_and_set_public_key", 
    "trusted_main"};
// data collector
static std::vector<std::string> dc_func = {
    "oe_terminate_enclave",
    "set_enclave_name",
    "add_new_dispatcher",
    "register_sec_sock",
    "retrieve_data_pkt",
    "retrieve_ctrl_pkt",
    "get_enclave_format_settings",
    "get_targeted_evidence_with_public_key",
    "verify_evidence_and_set_public_key",
    "trusted_main"};


// TODO : refactorize using a map
class UntrustedLib
{
    private:
        oe_enclave_t *enclave_id;
        void *ulib;
        std::string oe_create_fname;
    public:
        UntrustedLib(oe_enclave_t *eid) { this->enclave_id = eid; }
        ~UntrustedLib(){ dlclose(this->ulib); PRINTCPP(LOG_LEVEL_DEBUG, "UntrustedLib destructor" << std::endl); }
        unsigned int load_ulib(const char *ulibname);
        unsigned int load_ufunc(std::vector<std::string> &ufuncnames);
        void set_oe_create_fname(std::string create_fname) { this->oe_create_fname = create_fname; }
        std::string get_oe_create_fname() { return this->oe_create_fname; }
        oe_create_enclave_t oe_create_enclave;
        oe_terminate_enclave_t oe_terminate_enclave;
        set_enclave_name_t set_enclave_name;
        add_new_dispatcher_t add_new_dispatcher;
        register_sec_sock_t register_sec_sock;
        retrieve_pkt_t retrieve_ctrl_pkt;
        retrieve_pkt_t retrieve_data_pkt;
        exchange_pkt_t exchange_pkt;
        get_enclave_format_settings_t get_enclave_format_settings;
        get_targeted_evidence_with_public_key_t get_targeted_evidence_with_public_key;
        verify_evidence_and_set_public_key_t verify_evidence_and_set_public_key;
        collect_data_pkt_t collect_data_pkt;
        trusted_main_t trusted_main;
        load_table_db_t load_table_db;
        load_enc_table_db_t load_enc_table_db;
        exec_core_computation_t exec_core_computation;
        exec_sql_and_store_t exec_sql_and_store;
        free_stored_sql_t free_stored_sql;
        compute_on_stored_data_t compute_on_stored_data;
        custom_data_load_t custom_data_load;
};

class EnclaveHandler
{
    private:
        std::string enclave_name;
        std::string enclave_type;
        std::string enclave_pathname;
        std::string pubkey_name;
        oe_enclave_t *enclave_id = NULL;
        unsigned int trusted_id = 0;
        oe_enclave_t **enclave_p_id = &enclave_id;
        UntrustedLib *ulib = NULL;
        std::map<std::string, std::pair<std::thread, std::thread::native_handle_type> > enclave_threads;
    public:
        //EnclaveHandler(std::string ename, oe_enclave_t *eid) { this->enclave_name = ename; this->enclave_id = eid; }
        EnclaveHandler(std::string ename, oe_enclave_t *eid);
        ~EnclaveHandler();
        int deinit();
        std::string get_enclave_name() { return this->enclave_name; }
        std::string get_enclave_type() { return this->enclave_type; }
        std::string get_enclave_pathname() { return this->enclave_pathname; }
        std::string get_enclave_pubkey_name() { return this->pubkey_name; }
        void set_enclave_name(std::string ename) { this->enclave_name = ename; }
        void set_enclave_type(std::string etype) { this->enclave_type = etype; }
        void set_trusted_id(unsigned int id) { this->trusted_id = id; }
        void set_enclave_pubkey_name(std::string pubkey_name) {this->pubkey_name = pubkey_name; }
        oe_enclave_t *get_enclave_id() { return this->enclave_id; }
        oe_enclave_t **get_enclave_p_id() { return this->enclave_p_id; }
        unsigned int get_trusted_id() {return this->trusted_id; }
        void set_ulib(UntrustedLib *ulib) { this->ulib = ulib; }
        UntrustedLib *get_ulib() { return this->ulib; }
        int is_thread_present(std::string th_name);
        void add_enclave_thread(std::thread &th, std::string th_name);
        void del_enclave_thread(std::string th_name);
        void del_enclave_threads();
        void start_enclave_threads();
        void stop_enclave_threads();
};

class CoreExchange
{
    private :
        EnclaveHandler *ehandler;
        unsigned int nb_data_tasks = 0;
        unsigned int nb_ctrl_pkt = 0;
        boost::circular_buffer<buf_elt *> ctrl_pkt_buf[MAX_DATA_TASKS];
        EnclaveHandler *data_tasks_ids[MAX_DATA_TASKS];
        secure_socket *sec_socks[MAX_DATA_TASKS];
    public :
    	CoreExchange(EnclaveHandler *ehandler) { this->ehandler = ehandler; for (unsigned int i = 0; i < MAX_DATA_TASKS; i++) this->ctrl_pkt_buf[i].set_capacity(10);}
    	~CoreExchange(){ PRINTCPP(LOG_LEVEL_DEBUG, "CoreExchange destructor" << std::endl); }
    	int init();
        int deinit();
        int init_data_task(int dt_id, std::string dt_name, std::string dt_type);
        int deinit_data_task(int dt_id);
        std::vector<int> get_data_tasks_ids();
        void call_dt_main(unsigned int dt_id);
        void call_trusted_main(unsigned int dt_id);
        void start_data_task(int dt_id);
        void stop_data_task(int dt_id);
        std::string get_core_name() { return this->ehandler->get_enclave_name();}
        std::string get_core_pathname() {return this->ehandler->get_enclave_pathname();}
        oe_enclave_t *get_core_id() { return this->ehandler->get_enclave_id(); }
        oe_enclave_t **get_core_p_id() { return this->ehandler->get_enclave_p_id(); }
        unsigned int get_core_trusted_id() { return this->ehandler->get_trusted_id(); }
        EnclaveHandler *get_core_ehandler() { return this->ehandler; }
        void update_nb_ctrl_pkt() { this->nb_ctrl_pkt++; }
        unsigned int get_nb_data_tasks() { return this->nb_data_tasks; }
        void update_nb_data_tasks() { this->nb_data_tasks++; }
        // EnclaveHandler **get_data_tasks_ids() { return this->data_tasks_ids; }
        void push_pkt_to_buf(unsigned int dt_id, buf_elt *pkt);
        buf_elt *pop_pkt_from_buf(unsigned int dt_id);
        int push_pkt_to_enclave(unsigned int sock_id, unsigned int dt_id, buf_elt *pkt);
        int pull_pkt_from_enclave(unsigned int id, std::string type);
        void push_and_pull(unsigned int dt_id);
        int collect(unsigned int dt_id);
        int local_attestation(EnclaveHandler *ea, EnclaveHandler *eb);
        void enclave_exchange_handler(unsigned int dt_id);
        void enclave_collect_handler(unsigned int dt_id, unsigned int interval);
        int load_table_db(const char *filename);
        int load_enc_table_db(const char *filename);
        int exec_core_computation(const char* params);
        int exec_sql_and_store(const char* params);
        int free_stored_sql();
        int compute_on_stored_data(const char* params);
        int custom_data_load(std::string params);
};


/* Tools */

int get_available_id(std::vector<unsigned int> ids);
int get_id_by_name(std::map<std::string, std::pair<int, int> > mports,
    std::string ename);


#endif
