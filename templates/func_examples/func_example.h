typedef struct {
	unsigned int nb_columns;
	unsigned int nb_rows;
	int *a;
	int *b;
	float *c;
} funcres_t;

funcres_t *func1db(unsigned int nb_columns, unsigned int nb_rows, int *a, int *b, float *c);
