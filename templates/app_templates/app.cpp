#include <zmqpp/zmqpp.hpp>
#include "AcqServer.h"
#include "ControlPacket.h"


int main(int argc, char *argv[])
{
    // register acqunit for this app
    zmqpp::context context;
    zmqpp::socket socket(context, zmqpp::socket_type::push);
    register_new_acqunit(socket, "DATA_TASK_NAME");

    // initialize function arguments
    char fname[] = "func1db";
    unsigned int nb_func_args = 1;

    // build control packet
    // availables db request modes :
    //     - get all data : 0
    //     - get data1 : 1
    //     - get data2 : 2
    auto v = build_random_t<int *>("int", 1, 0, 0); // db request mode -> use of vector only to be compatible with serialization
    ctrl_pkt_to_send *pkt = new_ctrl_pkt(nb_func_args, fname, {sizeof(int)}, v);
    size_t size_pkt = sizeof(ctrl_pkt_to_send) + strlen(fname) + sizeof(int);

    // send packet
    zmqpp::message *msg = new zmqpp::message(size_pkt);
    msg->add_raw(reinterpret_cast<char *>(pkt), size_pkt);
    socket.send(*msg);
    delete msg;

    socket.close();
    return 0;
}

