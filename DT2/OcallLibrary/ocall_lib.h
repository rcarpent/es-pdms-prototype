#ifndef OCALL_LIB_H_
#define OCALL_LIB_H_

#if defined(__cplusplus)
extern "C" {
#endif

void ocall_print_string(const char *str);


#if defined(__cplusplus)
}
#endif

#endif