#include "DT2.h"
#include "CommonEnclave.h"


funcres_t *func1db(unsigned int nb_columns, unsigned int nb_rows, int *a, int *b, float *c) 
{
    PRINT(LOG_LEVEL_DESCRIBE, "DT2 : in function func1db\n");
    PRINT(LOG_LEVEL_DESCRIBE, "Arguments : %d, %d, %d, %d, %d, %d, %f, %f, %f\n", a[0], a[1], a[2],  b[0], b[1], b[2], c[0], c[1], c[2]);
    PRINT(LOG_LEVEL_DESCRIBE, "Processing the sum of %d and %d\n", a[0], b[2]);
    PRINT(LOG_LEVEL_DESCRIBE, "Processing the product of %d and %d\n", a[1], b[2]);
    PRINT(LOG_LEVEL_DESCRIBE, "Processing the sum of %f and %f\n", c[1], c[2]);
    funcres_t *res = (funcres_t *)malloc(sizeof(funcres_t));
    res->nb_columns = 3;
    res->nb_rows = 1;
    res->a = (int *)malloc(sizeof(int));
    res->b = (int *)malloc(sizeof(int));
    res->c = (float *)malloc(sizeof(float));
    res->a[0] = a[0] + b[2];
    res->b[0] = a[1] * b[2];
    res->c[0] = c[1] + c[2];
    PRINT(LOG_LEVEL_DESCRIBE, "Result of func1db to submit : [%d, %d, %f]\n", res->a[0], res->b[0], res->c[0]);

    return res;
}

funcres2_t *func2db(unsigned int nb_columns, unsigned int nb_rows, int *a)
{
    PRINT(LOG_LEVEL_DESCRIBE, "DT2: in function func2db\n");
    PRINT(LOG_LEVEL_DESCRIBE, "Arguments : %d, %d\n", a[0], a[1], a[2]);
    PRINT(LOG_LEVEL_DESCRIBE, "Processing the sum of %d and %d\n", a[0], a[1]);
    funcres2_t *res = (funcres2_t *)malloc(sizeof(funcres2_t));
    res->nb_columns = 1;
    res->nb_rows = 1;
    res->a = (int *)malloc(sizeof(int));
    res->a[0] = a[0] + a[1];
    PRINT(LOG_LEVEL_DESCRIBE, "Result of func2db to submit : [%d]\n", res->a[0]);

    return res;
}

funcres3_t *func3db(unsigned int nb_columns, unsigned int nb_rows, int *a, int *b, float *c)
{
    PRINT(LOG_LEVEL_DESCRIBE, "DT2: in function func3db\n");
    PRINT(LOG_LEVEL_DESCRIBE, "Arguments : ");
    for (unsigned int i = 0; i < nb_rows; i++)
        PRINT(LOG_LEVEL_DESCRIBE, "\n\t%d, %d, %f", a[i], b[i], c[i]);
    PRINT(LOG_LEVEL_DESCRIBE, "\n");
    PRINT(LOG_LEVEL_DESCRIBE, "Processing the product of %d and %f\n", a[0], c[1]);
    PRINT(LOG_LEVEL_DESCRIBE, "Processing the sum of %d and %d\n", a[1], b[1]);
    funcres3_t *res = (funcres3_t *)malloc(sizeof(funcres3_t));
    res->nb_columns = 2;
    res->nb_rows = 2;
    res->a = (float *)malloc(sizeof(float) * 2);
    res->a[0] = static_cast<float>(a[0]) * c[1];
    res->a[1] = static_cast<float>(a[1] * b[1]);
    PRINT(LOG_LEVEL_DESCRIBE, "Result of func3db to submit : [%f,  %f]\n", res->a[0], res->a[1]);

    return res;
}

int trusted_main(unsigned int sock_id)
{
    PRINT(LOG_LEVEL_DESCRIBE, "Nothing to do in main of DT2\n");
    return 1;
}