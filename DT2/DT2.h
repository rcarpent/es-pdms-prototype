#ifndef _DT2_H_
#define _DT2_H_


typedef struct {
	unsigned int nb_columns;
	unsigned int nb_rows;
	int *a;
	int *b;
	float *c;
} funcres_t;

typedef struct {
	unsigned int nb_columns;
	unsigned int nb_rows;
	int *a;
} funcres2_t;

typedef struct {
	unsigned int nb_columns;
	unsigned int nb_rows;
	float *a;
} funcres3_t;


funcres_t *func1db(unsigned int nb_columns, unsigned int nb_rows, int *a, int *b, float *c);
funcres2_t *func2db(unsigned int nb_columns, unsigned int nb_rows, int *a);
funcres3_t *func3db(unsigned int nb_columns, unsigned int nb_rows, int *a, int *b, float *c);


#endif
