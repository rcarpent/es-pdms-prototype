#include <iostream>
#include <string>
#include <msgpack.hpp>
#include "AcqServer.h"

using namespace std;


/* serialization/deserialization */

zmqpp::message *pack_data(ctrl_pkt &pkt)
{
    msgpack::sbuffer sbuf;
    msgpack::pack(sbuf, pkt);
    zmqpp::message *msg = new zmqpp::message(sbuf.size());
    msg->add_raw(sbuf.data(), sbuf.size());
    return msg;
}

void unpack_data(zmqpp::message &message, ctrl_pkt &pkt)
{
    msgpack::unpacked unpacked_data;
    msgpack::unpack(unpacked_data, static_cast<const char*>(message.raw_data(1)), message.size(1));
    unpacked_data.get().convert(pkt);
    PRINTCPP(LOG_LEVEL_DESCRIBE, "Received control packet : [" << pkt.cmd << ", " << pkt.arg1 << ", " << pkt.arg2 << "]" << endl);
}

/* network */

void connect_to_server(zmqpp::socket &socket, int port)
{
    const string endpoint = "tcp://127.0.0.1:" + std::to_string(port);
    PRINTCPP(LOG_LEVEL_DESCRIBE,"Opening connection to " << endpoint << "..." << endl);
    socket.connect(endpoint);
}

void new_pull_server(zmqpp::socket &socket, int port)
{
 	const string endpoint = "tcp://*: " + std::to_string(port);
    PRINTCPP(LOG_LEVEL_DESCRIBE, "Binding to " << endpoint << endl);
    socket.bind(endpoint);
}

void acquire_pkt(zmqpp::socket &socket, ctrl_pkt &pkt)
{
    zmqpp::message message;
    socket.receive(message);

    // testing only
    // const char *data = static_cast<const char*>(message.raw_data(1));

    // struct cpkt {
    //     size_t size_str1;
    //     size_t size_str2;
    //     char *str1;
    //     char *str2;
    // };

    // char *str1 = (char *)malloc(((struct cpkt *)data)->size_str1);
    // char *str2 = (char *)malloc(((struct cpkt *)data)->size_str2);
    // memcpy(str1, data + 16, ((struct cpkt *)data)->size_str1);
    // memcpy(str2, data + 16 + ((struct cpkt *)data)->size_str1 + 1, ((struct cpkt *)data)->size_str2);
    // std::cout << "Acquired message0 : " << str1 << ", " << str2 << std::endl;
    // std::cout << "Acquired message1 : " << ((struct cpkt *)data)->size_str1 << ", " << ((struct cpkt *)data)->size_str2 << std::endl;

    // unpack the data
    unpack_data(message, pkt);
}

void init_acq_server(zmqpp::socket &socket, int port)
{
	new_pull_server(socket, port);
}

// TODO : delete unused objects + return a retcode
void register_new_acqunit(zmqpp::socket &socket_acq, string dt_name)
{
   // connect to Control Module
    zmqpp::context context_ctrl;
    zmqpp::socket socket_ctrl(context_ctrl, zmqpp::socket_type::request);
    connect_to_server(socket_ctrl, CTRL_MODULE_PORT);

    // request the registering of a new acqunit
    // TODO : fix optionnal member of struct ctrl_pkt
    ctrl_pkt request = { "new_acqunit", dt_name, "0"};
    zmqpp::message *request_msg = pack_data(request);
    socket_ctrl.send(*request_msg);
    delete(request_msg);
    ctrl_pkt reply;
    acquire_pkt(socket_ctrl, reply);
    int port_acq = stoi(reply.arg1);
    PRINTCPP(LOG_LEVEL_DESCRIBE, "Acq : port received from control module : " << port_acq << endl);
    socket_ctrl.close();

    // establish connection with the acqunit acquired
    connect_to_server(socket_acq, port_acq);
}
