#ifndef _ACQ_SERVER_H_
#define _ACQ_SERVER_H_

#include <zmqpp/zmqpp.hpp>
//#include "ctrl_pkt.h"
#include "ControlPacket.h"
#include "log.h"

#if defined(__cplusplus)
extern "C" {
#endif

#define CTRL_MODULE_PORT 4831
#define DTB_PORT 4832


/* serialization/deserialization */

zmqpp::message *pack_data(ctrl_pkt &pkt);
void unpack_data(zmqpp::message &message, ctrl_pkt &pkt);


/* network */

void connect_to_server(zmqpp::socket &socket, int port);
void new_pull_server(zmqpp::socket &socket, int port);
//void acquire_pkt(zmqpp::context &context, zmqpp::socket &socket, ctrl_pkt &pkt);
void acquire_pkt(zmqpp::socket &socket, ctrl_pkt &pkt);
void init_acq_server(zmqpp::socket &socket, int port);
void register_new_acqunit(zmqpp::socket &socket_acq, std::string dt_name);

#if defined(__cplusplus)
}
#endif

#endif
