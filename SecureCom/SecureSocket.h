#ifndef SECURE_SOCKET_H_
#define SECURE_SOCKET_H_

#include "CircularBuffer.h"


typedef struct
{
	unsigned int who;
	unsigned int whom;
	cbuf_handle_t incoming_cbuf;
	cbuf_handle_t outcoming_cbuf;
} secure_socket;

secure_socket *init_sec_sock(unsigned int who, unsigned int whom);
void push_pkt_who(secure_socket *sock, buf_elt *elt);
void push_pkt_whom(secure_socket *sock, buf_elt *elt);
buf_elt *pull_pkt_who(secure_socket *sock);
buf_elt *pull_pkt_whom(secure_socket *sock);
bool is_pkt_available_to_who(secure_socket *sock);
bool is_pkt_available_to_whom(secure_socket *sock);

#endif
