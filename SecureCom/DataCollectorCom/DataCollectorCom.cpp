#include "DataCollectorCom.h"
#include <string.h>

#define MAX_COLLECTOR_BUF_SIZE 2048


struct data_collector_ws *dc_ws[MAX_DATA_COLLECTOR_WS];


/* collectors */ 

struct data_collector_ws *new_data_collector_ws()
{
    struct data_collector_ws *ws = (struct data_collector_ws *)malloc(sizeof(struct data_collector_ws));
    memset(ws, 0, sizeof(struct data_collector_ws));
    buf_elt *buffer = (buf_elt *)malloc(sizeof(buf_elt) * MAX_COLLECTOR_BUF_SIZE);
    ws->buffer = circular_buf_init(buffer, MAX_COLLECTOR_BUF_SIZE);
    ws->dsize = 0;

    return ws; 
}

// struct data_collector_ws **new_data_collectors_ws(int nb_collectors)
// {
//     struct data_collector_ws **dc_ws = (struct data_collector_ws **)malloc(nb_collectors * sizeof(struct data_collector_ws *));
//     printf("New data collectors ws\n");
//     for (int i = 0; i < nb_collectors; i++)
//         dc_ws[i] = new_data_collector_ws();

//     return dc_ws;
// }

int register_data_collector(unsigned sock_id)
{
    struct data_collector_ws *ws = new_data_collector_ws();
    dc_ws[sock_id] = ws;

    return 1;
}

// void add_data_collector_ws(
//     struct data_collector_ws **dc_ws,
//     struct data_collector_ws *ws,
//     int sock_id)
// {
//     dc_ws[sock_id] = ws;
// }

struct data_collector_ws *get_data_collector_ws(unsigned int sock_id)
{
    return dc_ws[sock_id];
}


/* data acquisition */

int acquire_data(unsigned int sock_id, uint8_t *result, int size) 
{
    // retrieve data collector workspace
    struct data_collector_ws *ws = get_data_collector_ws(sock_id);
    if (ws == NULL)
    {
        PRINT(LOG_LEVEL_ERRORS, "No data collector workspace is available for sockid %d\n", sock_id);
        return -1;
    }

    int res = read_nb(ws, result, size);

    PRINT(LOG_LEVEL_DEBUG, "Acquired data : %s\n", (char *)result);

    if (!res)
    {
        // TODO : print error code as string
        PRINT(LOG_LEVEL_ERRORS, "Warning : no data acquired : err_code %d\n", res);
        return 0;
    }

    return res;
}

int push_data_to_dc(struct data_collector_ws *ws, buf_elt *elt)
{
    circular_buf_put(ws->buffer, elt);
    ws->dsize += elt->data_size;
    PRINT(LOG_LEVEL_DESCRIBE, "Push %ld bytes to the data collector workspace of amount = %ld\n",
        elt->data_size, ws->dsize);
    
    free_buf_elt(elt);

    return 1;
}

int push_data_pkt(unsigned int sock_id, ctrl_pkt *pkt)
{
    PRINT(LOG_LEVEL_DESCRIBE, "Push converted data in socket %d\n", sock_id);

    // convert control packet to buffer element
    uint8_t *p = reinterpret_cast<uint8_t *>(pkt);
    size_t p_size = sizeof(ctrl_pkt) + pkt->size_fname + pkt->size_params;
    buf_elt *elt = new_buf_elt(p, p_size);
    // encrypt result
    buf_elt *encrypted_elt = encrypt_pkt(sock_id, elt);

    if (encrypted_elt == NULL)
    {
        PRINT(LOG_LEVEL_ERRORS,
            "Error during publishing of data packet\
            -> encryption failed\n");
        return -1;
    }

    // push to socket
    secure_socket *sock = get_sec_sock(sock_id);

    if (!sock)
    {
        PRINT(LOG_LEVEL_ERRORS,
        "Error : can't push data packet because\
         secure socket has not been initialized\n");
        return -1;
    }

    push_pkt_who(sock, encrypted_elt);
    
    return 1;
}

int retrieve_data_pkt(unsigned int sock_id)
{
    PRINT(LOG_LEVEL_DESCRIBE, "Retrieving data packet of collector on sock_id %d\n", sock_id);

    // retrieve packet
    secure_socket *sock = get_sec_sock(sock_id);

    if (!sock)
    {
        PRINT(LOG_LEVEL_ERRORS,
            "Error : can't retrieve data packet because\
            secure socket has not been initialized\n");
        return -1;
    }


    buf_elt *encrypted_elt = pull_pkt_who(sock);

    if (encrypted_elt == NULL)
    {
        PRINT(LOG_LEVEL_DEBUG, "No packet has been pulled\n");
        return 0;
    }

    // decrypt packet
    buf_elt *elt = decrypt_pkt(sock_id, encrypted_elt);

    if (elt == NULL)
    {
        PRINT(LOG_LEVEL_ERRORS,
            "Error during retrieving of data packet\
            -> decryption failed\n");
        return -1;
    }
    else {
        PRINT(LOG_LEVEL_DESCRIBE, "Success to decrypt data packet\n");
    }

    // retrieve data collector workspace
    struct data_collector_ws *ws = get_data_collector_ws(sock_id);
    if (ws == NULL)
    {
        PRINT(LOG_LEVEL_ERRORS, "No data collector workspace is available for sockid %d\n", sock_id);
        return -1;
    }

    // push new data to data_collector_ws
    push_data_to_dc(ws, elt);

    //freeing

    return 1;
}

int submit_req(unsigned int sock_id, ctrl_pkt *pkt)
{
    PRINT(LOG_LEVEL_DESCRIBE, "Submit request in socket %d\n", sock_id);

    // convert control packet to buffer element
    uint8_t *p = reinterpret_cast<uint8_t *>(pkt);
    size_t p_size = sizeof(ctrl_pkt) + pkt->size_fname + pkt->size_params;
    buf_elt *elt = new_buf_elt(p, p_size);
    // encrypt result
    buf_elt *encrypted_elt = encrypt_pkt(sock_id, elt);

    if (encrypted_elt == NULL)
    {
        PRINT(LOG_LEVEL_ERRORS, "Error during submission of data packet\
            -> encryption failed\n");
        return -1;
    }

    // push to socket
    secure_socket *sock = get_sec_sock(sock_id);

    if (!sock)
    {
        PRINT(LOG_LEVEL_ERRORS,
            "Error : can't submit request packet because\
            secure socket has not been initialized\n");
        return -1;
    }

    push_pkt_who(sock, encrypted_elt);
    
    return 1;
}

// TODO :  use dedicated sub function
int establish_connection(unsigned int sock_id, const char *url)
{
    // build the request
    uint8_t *u8_req_param = const_cast<uint8_t *>(reinterpret_cast<const uint8_t *>(url));
    size_t size = sizeof(uint8_t) * strlen(url);
    char req_str[] = "connect";
    ctrl_pkt *req_pkt = new_ctrl_pkt(0, 0, req_str, {size},
        std::tuple<unsigned int, size_t, uint8_t *>
        (
            strlen(url),
            sizeof(uint8_t),
            u8_req_param
        ));

    // push request packet on socket
    uint8_t res = submit_req(sock_id, req_pkt);
    if (!res)
    {
        PRINT(LOG_LEVEL_ERRORS,
            "Error during establishment of connection\
            -> submission of request failed\n");
        return 0;
    }

    return 1;
}