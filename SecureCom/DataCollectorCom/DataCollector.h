#ifndef _DATA_COLLECTOR_H_
#define _DATA_COLLECTOR_H_

#include "CommonEnclave.h"

#define ERR_DC_NODATA -1
#define ERR_DC_COPY_DATA -2


struct data_collector_ws 
{
    cbuf_handle_t buffer;
	size_t dsize;
};

// void init_ribbon(struct data_collector_ws *ws);
// void flush_ribbon(struct data_collector_ws *ws);
// void print_ribbon(struct data_collector_ws *ws, int limit);

int read_nb(struct data_collector_ws *ws, uint8_t *result, size_t size);
int ensure_data(struct data_collector_ws *ws, size_t size);

#endif