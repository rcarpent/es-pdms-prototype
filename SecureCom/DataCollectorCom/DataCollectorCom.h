#ifndef _DATA_TASK_COM_H_
#define _DATA_TASK_COM_H_

#include "CommonEnclave.h"
#include "DataCollector.h"

#define MAX_DATA_COLLECTOR_WS 10


/* global variables */

// collectors
// static struct data_collector_ws **dc_ws;
// extern struct data_collector_ws **dc_ws;
static unsigned int nb_data_collector_ws = 0;


/* collectors */

struct data_collector_ws *new_data_collector_ws();
// struct data_collector_ws **new_data_collectors_ws(int nb_collectors);
int register_data_collector(unsigned sock_id);
// void add_data_collector_ws(
// 	struct data_collector_ws **dc_ws,
// 	struct data_collector_ws *ws,
// 	int sock_id);
struct data_collector_ws *get_data_collector_ws(unsigned int sock_id);


/* data acquisition */

int push_data_to_dc(struct data_collector_ws *ws, buf_elt *elt);
int acquire_data(unsigned int sock_id, uint8_t *result, int size);
int push_data_pkt(unsigned int sock_id, ctrl_pkt *pkt);


/* requests to core */

int establish_connection(unsigned int sock_id, const char *url);
int submit_req(unsigned int sock_id, ctrl_pkt *pkt);

#endif