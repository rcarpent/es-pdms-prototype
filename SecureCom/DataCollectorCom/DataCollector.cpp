#include "DataCollector.h"
#include <stdlib.h>


/* data collector ribbon */

//TODO : add functions in case of malformed data

// void init_ribbon(struct data_collector_ws *ws, int size)
// {
//     ws->buffer = (uint8_t *)malloc(size);
//     ws->buffer_size = size;
//     ws->data_start = ws->buffer;
//     ws->data_size = 0;
// }

// void flush_ribbon(struct data_collector_ws *ws) 
// {
//     ws->data_start = ws->buffer;
//     ws->data_size = 0;
// }

// void print_ribbon(struct data_collector_ws *ws, int limit) 
// {
//     int max_size, i;
//     printf("ribbon size = %d data = ", ws->dsize);

//     if (ws->dsize < limit) {
//         max_size = ws->dsize;
//     } 
//     else {
//         max_size = limit;
//     }

//     for(i = 0; i < max_size; i++) 
//     {
//         printf("%c\n", ws->buffer[i]);
//     }

//     if (max_size == limit) 
//     {
//         printf("...\n");
//     } 
//     else {
//         printf("\n");
//     }
// }

int ensure_data(struct data_collector_ws *ws, size_t size) 
{
    if (size == 0 || ws->dsize == 0)
    {
        PRINT(LOG_LEVEL_DEBUG, "No data to process -> size = %ld, data_size = %ld\n",
            size, ws->dsize);
        return ERR_DC_NODATA;
    }

    if (ws->dsize < size)
    {
        PRINT(LOG_LEVEL_DEBUG, "No enough data in collector's workspace\n");
        return ERR_DC_NODATA;
    }

    return size;
}

// read at least 'size' bytes (a element of buffer can't be truncated)
int read_nb(struct data_collector_ws *ws, uint8_t *result, size_t size) 
{
    //ensure that at least "size" data are available
    if (ensure_data(ws, size) == ERR_DC_NODATA)
        return ERR_DC_NODATA;

    size_t rsize = circular_buf_get_data(ws->buffer, result, size);

    return rsize;
}
