#ifndef _CIRCULAR_BUFFER_H_
#define _CIRCULAR_BUFFER_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include "assert.h"
#include "../Include/log.h"



/**
* Circular buffer element structure
*/

// typedef struct
// {
//     size_t data_size;
//     uint8_t *data;
// } buf_elt;

typedef struct
{
    size_t data_size;
    size_t offset;
    uint8_t data[];
} buf_elt;

/**
* Create a new buffer element
*/
buf_elt *new_buf_elt(uint8_t *pkt, size_t pkt_size);

/**
* Free a buffer element
*/

void free_buf_elt(buf_elt *elt);

/**
* Circular buffer structure
*/
typedef struct circular_buf_t circular_buf_t;

/**
* Handle type, the way users interact with the API
*/
typedef circular_buf_t *cbuf_handle_t;

/**
* Move the head pointer (and tail pointer if buffer is full) after writing
*/
void advance_pointer(cbuf_handle_t cbuf, size_t elt_size);

/**
* Move the tail pointer after reading
*/
void retreat_pointer(cbuf_handle_t cbuf, size_t elt_size);

/**
* Pass in a storage buffer and size, returns a circular buffer handle
* Requires: buffer is not NULL, size > 0
* Ensures: cbuf has been created and is returned in an empty state
*/
cbuf_handle_t circular_buf_init(buf_elt *buffer, size_t size);

/**
* Free a circular buffer structure
* Requires: cbuf is valid and created by circular_buf_init
* Does not free data buffer; owner is responsible for that
*/
void circular_buf_free(cbuf_handle_t cbuf);

/**
* Reset the circular buffer to empty, head == tail. Data not cleared
* Requires: cbuf is valid and created by circular_buf_init
*/
void circular_buf_reset(cbuf_handle_t cbuf);

/**
* Put version 1 continues to add data if the buffer is full
* Old data is overwritten
* Requires: cbuf is valid and created by circular_buf_init
*/
void circular_buf_put(cbuf_handle_t cbuf, buf_elt *data);

/**
* Put Version 2 rejects new data if the buffer is full
* Requires: cbuf is valid and created by circular_buf_init
* Returns 1 on success, 0 if buffer is full
*/
uint8_t circular_buf_put2(cbuf_handle_t cbuf, buf_elt *data);

/** 
* Retrieve a value from the buffer
* Requires: cbuf is valid and created by circular_buf_init
* Returns 1 on success, 0 if the buffer is empty
*/
buf_elt *circular_buf_get(cbuf_handle_t cbuf);

/** 
* Retrieve a block of data from the buffer
* Requires: cbuf is valid and created by circular_buf_init
* Returns 1 on success, 0 if the buffer is empty
*/
size_t circular_buf_get_data(cbuf_handle_t cbuf, uint8_t *data, size_t size);

/**
* Checks if the buffer is empty
* Requires: cbuf is valid and created by circular_buf_init
* Returns true if the buffer is empty
*/
bool circular_buf_empty(cbuf_handle_t cbuf);

/**
* Checks if the buffer is full
* Requires: cbuf is valid and created by circular_buf_init
* Returns true if the buffer is full
*/
bool circular_buf_full(cbuf_handle_t cbuf);

/**
* Check the capacity of the buffer
* Requires: cbuf is valid and created by circular_buf_init
* Returns the maximum capacity of the buffer
*/
size_t circular_buf_capacity(cbuf_handle_t cbuf);

/** 
* Check the number of elements stored in the buffer
* Requires: cbuf is valid and created by circular_buf_init
* Returns the current number of elements in the buffer
*/
size_t circular_buf_size(cbuf_handle_t cbuf);

/**
* Print the content of circular buffer
*/
void circular_buf_print(cbuf_handle_t cbuf);

//TODO: int circular_buf_get_range(circular_buf_t cbuf, uint8_t *data, size_t len);
//TODO: int circular_buf_put_range(circular_buf_t cbuf, uint8_t * data, size_t len);

#if defined(__cplusplus)
}
#endif

#endif