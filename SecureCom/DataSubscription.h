#ifndef DATA_SUBSCRIPTION_H
#define DATA_SUBSCRIPTION_H

#include <vector>
#include <iostream>
#include <unordered_map>
#include <string>


extern std::unordered_map<std::string, std::vector<std::string> > data_providers;
extern std::unordered_map<std::string, std::vector<std::string> > data_subscribers;

void add_data_provider(std::string subscriber, std::vector<std::string> &providers);
void add_data_subscriber(std::string provider, std::vector<std::string> &subscribers);

#endif