#include "SecureSocket.h"
#include <stdlib.h>

#define MAX_BUF_ELT_SIZE 500000


secure_socket *init_sec_sock(unsigned int who, unsigned int whom)
{
	secure_socket *sock = (secure_socket *)malloc(sizeof(secure_socket));
	assert(sock != NULL);
	sock->who = who;
	sock->whom = whom;
	// TODO : raise error if NULL is returned by malloc
	buf_elt *buf_inc = (buf_elt *)malloc(2 * MAX_BUF_ELT_SIZE);
	buf_elt *buf_out = (buf_elt *)malloc(2 * MAX_BUF_ELT_SIZE);
	assert(buf_inc != NULL && buf_out != NULL);
	sock->incoming_cbuf = circular_buf_init(buf_inc, MAX_BUF_ELT_SIZE);
	sock->outcoming_cbuf = circular_buf_init(buf_out, MAX_BUF_ELT_SIZE);

	return sock;
}

void push_pkt_who(secure_socket *sock, buf_elt *elt)
{
	circular_buf_put(sock->incoming_cbuf, elt);
}

void push_pkt_whom(secure_socket *sock, buf_elt *elt)
{
	circular_buf_put(sock->outcoming_cbuf, elt);
}

buf_elt *pull_pkt_who(secure_socket *sock)
{
	buf_elt *elt = circular_buf_get(sock->outcoming_cbuf);
	return elt;
}

buf_elt *pull_pkt_whom(secure_socket *sock)
{
	buf_elt *elt = circular_buf_get(sock->incoming_cbuf);
	return elt;
}

bool is_pkt_available_to_who(secure_socket *sock)
{
	return !circular_buf_empty(sock->incoming_cbuf);
}

bool is_pkt_available_to_whom(secure_socket *sock)
{
	return !circular_buf_empty(sock->outcoming_cbuf);
}