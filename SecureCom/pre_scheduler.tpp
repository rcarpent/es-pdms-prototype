T pre_process(std::vector<T> v)
{
	T sum = 0;

	for(auto it = v.begin(); it != v.end(); ++it)
    	sum += *it;

    std::cout << "sum of vector elements : " << sum << std::endl;

    return sum;
}