// Copyright (c) Open Enclave SDK contributors.
// Licensed under the MIT License.

enclave {
    include "SecureSocket.h"
    from "localattestation.edl" import *;
    trusted {
        public int register_sec_sock([user_check] secure_socket *sock);
        public void set_enclave_name([in, size=length] const char *ename, size_t length);
        // public void select_target_enclave_id(unsigned int id);
        public void add_new_dispatcher(
            unsigned int target_id,
            [in, size=target_name_size] const char *target_enclave_name,
            size_t target_name_size,
            [in, size=public_key_size] const char *public_key,
            size_t public_key_size);
        public int trusted_main(unsigned int sock_id);
    };

    /* 
     * ocall_print_string - invokes OCALL to display string buffer inside the enclave.
     *  [in]: copy the string buffer to App outside.
     *  [string]: specifies 'str' is a NULL terminated buffer.
     */
    untrusted {
        void ocall_print_string([in, string] const char *str);
    };

};
