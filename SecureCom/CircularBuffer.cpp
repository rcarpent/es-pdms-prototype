#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <assert.h>
#include <string.h>

#include "CircularBuffer.h"


struct circular_buf_t {
    buf_elt *buffer;
    size_t head;
    size_t tail;
    size_t size; //capacity bytes size
    bool full;
};

buf_elt *new_buf_elt(uint8_t *pkt, size_t pkt_size)
{
    // TODO : raise error if NULL is returned by malloc
    buf_elt *elt = (buf_elt *)malloc(sizeof(buf_elt) + sizeof(uint8_t) * pkt_size);
    assert(elt != NULL);
    elt->data_size = pkt_size;
    elt->offset = 0;
    memcpy(elt->data, pkt, pkt_size);
    return elt;
}

void free_buf_elt(buf_elt *elt)
{
    free(elt->data);
    free(elt);
}

void advance_pointer(cbuf_handle_t cbuf, size_t elt_size)
{
    assert(cbuf);

    // if(cbuf->full)
    // {
    //     cbuf->tail = (cbuf->tail + elt_size) % cbuf->size;
    // }

    cbuf->head = cbuf->head + elt_size;
    if (cbuf->head >= cbuf->size)
    {
        cbuf->head = 0;
    }

    // we mark full because we will advance tail on the next time around
    cbuf->full = (cbuf->head == cbuf->tail);
}

void retreat_pointer(cbuf_handle_t cbuf, size_t elt_size)
{
    assert(cbuf);

    cbuf->full = false;
    cbuf->tail += elt_size;
    if(cbuf->tail >= cbuf->size)
    {
        cbuf->tail = 0;
    }
}

cbuf_handle_t circular_buf_init(buf_elt *buffer, size_t size)
{
    assert(buffer && size);

    cbuf_handle_t cbuf = (cbuf_handle_t)malloc(sizeof(circular_buf_t));
    assert(cbuf);

    cbuf->buffer = buffer;
    cbuf->size = size;
    circular_buf_reset(cbuf);

    assert(circular_buf_empty(cbuf));

    return cbuf;
}

void circular_buf_free(cbuf_handle_t cbuf)
{
    assert(cbuf);
    free(cbuf);
}

void circular_buf_reset(cbuf_handle_t cbuf)
{
    assert(cbuf);

    cbuf->head = 0;
    cbuf->tail = 0;
    cbuf->full = false;
}

size_t circular_buf_size(cbuf_handle_t cbuf)
{
    assert(cbuf);

    size_t size = cbuf->size;

    if(!cbuf->full)
    {
        if(cbuf->head >= cbuf->tail)
        {
            size = (cbuf->head - cbuf->tail);
        }
        else
        {
            size = (cbuf->size + cbuf->head - cbuf->tail);
        }

    }

    return size;
}

size_t circular_buf_capacity(cbuf_handle_t cbuf)
{
    assert(cbuf);

    return cbuf->size;
}

void circular_buf_put(cbuf_handle_t cbuf, buf_elt *data)
{
    assert(cbuf && cbuf->buffer);
    if(cbuf->full)
    {
        PRINT(LOG_LEVEL_ERRORS, "Buffer was full, overwriting packets would lead to undefined behaviour.\n");
        PRINT(LOG_LEVEL_ERRORS, "Cancelling packet transmission.\n");
        return;
    }
    size_t elt_size = sizeof(buf_elt) + data->data_size;
    if(cbuf->size <= elt_size)
    {
        PRINT(LOG_LEVEL_ERRORS, "Packet is too big for buffer.\n");
        PRINT(LOG_LEVEL_ERRORS, "Cancelling packet transmission.\n");
        return;
    }
    if(cbuf->head < cbuf->tail && cbuf->head + elt_size > cbuf->tail)
    {
        PRINT(LOG_LEVEL_ERRORS, "New packet would overwrite existing packets.\n");
        PRINT(LOG_LEVEL_ERRORS, "Cancelling packet transmission.\n");
        return;
    }
    memcpy(((char *)cbuf->buffer) + cbuf->head, data, elt_size);

    advance_pointer(cbuf, elt_size);
}

uint8_t circular_buf_put2(cbuf_handle_t cbuf, buf_elt *data)
{
    uint8_t ret = 0;

    assert(cbuf && cbuf->buffer);

    if(!circular_buf_full(cbuf))
    {
        size_t elt_size = sizeof(buf_elt) + data->data_size;
        memcpy(&cbuf->buffer[cbuf->head], data, elt_size);
        advance_pointer(cbuf, elt_size);
        ret = 1;
    }

    return ret;
}

buf_elt *circular_buf_get(cbuf_handle_t cbuf)
{
    assert(cbuf && cbuf->buffer);

    buf_elt *data = NULL;

    if(!circular_buf_empty(cbuf))
    {
        buf_elt* tail_ptr = (buf_elt*)(((char *)cbuf->buffer) + cbuf->tail);
        size_t data_size = tail_ptr->data_size;
        data = (buf_elt *)malloc(sizeof(buf_elt) + sizeof(uint8_t) * data_size);
        assert(data != NULL);
        size_t elt_size = sizeof(buf_elt) + data_size;
        memcpy(data, tail_ptr, elt_size);
        retreat_pointer(cbuf, elt_size);
    }

    return data;
}

// TODO : to refactorize -> cases in while are redundant
size_t circular_buf_get_data(cbuf_handle_t cbuf, uint8_t *data, size_t size)
{
    assert(cbuf && cbuf->buffer);
    buf_elt *elt = NULL;
    size_t acc1 = 0;
    size_t acc2 = 0;
    size_t data_size = 0;
    size_t elt_size = 0;
    size_t tmp_size = 0;
    size_t offset = 0;
    // uint8_t *data = (uint8_t *)malloc(sizeof(uint8_t) * size);

    if(circular_buf_empty(cbuf))
    {
        // debug : TODO : link printf
        // printf("No enough data in circular buffer\n");
        //return NULL;
        return acc1;
    }

    while (acc1 < size)
    {
        // debug : TODO : link printf
        // printf("Getting data from circular buffer\n");
        offset = cbuf->buffer[cbuf->tail].offset;
        data_size = cbuf->buffer[cbuf->tail].data_size;
        acc2 = size - acc1;

        if (size <= data_size - offset)
        {
            memcpy(data, &(cbuf->buffer[cbuf->tail].data[offset]), size);
            cbuf->buffer[cbuf->tail].offset += size;
            return size;
        }

        if (size > data_size - offset)
        {
            memcpy(data, &(cbuf->buffer[cbuf->tail].data[offset]), data_size - offset);
            elt_size = sizeof(buf_elt) + data_size;
            cbuf->buffer[cbuf->tail].offset = 0;
            retreat_pointer(cbuf, elt_size);
            return data_size - offset;
        }

        if (acc2 >= data_size - offset)
        {
            memcpy(data + acc1, &(cbuf->buffer[cbuf->tail].data[offset]), data_size - offset);
            acc1 += data_size - offset;
            elt_size = sizeof(buf_elt) + data_size;
            cbuf->buffer[cbuf->tail].offset = 0;
            retreat_pointer(cbuf, elt_size);
            // debug : TODO : link printf
            // printf("%ld bytes has been read from circular buffer\n", size);
        }
        else 
        {
            // debug : TODO : link printf
            // printf("Build a buf_elt chunk of size %ld\n", data_size - acc2)
            memcpy(data, &(cbuf->buffer[cbuf->tail].data[offset]), acc2 - offset);
            cbuf->buffer[cbuf->tail].offset += acc2;
        }
    }

    // debug : TODO : link printf
    // printf("%d bytes has been requested ->\
    //     %ld has been provided by circular buffer\n", size, acc1);
    // return data;

    return acc1;
}

bool circular_buf_empty(cbuf_handle_t cbuf)
{
    assert(cbuf);

    return (!cbuf->full && (cbuf->head == cbuf->tail));
}

bool circular_buf_full(cbuf_handle_t cbuf)
{
    assert(cbuf);

    return cbuf->full;
}

void circular_buf_print(cbuf_handle_t cbuf)
{
    size_t i;

    for (i = 0; i < cbuf->size; i++)
    {
        if (cbuf->tail == cbuf->head)
        {
            break;
        }

        buf_elt *data = circular_buf_get(cbuf);
        //printf("Data : %lu - %s\n", data->data_size, data->data);
        free(data);
    }
}


// TODO : unit tests
// int main()
// {
//     buf_elt buffer[64];
//     buf_elt elt1 = {8, (uint8_t *)"HelloThe"};
//     buf_elt elt2 = {5, (uint8_t *)"World"};
//     buf_elt elt3 = {2, (uint8_t *)"Hi"};
//     buf_elt elt4 = {3, (uint8_t *)"You"};

//     cbuf_handle_t cbuf = circular_buf_init(buffer, 64);
//     circular_buf_put(cbuf, &elt1);
//     circular_buf_print(cbuf);
//     circular_buf_put(cbuf, &elt2);
//     circular_buf_put(cbuf, &elt3);
//     circular_buf_put(cbuf, &elt4);
//     circular_buf_print(cbuf);

//     return 0;
// }
