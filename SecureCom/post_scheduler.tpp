T post_process(std::vector<T> v)
{
	auto factor = 2;
	T sum = 0;

	for(auto it = v.begin(); it != v.end(); ++it)
    	sum += *it * factor;

    std::cout << "sum of vector elements, multiplied by a factor " << factor << " : " << sum << std::endl;

    return sum;
}