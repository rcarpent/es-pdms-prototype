#include "DataSubscription.h"


void add_data_provider(std::string subscriber, std::vector<std::string> &providers)
{
    data_providers[subscriber] = providers;

}

void add_data_subscriber(std::string provider, std::vector<std::string> &subscribers) 
{
    data_subscribers[provider] = subscribers;
}

