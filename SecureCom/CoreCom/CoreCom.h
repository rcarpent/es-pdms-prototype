#ifndef _CORE_COM_H_
#define _CORE_COM_H_

#include "CommonEnclave.h"
#include "SecureDB.h"
#include "DatabaseWrapper.h"
#include <unordered_map>


extern sqlite3 *db;
extern std::unordered_map<std::string, std::vector<std::string> > data_providers;
extern std::unordered_map<std::string, std::vector<std::string> > data_subscribers;
extern std::unordered_map<std::string, std::vector<std::pair <unsigned int, char *> > > subscribed_data;
extern std::unordered_map<std::string, std::pair<std::string, std::string> > credentials;
extern std::unordered_map<std::string, unsigned int> db_access_rights;
extern std::unordered_map<std::string, std::string> data_table_digests;
extern char core_tablename[];
extern char core_table_format[];
extern char *core_table_types[];

// retrieve the data from database by executing the sql_req
// and build a new buf_elt to push to secure socket
buf_elt *put_data_in_buf_elt(unsigned sock_id, char *fname, std::vector<std::vector<std::string> >& data, unsigned int nb_columns, unsigned int nb_rows, unsigned int whereToBegin);


#endif
