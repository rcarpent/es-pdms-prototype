#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>

#include <errno.h>
#include <mbedtls/certs.h>
#include <mbedtls/ctr_drbg.h>
#include <mbedtls/entropy.h>
#include <mbedtls/error.h>
#include <mbedtls/net_sockets.h>
#include <mbedtls/platform.h>
#include <openenclave/enclave.h>

#include "CoreNet.h"

#define MAX_ERROR_BUFF_SIZE 256
#define MAX_TLS_CLIENTS 10


char error_buf[MAX_ERROR_BUFF_SIZE];
mbedtls_ssl_context *tls_clients_ctxt[MAX_TLS_CLIENTS] = {NULL};
const size_t mbedtls_test_cas_pem_len = sizeof(mbedtls_test_cas_pem);

/* OE I/O subsystem */

int load_oe_modules()
{
    oe_result_t result = OE_FAILURE;

    if ((result = oe_load_module_host_resolver()) != OE_OK)
    {
        PRINT(
            LOG_LEVEL_ERRORS,
            "oe_load_module_host_resolver failed with %s\n",
            oe_result_str(result));
        return -1;
    }
    
    if ((result = oe_load_module_host_socket_interface()) != OE_OK)
    {
        PRINT(
            LOG_LEVEL_ERRORS,
            "oe_load_module_host_socket_interface failed with %s\n",
            oe_result_str(result));
        return -1;
    }

    return 1;
}

/* socket */

//TODO : set options for socket (setsockopt) + catch/redirect errors to stderr if possible
int open_socket(const char *serv_addr, unsigned short port)
{
    PRINT(LOG_LEVEL_DESCRIBE, "------- open_socket-----------\n");
    int sock;
    oe_result_t result = OE_FAILURE;

    // load oe modules
    if (!load_oe_modules())
    {
        PRINT(LOG_LEVEL_ERRORS, "Error during opening of socket\
            -> load_oe_modules failed\n");
        return -1;
    }

    // create the client socket
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        PRINT(LOG_LEVEL_ERRORS, "open_socket : socket() failed\n");
        return -1;
    }

    // connect to the server
    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(serv_addr);
    addr.sin_port = htons(port);

    if (connect(sock, (struct sockaddr *)&addr, sizeof(addr)) != 0)
    {
        PRINT(LOG_LEVEL_ERRORS, "open_socket : connect() failed\n");
        return -1;
    }

    // send message to server
    char message[] = "hello";
    if (send(sock, message, strlen(message), 0) < 0)
    {
        PRINT(LOG_LEVEL_ERRORS, "open_socket : send failed\n");
        close(sock);
        return -1;
    }

    // receive message from server
    char server_reply[2048];;
    if (recv(sock, &server_reply, sizeof(server_reply), 0) < 0)
    {
        PRINT(LOG_LEVEL_ERRORS, "open_socket : recv() failed\nd");
        close(sock);
        return -1;
    }

    PRINT(LOG_LEVEL_DESCRIBE, "-------- open_socket : message received from server : %s\n", server_reply);

    PRINT(LOG_LEVEL_DESCRIBE, "------- open_socket : closing socket-----------\n");
    close(sock);
        
    return 1;
}


/* TLS */ 

// debug

static void my_debug(void *ctx, int level, const char *file, int line, const char *str)
{
    ((void)level);

    mbedtls_fprintf((FILE*)ctx, "%s:%04d: %s", file, line, str);
    fflush((FILE *)ctx);
}

static void print_last_err(int err_code)
{
    if (err_code != MBEDTLS_EXIT_SUCCESS)
    {
        char error_buf[100];
        mbedtls_strerror(err_code, error_buf, 100);
        PRINT(LOG_LEVEL_ERRORS, "Last error was: %d - %s\n", err_code, error_buf);
    }
    else
    {
        PRINT(LOG_LEVEL_DEBUG, "No errors\n");
    }
}

// tls client

void init_client_ssl(
    mbedtls_net_context *server_fd,
    mbedtls_ssl_context *ssl,
    mbedtls_ssl_config *conf,
    mbedtls_ctr_drbg_context *ctr_drbg,
    mbedtls_x509_crt *client_cert,
    mbedtls_pk_context *pkey)
{
    mbedtls_net_init(server_fd);
    mbedtls_ssl_init(ssl);
    mbedtls_ssl_config_init(conf);
    mbedtls_ctr_drbg_init(ctr_drbg);
    mbedtls_x509_crt_init(client_cert);
    mbedtls_pk_init(pkey);
}

void free_client_ssl(
    mbedtls_net_context *server_fd,
    mbedtls_ssl_context *ssl,
    mbedtls_ssl_config *conf,
    mbedtls_ctr_drbg_context *ctr_drbg,
    mbedtls_x509_crt *client_cert,
    mbedtls_entropy_context *entropy,
    mbedtls_pk_context *pkey)
{
    mbedtls_net_free(server_fd);
    mbedtls_x509_crt_free(client_cert);
    mbedtls_ssl_free(ssl);
    mbedtls_ssl_config_free(conf);
    mbedtls_ctr_drbg_free(ctr_drbg);
    mbedtls_entropy_free(entropy);
    mbedtls_pk_free(pkey);

}

int configure_client_ssl(
    mbedtls_ssl_context *ssl,
    mbedtls_ssl_config *conf,
    mbedtls_ctr_drbg_context *ctr_drbg,
    mbedtls_x509_crt *client_cert)
{
    int ret = 0;
    oe_result_t result = OE_FAILURE;

    PRINT(LOG_LEVEL_DESCRIBE, "Setting up the SSL/TLS structure...\n");

    if ((ret = mbedtls_ssl_config_defaults(
             conf,
             MBEDTLS_SSL_IS_CLIENT,
             MBEDTLS_SSL_TRANSPORT_STREAM,
             MBEDTLS_SSL_PRESET_DEFAULT)) != 0)
    {
        PRINT(LOG_LEVEL_ERRORS, "failed! mbedtls_ssl_config_defaults returned %d\n", ret);
        return MBEDTLS_EXIT_FAILURE;
    }

    PRINT(LOG_LEVEL_DESCRIBE, "mbedtls_ssl_config_defaults returned successfully\n");

    /* OPTIONAL is not optimal for security,
    * but makes interop easier in this simplified example */
    mbedtls_ssl_conf_authmode(conf, MBEDTLS_SSL_VERIFY_OPTIONAL);
    mbedtls_ssl_conf_ca_chain(conf, client_cert, NULL);
    mbedtls_ssl_conf_rng(conf, mbedtls_ctr_drbg_random, ctr_drbg);
    mbedtls_ssl_conf_dbg(conf, my_debug, stdout);

    if ((ret = mbedtls_ssl_setup(ssl, conf)) != 0)
    {
        PRINT(LOG_LEVEL_ERRORS, "failed! mbedtls_ssl_setup returned %d\n", ret);
        return MBEDTLS_EXIT_FAILURE;
    }

    return ret;
}

// checks

int is_tls_client_exist(unsigned int sock_id)
{
    if (tls_clients_ctxt[sock_id] == NULL)
    {
        PRINT(LOG_LEVEL_ERRORS, "No tls client for sock id %d\n", sock_id);
        return 0;
    }

    return 1;
}

// reading and writing data

int read_data_tls(unsigned int sock_id, unsigned char *buf, size_t size)
{
    int ret = 0;

    if (!is_tls_client_exist(sock_id)) 
    {
        PRINT(LOG_LEVEL_ERRORS, "Error reading -> no tls client for sock id %d\n", sock_id);
        return -1;
    }

    mbedtls_ssl_context *ssl = tls_clients_ctxt[sock_id];
    
    PRINT(LOG_LEVEL_DESCRIBE, "Read the response from server:\n");

    do
    {
        ret = mbedtls_ssl_read(ssl, buf, size);
        if (ret == MBEDTLS_ERR_SSL_WANT_READ ||
            ret == MBEDTLS_ERR_SSL_WANT_WRITE)
            continue;

        // TODO : to check
        if (ret == MBEDTLS_ERR_SSL_PEER_CLOSE_NOTIFY) 
        {
            PRINT(LOG_LEVEL_DEBUG, "Close notification received\n");
            break;
        }

        if (ret < 0)
        {
            PRINT(LOG_LEVEL_ERRORS, "Failed! mbedtls_ssl_read returned %d\n\n", ret);
            break;
        }

        if (ret == 0)
        {
            // printf("\nEOF\n\n");
            break;
        }

        else
        {
            PRINT(LOG_LEVEL_DESCRIBE, "Client done reading server data\n");
            break;
        }

        PRINT(LOG_LEVEL_DESCRIBE, "%d bytes received from server:\n", ret);
        // printf("Verified: the contents of server payload were expected\n");
    } while (1);

    return ret;
}

int write_data_tls(unsigned int sock_id, unsigned char *buf, size_t size)
{
    int ret = 0;

    if (!is_tls_client_exist(sock_id)) 
    {
        PRINT(LOG_LEVEL_ERRORS, "Error writing -> no tls client for sock id %d\n", sock_id);
        return -1;
    }

    mbedtls_ssl_context *ssl = tls_clients_ctxt[sock_id];

    // Write client payload to the server
    PRINT(LOG_LEVEL_DESCRIBE, "Write to server:\n");
    while ((ret = mbedtls_ssl_write(ssl, buf, size)) <= 0)
    {
        if (ret != MBEDTLS_ERR_SSL_WANT_READ &&
            ret != MBEDTLS_ERR_SSL_WANT_WRITE)
        {
            PRINT(LOG_LEVEL_ERRORS, "Failed! mbedtls_ssl_write returned %d\n", ret);
            return ret;
        }
    }

    PRINT(LOG_LEVEL_DESCRIBE, "%d bytes written\n", ret);
    PRINT(LOG_LEVEL_DESCRIBE, "Data written : %s\n", (char *)buf);

    return ret;
}

// effective establisment of tls connexion 

int launch_tls_client(mbedtls_ssl_context *ssl, char *server_name, char *server_port)
{
    int ret = 1;
    uint32_t flags;
    const char *pers = "ssl_client";
    oe_result_t result = OE_FAILURE;
    int exit_code = MBEDTLS_EXIT_FAILURE;

    mbedtls_net_context *server_fd = (mbedtls_net_context *)malloc(sizeof(mbedtls_net_context));
    mbedtls_entropy_context *entropy = (mbedtls_entropy_context *)malloc(sizeof(mbedtls_entropy_context));
    mbedtls_ctr_drbg_context *ctr_drbg = (mbedtls_ctr_drbg_context *)malloc(sizeof(mbedtls_ctr_drbg_context));
    mbedtls_x509_crt *client_cert = (mbedtls_x509_crt *)malloc(sizeof(mbedtls_x509_crt));
    mbedtls_ssl_config *conf = (mbedtls_ssl_config *)malloc(sizeof(mbedtls_ssl_config));
    mbedtls_pk_context *pkey = (mbedtls_pk_context *)malloc(sizeof(mbedtls_pk_context));

    // explicitly enabling host resolver and socket features
    if (!load_oe_modules())
    {
        PRINT(LOG_LEVEL_ERRORS,
            "Error during opening of socket\
            -> load_oe_modules failed\n");
        free_client_ssl(server_fd, ssl, conf, ctr_drbg, client_cert, entropy, pkey);
        return -1;
    }

    // initialize mbedtls objects
    init_client_ssl(server_fd, ssl, conf, ctr_drbg, client_cert, pkey);

    PRINT(LOG_LEVEL_DESCRIBE, "\nSeeding the random number generator...\n");
    mbedtls_entropy_init(entropy);
    if ((ret = mbedtls_ctr_drbg_seed(
             ctr_drbg,
             mbedtls_entropy_func,
             entropy,
             (const unsigned char *)pers,
             strlen(pers))) != 0)
    {
        PRINT(
            LOG_LEVEL_ERRORS, "Failed ! mbedtls_ctr_drbg_seed returned %d\n", ret);
        free_client_ssl(server_fd, ssl, conf, ctr_drbg, client_cert, entropy, pkey);
        return ret;
    }

    // initialize certificates
    PRINT(LOG_LEVEL_DESCRIBE,  "  . Loading the CA root certificate ..." );
    ret = mbedtls_x509_crt_parse(client_cert, (const unsigned char *) mbedtls_test_cas_pem,
         mbedtls_test_cas_pem_len );
    ret = mbedtls_x509_crt_parse(client_cert, (const unsigned char *) mbedtls_test_cert,
         sizeof(mbedtls_test_cert));

    if(ret < 0)
    {
        PRINT(LOG_LEVEL_ERRORS, "Failed ! mbedtls_x509_crt_parse returned -0x%x\n\n", -ret);
        free_client_ssl(server_fd, ssl, conf, ctr_drbg, client_cert, entropy, pkey);
        return ret;
    }

    ret = mbedtls_pk_parse_key(
        pkey, 
        (const unsigned char *) mbedtls_test_key, 
        sizeof(mbedtls_test_key), 
        NULL, 0);

    if(ret != 0)
    {
        PRINT(LOG_LEVEL_ERRORS, "Failed ! mbedtls_pk_parse_key returned %d\n\n", ret);
        return ret;
    }

    PRINT(LOG_LEVEL_DESCRIBE, "ok (%d skipped)\n", ret);

    // start the connection
    PRINT(
        LOG_LEVEL_DESCRIBE, "Connecting to tcp : %s:%s...\n", server_name, server_port);

    if ((ret = mbedtls_net_connect(
             server_fd, server_name, server_port, MBEDTLS_NET_PROTO_TCP)) != 0)
    {
        PRINT(
            LOG_LEVEL_ERRORS, "Failed ! mbedtls_net_connect returned %d errno=%d\n", ret, errno);
        free_client_ssl(server_fd, ssl, conf, ctr_drbg, client_cert, entropy, pkey);
        return ret;
    }

    PRINT(LOG_LEVEL_DESCRIBE, "Connected to server : %s.%s\n", server_name, server_port);

    // configure client SSL settings
    ret = configure_client_ssl(ssl, conf, ctr_drbg, client_cert);
    if (ret != 0)
    {
        PRINT(LOG_LEVEL_ERRORS, "Failed ! mbedtls_net_connect returned %d\n", ret);
        free_client_ssl(server_fd, ssl, conf, ctr_drbg, client_cert, entropy, pkey);
        return ret;
    }

    if ((ret = mbedtls_ssl_set_hostname(ssl, server_name)) != 0)
    {
        PRINT(
            LOG_LEVEL_ERRORS, "Failed ! mbedtls_ssl_set_hostname returned %d\n", ret);
        free_client_ssl(server_fd, ssl, conf, ctr_drbg, client_cert, entropy, pkey);
        return ret;
    }

    mbedtls_ssl_set_bio(
        ssl, server_fd, mbedtls_net_send, mbedtls_net_recv, NULL);

    // handshake
    PRINT(LOG_LEVEL_DESCRIBE, "Performing the SSL/TLS handshake...\n");
    while ((ret = mbedtls_ssl_handshake(ssl)) != 0)
    {
        if (ret != MBEDTLS_ERR_SSL_WANT_READ &&
            ret != MBEDTLS_ERR_SSL_WANT_WRITE)
        {
            PRINT(LOG_LEVEL_ERRORS, "Failed ! mbedtls_ssl_handshake returned -0x%x\n\n", -ret);
            free_client_ssl(server_fd, ssl, conf, ctr_drbg, client_cert, entropy, pkey);
            return ret;
        }
    }

    PRINT(LOG_LEVEL_DESCRIBE, "TLS handshake done successfully\n");

    // verify the server certificate
    PRINT(LOG_LEVEL_DESCRIBE, "Verifying peer X.509 certificate..." );

    /* Note : in real life, we probably want to bail out when ret != 0 */
    if ((flags = mbedtls_ssl_get_verify_result(ssl)) != 0)
    {
        char vrfy_buf[512];
        PRINT(LOG_LEVEL_ERRORS, "failed\n" );
        mbedtls_x509_crt_verify_info(vrfy_buf, sizeof(vrfy_buf), "  ! ", flags);
        PRINT(LOG_LEVEL_ERRORS, "%s\n", vrfy_buf );
    }
    else
        PRINT(LOG_LEVEL_DESCRIBE, " ok\n" );

    // TODO : to move to appropriate place
    // mbedtls_ssl_close_notify(&ssl);
    ret = MBEDTLS_EXIT_SUCCESS;

    return 1;
}

int establish_com_tls(unsigned int sock_id, char *server_name, char *server_port)
{
    PRINT(LOG_LEVEL_DESCRIBE, "Setting a tls connexion on address %s:%s\n", server_name, server_port);

    if (tls_clients_ctxt[sock_id] != NULL)
    {
        PRINT(LOG_LEVEL_ERRORS, "Tls client already exist for sock_id %d\n", sock_id);
        return 0;
    }

    mbedtls_ssl_context *ssl = (mbedtls_ssl_context *)malloc(sizeof(mbedtls_ssl_context));
    int retcode = launch_tls_client(ssl, server_name, server_port);

    if (retcode)
    {
        tls_clients_ctxt[sock_id] = ssl;
    }
    else
    {
        PRINT(LOG_LEVEL_ERRORS, "Can't launch tls client on sock id %d\n", sock_id);
        return retcode;
    }

    return 1;
}
