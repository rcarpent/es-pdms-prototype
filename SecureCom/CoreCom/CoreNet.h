#ifndef _CORE_NET_H_
#define _CORE_NET_H_

#include "mbedtls/ssl.h"
#include "../../Include/log.h"


const char mbedtls_test_cas_pem[] = "-----BEGIN CERTIFICATE-----\r\n"                                      \
    "MIIDQTCCAimgAwIBAgIBAzANBgkqhkiG9w0BAQsFADA7MQswCQYDVQQGEwJOTDER\r\n" \
    "MA8GA1UECgwIUG9sYXJTU0wxGTAXBgNVBAMMEFBvbGFyU1NMIFRlc3QgQ0EwHhcN\r\n" \
    "MTkwMjEwMTQ0NDAwWhcNMjkwMjEwMTQ0NDAwWjA7MQswCQYDVQQGEwJOTDERMA8G\r\n" \
    "A1UECgwIUG9sYXJTU0wxGTAXBgNVBAMMEFBvbGFyU1NMIFRlc3QgQ0EwggEiMA0G\r\n" \
    "CSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDA3zf8F7vglp0/ht6WMn1EpRagzSHx\r\n" \
    "mdTs6st8GFgIlKXsm8WL3xoemTiZhx57wI053zhdcHgH057Zk+i5clHFzqMwUqny\r\n" \
    "50BwFMtEonILwuVA+T7lpg6z+exKY8C4KQB0nFc7qKUEkHHxvYPZP9al4jwqj+8n\r\n" \
    "YMPGn8u67GB9t+aEMr5P+1gmIgNb1LTV+/Xjli5wwOQuvfwu7uJBVcA0Ln0kcmnL\r\n" \
    "R7EUQIN9Z/SG9jGr8XmksrUuEvmEF/Bibyc+E1ixVA0hmnM3oTDPb5Lc9un8rNsu\r\n" \
    "KNF+AksjoBXyOGVkCeoMbo4bF6BxyLObyavpw/LPh5aPgAIynplYb6LVAgMBAAGj\r\n" \
    "UDBOMAwGA1UdEwQFMAMBAf8wHQYDVR0OBBYEFLRa5KWz3tJS9rnVppUP6z68x/3/\r\n" \
    "MB8GA1UdIwQYMBaAFLRa5KWz3tJS9rnVppUP6z68x/3/MA0GCSqGSIb3DQEBCwUA\r\n" \
    "A4IBAQA4qFSCth2q22uJIdE4KGHJsJjVEfw2/xn+MkTvCMfxVrvmRvqCtjE4tKDl\r\n" \
    "oK4MxFOek07oDZwvtAT9ijn1hHftTNS7RH9zd/fxNpfcHnMZXVC4w4DNA1fSANtW\r\n" \
    "5sY1JB5Je9jScrsLSS+mAjyv0Ow3Hb2Bix8wu7xNNrV5fIf7Ubm+wt6SqEBxu3Kb\r\n" \
    "+EfObAT4huf3czznhH3C17ed6NSbXwoXfby7stWUDeRJv08RaFOykf/Aae7bY5PL\r\n" \
    "yTVrkAnikMntJ9YI+hNNYt3inqq11A5cN0+rVTst8UKCxzQ4GpvroSwPKTFkbMw4\r\n" \
    "/anT1dVxr/BtwJfiESoK3/4CeXR1\r\n"                                     \
    "-----END CERTIFICATE-----\r\n";

const char mbedtls_test_cert[] = "-----BEGIN CERTIFICATE-----\r\n"
"MIIDOTCCAiECFHgqoD+a8k/TUTdStD2GgjtmAAdBMA0GCSqGSIb3DQEBCwUAMFkx\r\n"
"CzAJBgNVBAYTAkFVMRMwEQYDVQQIDApTb21lLVN0YXRlMSEwHwYDVQQKDBhJbnRl\r\n"
"cm5ldCBXaWRnaXRzIFB0eSBMdGQxEjAQBgNVBAMMCWxvY2FsaG9zdDAeFw0yMTAy\r\n"
"MjMyMDExMTNaFw0yMTAzMjUyMDExMTNaMFkxCzAJBgNVBAYTAkFVMRMwEQYDVQQI\r\n"
"DApTb21lLVN0YXRlMSEwHwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQx\r\n"
"EjAQBgNVBAMMCWZsb2NsaWVudDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC\r\n"
"ggEBANQL4B9uM1ekc85YqBpXyAe2q7k9/fSQxdgGJl10dm+OyujrZPfAFUwrJjzx\r\n"
"g64WDWlALHEs/mTuEVCf4y/5HxRPObWidq3PFVGRFTRlVRk86NHE2uGef3PNdJqs\r\n"
"ITOfaouf5CQDCuqqp009csSbcCy9x8aYhRFzvZVgZ360ShexJbkDNvl2sk0kJs9U\r\n"
"DWbwoWlx/eyaaxipT9tjMiQS9XMA1urW9PHFyNRKGRzHFvBR/GG3X+HnUELJC0s0\r\n"
"La6k5x1O5Q/L5+abgIwTQ9xeB7oKstuXE4g3Z4lOWPJgcgbFTf33ZtVVT3l81IJo\r\n"
"bcSKm9xpnDrSAHcpgC98Y0lTxEsCAwEAATANBgkqhkiG9w0BAQsFAAOCAQEAXVRg\r\n"
"OQqXeI6xuXFPRnvNVg1fuBZfs7HOGby7intYWmB9D2GjQ9QujCyGpkr6o4Xr1bfH\r\n"
"+lzqYomb1wi4Di+5trYOlkfpgaBAtz+UxLv4Sd5JJK8hBEjzvMnFAraHq1HDmzC7\r\n"
"5tVO32dJYV/wzU4hEeo8FcuL3vUss3t9dYtGteewQJh7nu1lvmkQ53sWva8Cp90g\r\n"
"u+ZFFes4xzwHNZ/W0jhvnmmRyboHgG6q6aZGBvJ/f5c1hpvEoY1xQJDj4sbUJFIn\r\n"
"vMx8TO8EIqieaihV1IrC4hw3eUbDopUcX/+N4WBB+o2Gm5f+Z+gFXNdE3fIRXOCB\r\n"
"ZTq5wgxpJKADeekbaQ==\r\n"
"-----END CERTIFICATE-----\r\n";

const char mbedtls_test_key[] ="-----BEGIN RSA PRIVATE KEY-----\r\n"
"MIIEowIBAAKCAQEA1AvgH24zV6RzzlioGlfIB7aruT399JDF2AYmXXR2b47K6Otk\r\n"
"98AVTCsmPPGDrhYNaUAscSz+ZO4RUJ/jL/kfFE85taJ2rc8VUZEVNGVVGTzo0cTa\r\n"
"4Z5/c810mqwhM59qi5/kJAMK6qqnTT1yxJtwLL3HxpiFEXO9lWBnfrRKF7EluQM2\r\n"
"+XayTSQmz1QNZvChaXH97JprGKlP22MyJBL1cwDW6tb08cXI1EoZHMcW8FH8Ybdf\r\n"
"4edQQskLSzQtrqTnHU7lD8vn5puAjBND3F4Hugqy25cTiDdniU5Y8mByBsVN/fdm\r\n"
"1VVPeXzUgmhtxIqb3GmcOtIAdymAL3xjSVPESwIDAQABAoIBAHhQ7LCo1NUL6PHS\r\n"
"eD/KQyOtUK7hL/HPG5NBmg84/+K4wQV8W2QAR5Vafm3iE/d5Z86qrQzinM4kDwsS\r\n"
"TdRwpGPfuD0JnBFvCQepLMD4JlsVpn0wF4ohZuwMTfddZ9FkyQ3Egay1VUJl5lv7\r\n"
"W92W3QVmAa7757VJWO8jLlZEci4SOpg3h3byyiyzzo1AomN/lXIfPH/MhO2zRS4p\r\n"
"L174UWfRbQBqxiFIbnB3ZSINpNtzCHljwcxWQ+oE1zNNDh0W7iyGfvDkO4pOA+En\r\n"
"guki1Z3qPtGZSnqWxtQihv5oFGOwSY9nuaaKKLNg0ypXcQOhbxEE98zb1c7UoXzw\r\n"
"sGAge0ECgYEA/c+bU4zHMAyB3+gXzTbsG1bY9ux8JTXYX0vnc6DD9MDGad8L4Dg9\r\n"
"LCs7aNu3Mn6XbSm6qL9A+VD7FZIo3l4o62tppbI3raw6JaxQkxLsBIUI4Ys3nbDI\r\n"
"/u9AsyX5hOg4wE0E8hJaYDM3/5ameD6eHp/V6SeAbqXFUmtFZpJp6BECgYEA1eAO\r\n"
"WmxokKv13xSfUYV3R1hGrEWZ+qOTVTMCwzT7JjN0bXJJ2Es6tgQYvgpTkpiVpXO9\r\n"
"zGEzPvivA2vwKYE30d6cgvW942nGY0rbZ6E3F2aFH7oyoQ/jRFQ1o0Iy8bg60Hic\r\n"
"Xg4sBmixlhm2kCayaIoNgT99qnkTjCrGqt5CIpsCgYBFlhXP4lj6I3O4H7nm2ZgC\r\n"
"Jt11VYDz8r9P4U+jtAAFqpS7kht/bevXoEK5jQO9JFf/5eD3QvhdYoZ0c44g6U/u\r\n"
"u5RLiYEabI3Epmmw6Q8sbgzxSWoGIeH5gz8J3u96MPDeSD+C3uVIWsKPoI4jiYdw\r\n"
"fgH32oyryWdAIYj1hk7jAQKBgQDMwCxdis1sOwQupdqEO+beKXe3MwbfeTvyY9xw\r\n"
"L0eE4dvDb5ecdyDk4fUh8lbh0CvrcmuwvxlFXuSFvQ71XRyoUZq6kTJSBZCEvERh\r\n"
"6GgKgf5OSn5vzPsBDjEvc4+782hy7hScwqqJFC93CHTLehBwnkGhi7vtuO3q0YTS\r\n"
"Tec4owKBgF6IP/s6zuIFo7daFig/UMokd8MgHlhIdkDj97K+L4o5FqATCmTyO1tr\r\n"
"Rj6UJFs6UxwrHB1qkD9KMr/iurBGduEGYuhyaavsqal7Ot8G9q3bedtOdAV8g+ha\r\n"
"7RTJB7ueseqST+5UqYQCyAnvmBRSCTYrIbs6b36dEhO9oGQtA18p\r\n"
"-----END RSA PRIVATE KEY-----\r\n";

int load_oe_modules();
int open_socket(const char *serv_addr, unsigned short port);
// void init_client_ssl(
//     mbedtls_net_context *server_fd
//     mbedtls_ssl_context *ssl,
//     mbedtls_ssl_config *conf,
//     mbedtls_ctr_drbg_context *ctr_drbg,
//     mbedtls_x509_crt *client_cert)
// void free_client_ssl(
//     mbedtls_net_context *server_fd
//     mbedtls_ssl_context *ssl,
//     mbedtls_ssl_config *conf,
//     mbedtls_ctr_drbg_context *ctr_drbg,
//     mbedtls_x509_crt *client_cert,
//     mbedtls_entropy_context entropy);
// int configure_client_ssl(
//     mbedtls_ssl_context *ssl,
//     mbedtls_ssl_config *conf,
//     mbedtls_ctr_drbg_context *ctr_drbg,
//     mbedtls_x509_crt *client_cert);
int is_tls_client_exist(unsigned int sock_id);
int read_data_tls(unsigned int sock_id, unsigned char *buf, size_t size);
int write_data_tls(unsigned int sock_id, unsigned char *buf, size_t size);
int launch_tls_client(mbedtls_ssl_context *ssl, char *server_name, char *server_port);
int establish_com_tls(unsigned int sock_id, char *server_name, char *server_port);


#endif