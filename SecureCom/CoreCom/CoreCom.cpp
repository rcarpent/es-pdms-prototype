#include "CoreCom.h"
#include "CoreNet.h"
#include "mbedtls/sha256.h"
#include "mbedtls/md.h"
#include <iomanip>


#define MAX_DATA_COLLECTORS 10
#define MAX_BUF_SIZE 1024

// Should be a query parameter
const char *tablename = "GeolifeTrajectoriesMerged";

pkt_type prefilter(uncap_data *udata)
{
    if (!strcmp(udata->fname, "connect"))
    {
        PRINT(LOG_LEVEL_DESCRIBE, "Establishment of tls socket has been requested\n");
        return t_tls_connect;
    }

    else if (!strcmp(udata->fname, "http_req"))
    {
        PRINT(LOG_LEVEL_DESCRIBE, "Processing of http request\n");
        return t_http_req;
    }

    else 
    {
        PRINT(LOG_LEVEL_DESCRIBE, "Processing of data\n");
        return t_data;
    }
}

uncap_data *uncap_ctrl_pkt(untrust_ctrl_pkt *pkt)
{
    char *data = reinterpret_cast<char *>(pkt);
    size_t size = sizeof(pkt->size_fname) + sizeof(pkt->size_params) + sizeof(pkt->nb_params);
    uncap_data *udata = (uncap_data *)malloc(sizeof(uncap_data));
    oe_assert(udata != NULL);
    memset(udata, 0, sizeof(uncap_data));
    udata->fname = (char *)malloc(pkt->size_fname);
    oe_assert(udata->fname != NULL);
    memset(udata->fname, 0, pkt->size_fname);
    memcpy(udata->fname, data + size, pkt->size_fname);
    udata->params = (char *)malloc(pkt->size_params);
    oe_assert(udata->params != NULL);
    memset(udata->params, 0, pkt->size_params);
    memcpy(udata->params, data + size + pkt->size_fname, pkt->size_params);
    udata->size = pkt->size_params;

    PRINT(LOG_LEVEL_DEBUG, 
        "Packet exchanged by Core to DataTask :\n\
        \tsize_fname : %ld\n\
        \tsize_params : %ld\n\
        \tnb_params : %d\n\
        \tfname : %s\n",\
        pkt->size_fname, pkt->size_params,
        pkt->nb_params, udata->fname);

    return udata;
}

int exchange_pkt(unsigned int sock_id, buf_elt *pkt)
{
    PRINT(LOG_LEVEL_DESCRIBE, "Exchanging packet\n");

    secure_socket *sock = get_sec_sock(sock_id);

    if (!sock)
    {
        PRINT(LOG_LEVEL_ERRORS, "Error : can't exchange packet because secure socket has not been initialized\n");
        return -1;
    }

    if (pkt == NULL)
    {
        PRINT(LOG_LEVEL_DEBUG, "No exchange because of empty packet\n");
        return 0;
    }

    // uncapsulation of data
    untrust_ctrl_pkt *unpkt = (untrust_ctrl_pkt *)pkt->data;
    uncap_data *udata = uncap_ctrl_pkt(unpkt);

    // filtering
    pkt_type ptype = prefilter(udata);

    // testing
    PRINT(LOG_LEVEL_DESCRIBE, "Build a new control packet with data retrieved from db :\n");
    // const char *sql_req = convert_params_to_sql(udata->params, target_names[sock_id]);
    const char *sql_req = convert_params_to_sql(udata->params, tablename);
    
    DataDB d_info = DataDB();

    // sql request
    if(execute_sql(sql_req, &d_info) != 0)
    {
        PRINT(LOG_LEVEL_ERRORS, "Error during packet exchange \
            -> Cannot retrieve data from Database\n");
        return -1;
    }

    d_info.display_db_data();
    unsigned int nb_columns = d_info.get_nb_columns();
    unsigned int nb_rows = d_info.get_nb_rows();
    
    std::vector<std::vector<std::string> >& data = d_info.get_db_data();

    /* EXPERIMENTAL */
    int nb_dts = 1; // Should be a query parameter
    bool split = 0;
    bool pipeline = 0;

    if(split)
    {
        /* Input splitting */
        buf_elt **db_elements = (buf_elt **)malloc(nb_dts * sizeof(buf_elt*));
        buf_elt **elements = (buf_elt **)malloc(nb_dts * sizeof(buf_elt*));
        for(int i = 0 ; i < nb_dts ; i++) {
            db_elements[i] = put_data_in_buf_elt(i, udata->fname, data, nb_columns, nb_rows/nb_dts, i*nb_rows/nb_dts);

            // encrypt packet
            elements[i] = encrypt_pkt(i, db_elements[i]);
            if (elements[i] == NULL)
            {
                PRINT(LOG_LEVEL_ERRORS, "Error during exchanging of packet\
                    -> encryption failed\n");
                return -1;
            }

            // push packet
            push_pkt_whom(get_sec_sock(i), elements[i]);

            // freeing
            free(db_elements[i]);
            free(elements[i]);
        }
        free_uncap_data(udata);
        free(db_elements);
        free(elements);
    }
    else if (pipeline)
    {
        /* Pipeline = 1 obj per pkt */
        unsigned int socket_id = 1;
        unsigned int rows_per_obj = 60;
        unsigned int number_of_obj = nb_rows/rows_per_obj;
        buf_elt **db_elements = (buf_elt **)malloc(number_of_obj * sizeof(buf_elt*));
        assert(db_elements != NULL);
        buf_elt **elements = (buf_elt **)malloc(number_of_obj * sizeof(buf_elt*));
        assert(elements != NULL);
        for(unsigned int i = 0 ; i < number_of_obj ; i++) {
            db_elements[i] = put_data_in_buf_elt(socket_id, udata->fname, data, nb_columns, rows_per_obj, i*rows_per_obj);

            // encrypt packet
            elements[i] = encrypt_pkt(socket_id, db_elements[i]);
            if (elements[i] == NULL)
            {
                PRINT(LOG_LEVEL_ERRORS, "Error during exchanging of packet\
                    -> encryption failed\n");
                return -1;
            }

            // push packet
            push_pkt_whom(get_sec_sock(socket_id), elements[i]);

            // freeing
            free(db_elements[i]);
            free(elements[i]);
        }
        free_uncap_data(udata);
        free(db_elements);
        free(elements);
    }
    else if (!split && !pipeline)
    {
        /* Each input is the same */
        buf_elt *db_element = put_data_in_buf_elt(sock_id, udata->fname, data, nb_columns, nb_rows, 0); // On se permet de mettre la même sock_id alors qu'on va transmettre à différentes DT car sock_id pas utilisé autrement que pour providers.
        buf_elt *element = encrypt_pkt(sock_id, db_element); // On se permet de mettre la même sock_id car pour l'instant la clé AES est la même pour toutes les DT.
        if (element == NULL)
        {
            PRINT(LOG_LEVEL_ERRORS, "Error during exchanging of packet\
                -> encryption failed\n");
            return -1;
        }
        for(int i = 0 ; i < nb_dts ; i++)
        {
            // push packet
            push_pkt_whom(get_sec_sock(i), element);
        }
        free_uncap_data(udata);
        free(db_element);
        free(element);
    }
    return 1;
    
}

uncap_data *uncap_data_pkt(ctrl_pkt *pkt)
{
    char *data = reinterpret_cast<char *>(pkt);
    size_t size = sizeof(pkt->size_fname) + sizeof(pkt->size_params) + sizeof(pkt->nb_columns) + sizeof(pkt->nb_rows);
    uncap_data *udata = (uncap_data *)malloc(sizeof(uncap_data));
    oe_assert(udata != NULL);
    memset(udata, 0, sizeof(uncap_data));
    udata->fname = (char *)malloc(pkt->size_fname);
    oe_assert(udata->fname != NULL);
    memset(udata->fname, 0, pkt->size_fname);
    memcpy(udata->fname, data + size, pkt->size_fname);
    udata->params = (char *)malloc(pkt->size_params);
    oe_assert(udata->params != NULL);
    memset(udata->params, 0, pkt->size_params);
    memcpy(udata->params, data + size + pkt->size_fname, pkt->size_params);
    udata->size = pkt->size_params;

    PRINT(LOG_LEVEL_DEBUG,
        "Data received from Data Task :\n\
        \tnb_columns : %d\n\
        \tnb_rows : %d\n\
        \tsize_fname : %ld\n\
        \tsize_params : %ld\n\
        \tfname : %s\n",\
        pkt->nb_rows, pkt->nb_columns,
        pkt->size_fname, pkt->size_params,
        udata->fname);

    // debug
    // printf("\tparams : %f, %f\n", ((float *)(params))[0], ((float *)(params))[1]);
    // printf("\tparams : %d, %d, %f\n", ((int *)(params))[0], ((int *)(params))[1], ((float *)(params))[2]);

    return udata;
}

/* tools */

static void print_hex(
    const char *title,
    const unsigned char buf[],
    size_t len)
{
    PRINT(LOG_LEVEL_DEBUG, "%s: ", title);

    for (size_t i = 0; i < len; i++) {
        if (buf[i] < 0xF) {
            PRINT(LOG_LEVEL_DEBUG, "0%x", buf[i]);
        } else {
            PRINT(LOG_LEVEL_DEBUG, "%x", buf[i]);
        }
    }

    PRINT(LOG_LEVEL_DEBUG, "\n");
}

static std::string convert_digest(
    const unsigned char buf[], 
    size_t len)
{
    std::ostringstream ss;
    char val[50];

    for (size_t i = 0; i < len; i++) {
        if (buf[i] < 0xF) 
        {
            sprintf(val, "0%x", buf[i]);
            ss << val;
        }
        else
        { 
            sprintf(val, "%x", buf[i]);
            ss << val;
        }
    }

    return ss.str();
}

int retrieve_data_pkt(unsigned int sock_id)
{
    PRINT(LOG_LEVEL_DESCRIBE, "Retrieving packet from data task on sock_id %d\n", sock_id);

    // retrieve packet
    secure_socket *sock = get_sec_sock(sock_id);

    if (!sock)
    {
        PRINT(LOG_LEVEL_ERRORS,
            "Error : can't retrieve data packet because\
            secure socket has not been initialized\n");
        return -1;
    }


    buf_elt *encrypted_elt = pull_pkt_whom(sock);

    if (encrypted_elt == NULL)
    {
        PRINT(LOG_LEVEL_DEBUG, "No packet has been pulled\n");
        return 0;
    }

    // encrypt packet
    buf_elt *elt = decrypt_pkt(sock_id, encrypted_elt);

    if (elt == NULL)
    {
        PRINT(LOG_LEVEL_ERRORS,
            "Error during retrieving of data packet\
            -> decryption failed\n");
        return -1;
    }
    else {
        PRINT(LOG_LEVEL_DESCRIBE, "Success to decrypt data packet\n");
    }

    // uncapsulation of data
    ctrl_pkt *pkt = (ctrl_pkt *)(elt->data);
    uncap_data *udata = uncap_data_pkt(pkt);

    // filtering
    pkt_type ptype = prefilter(udata);

    // TODO : check return code of write_data_tls
    if (ptype == t_http_req)
    {
        char *str_req = strndup(udata->params, udata->size);
        write_data_tls(sock_id, const_cast<unsigned char *>(reinterpret_cast<const unsigned char *>(str_req)), udata->size);
        free(elt);
        free(encrypted_elt);
        free_uncap_data(udata);
        return 1;
    }
    //TODO : extract port from udata + check return code of establish_com_tls
    else if (ptype == t_tls_connect)
    {
        char *server_port = strdup("4433"); // for testing only
        char *server_name = strndup(udata->params, udata->size);
        int retcode = establish_com_tls(sock_id, server_name, server_port);
        free(elt);
        free(encrypted_elt);
        free_uncap_data(udata);
        return retcode;
    }
    else if (ptype == t_data)
    {
        // debug
        auto it = db_access_rights.find(std::string(target_names[sock_id]));  
        if (it != db_access_rights.end() && it->second)
        {
            // debug
            // printf("Data received from DC1 :\n"
            //     "\tnb_columns : %d\n\tnb_rows : %d\n"
            //     "\tsize_fname : %ld\n"
            //     "\tsize_params : %ld\n"
            //     "\tfname : %s\n",
            //     pkt->nb_rows, pkt->nb_columns,
            //     pkt->size_fname, pkt->size_params,
            //     udata->fname);
            // printf("\tparams :\n\t\t%d, %d, %d\n\t\t%d, %d, %d\n\t\t%f, %f, %f\n\n", 
            // ((int *)(udata->params))[0],
            // ((int *)(udata->params + 4))[0],
            // ((int *)(udata->params + 8))[0],
            // ((int *)(udata->params + (pkt->nb_rows-1) * 4))[1],
            // ((int *)(udata->params + (pkt->nb_rows-1) * 4 + 4))[1],
            // ((int *)(udata->params + (pkt->nb_rows-1) * 4 + 8))[1],
            // ((float *)(udata->params + (pkt->nb_rows-1) * 2 * 4))[2],
            // ((float *)(udata->params + (pkt->nb_rows-1) * 2 * 4 + 4))[2],
            // ((float *)(udata->params + (pkt->nb_rows-1) * 2 * 4 + 8))[2]);
        
            char ***data = convert_serial_data(
                pkt->nb_rows,
                pkt->nb_columns,
                core_table_types,
                udata->params);

            std::string conv_data = "";
            for (int i = 0; i < pkt->nb_rows; i++)
            {
                for (int j = 0; j < pkt->nb_columns; j++)
                    conv_data += std::string(data[i][j]);
            }

            PRINT(LOG_LEVEL_DESCRIBE, "Compute sha256 digest on tuples provided by %s\n", target_names[sock_id]);
            unsigned char output1[32];
            auto ucdata =
                reinterpret_cast<const unsigned char *>(conv_data.c_str());

            mbedtls_sha256_ret(
                const_cast<unsigned char *>(ucdata),
                conv_data.length(), 
                output1, 0);
            print_hex("Calculated digest", output1, sizeof(output1));
            std::string digest_str = convert_digest(output1, sizeof(output1));
            PRINT(LOG_LEVEL_DESCRIBE, "Converted digest : %s\n", digest_str.c_str());

            if (!data_table_digests[target_names[sock_id]].compare(digest_str))
            {
                PRINT(LOG_LEVEL_DESCRIBE, "Integrity checked on data provided by %s -> loading in database\n",
                    target_names[sock_id]);

                load_data_to_db(
                    db, core_tablename, core_table_format, core_table_types,
                    data, pkt->nb_rows, pkt->nb_columns);
            }

            for (int i = 0; i < pkt->nb_rows; i++)
            {
                for (int j = 0; j < pkt->nb_columns; j++)
                    free(data[i][j]);
                free(data[i]);
            }
            free(data);
        }

        auto subscribers = data_subscribers[std::string(target_names[sock_id])];
        if (!subscribers.empty()) 
        {
            for (auto &p : subscribers){
                subscribed_data[std::string(target_names[sock_id])].push_back(std::make_pair(pkt->nb_rows, udata->params));
                PRINT(LOG_LEVEL_DESCRIBE, "Data from %s has been provided to %s\n", target_names[sock_id], p.c_str());
            }
            return 1;
        }
        free(elt);
        free(encrypted_elt);
        free_uncap_data(udata);
    }
    else {
        // freeing
        free(elt);
        free(encrypted_elt);
        free_uncap_data(udata);
    }

    return 1;
}

int collect_data_pkt(unsigned int sock_id)
{
    unsigned char *data = (unsigned char *)malloc(sizeof(unsigned char) * MAX_BUF_SIZE);
    memset(data, 0, MAX_BUF_SIZE);
    int ret;

    PRINT(LOG_LEVEL_DESCRIBE, "Collect data packet on sock_id %d\n", sock_id);

    if (!is_tls_client_exist(sock_id)) 
    {
        return -1;
    }

    ret = read_data_tls(sock_id, data, MAX_BUF_SIZE);

    if (ret == 0)
    {
        PRINT(LOG_LEVEL_DESCRIBE, "No data to collect on sock id %d\n", sock_id);
        return 0;
    }

    if (ret < 0)
    {
        PRINT(LOG_LEVEL_ERRORS, "Error during collect of data on tls connexion\n");
        return -1;
    }

    //debug
    PRINT(LOG_LEVEL_DESCRIBE, "A packet of %d bytes has been collected on tls connexion\n", ret);

    //TODO : to refactorize and move in tools section
    //TODO : should be integrated in http parser
    // extract digest
    std::string str = std::string(reinterpret_cast<char *>(data));
    std::string ptrn = "Digest: ";
    size_t first = str.find(ptrn);
    size_t last = str.find("Content-Length");
    std::string digest = "";

    if (first != std::string::npos && last != std::string::npos)
    {
        digest = str.substr(first, last-first);
        digest.erase(digest.find(ptrn), ptrn.length());
        digest.erase(digest.length()-2, 3); // temporary dirty trick
        data_table_digests[target_names[sock_id]] = digest;
        PRINT(LOG_LEVEL_DESCRIBE, "Digest found : %s\n", digest.c_str());
    }
    else
    {
        first = 0;
        last = 0;
        PRINT(LOG_LEVEL_DESCRIBE, "No digest found in collected packet\n");
    }
 
    // build buf_elt from data collected
    // temporary dirty trick
    buf_elt *elt = new_buf_elt(reinterpret_cast<uint8_t *>(data + last), ret - last);
    // buf_elt *elt = new_buf_elt(reinterpret_cast<uint8_t *>(data), ret);
    // encrypt result
    buf_elt *encrypted_elt = encrypt_pkt(sock_id, elt);

    if (encrypted_elt == NULL)
    {
        PRINT(LOG_LEVEL_ERRORS, "Error during encryption of collected data packet\n");
        return -1;
    }

    // push to socket
    secure_socket *sock = get_sec_sock(sock_id);

    if (!sock)
    {
        PRINT(LOG_LEVEL_ERRORS, "Error : collected packet can't be pushed on secure socket\n");
        return -1;
    }
    push_pkt_whom(sock, encrypted_elt);

    return 1;
}


/* database */

// TODO : to refactorize to be more efficient + split in sub-functions
buf_elt *put_data_in_buf_elt(unsigned sock_id, char *fname, std::vector<std::vector<std::string> >& data, unsigned int nb_columns, unsigned int nb_rows, unsigned int whereToBegin)
{

    // conversion of data retrieved from database
    size_t size;
    // auto tb_scheme = tables_schemes[target_names[sock_id]];
    if(tables_schemes.count(std::string(tablename)) == 0)
    {
        PRINT(LOG_LEVEL_ERRORS, "Unknown table scheme %s\n", tablename);
        return NULL;
    }
    auto tb_scheme = tables_schemes[tablename];
    std::vector<size_t> type_sizes = get_db_type_size(tb_scheme);
    std::vector<size_t> sizes;

    auto providers = data_providers[std::string(target_names[sock_id])];
    // debug
    if (!providers.empty())
    {
        PRINT(LOG_LEVEL_DESCRIBE, "Data Task %s is subscribed to : ", target_names[sock_id]);
        for (auto  &p : providers){
            PRINT(LOG_LEVEL_DESCRIBE, "%s ", p.c_str());
        }
        PRINT(LOG_LEVEL_DESCRIBE, "\n");
    }

    std::vector<std::vector<std::string> > sub_data_vec;
    unsigned int up_nb_rows = 0;
    unsigned int sub_data_len[providers.size()];

    unsigned int provider_idx = 0;
    for (auto it = providers.begin(); it != providers.end(); it++)
    {
        auto &sub_data = subscribed_data[*it];
        sub_data_len[provider_idx] = sub_data.size();
        provider_idx++;
        for (auto it2 = sub_data.begin(); it2 != sub_data.end(); it2++)
        {
            PRINT(LOG_LEVEL_DESCRIBE, "Adding of data from %s in the packet at destination to %s\n", it->c_str(), target_names[sock_id]);
            up_nb_rows += it2->first;
        }
    }

    provider_idx = 0;
    char *params[nb_columns];
    int offset = 0;

    unsigned int row_number_limit = (whereToBegin + nb_rows > data[0].size() ? data[0].size() : whereToBegin + nb_rows);

    for (unsigned int p_idx = 0; p_idx < nb_columns; p_idx++)
    {
        size = type_sizes[p_idx] * (nb_rows);
        size += (up_nb_rows * type_sizes[p_idx]);
        sizes.push_back(size);

        // debug
        // printf("Updated size = %d\n", size);
        // printf("size for column %d = %d\n", p_idx, size); 

        params[p_idx] = new char[size];
        int cursor = 0;

        for (unsigned int rownumber = whereToBegin; rownumber < row_number_limit ; rownumber++)
        {
            auto &str = data[p_idx][rownumber];
            convert_db_data(str.data(), tb_scheme[p_idx], &params[p_idx][cursor]);
            cursor += type_sizes[p_idx];
        }

        // TODO : to refactorize using sum + implement bound the max size of built packet
        if (!providers.empty())
        {
            for (auto it = providers.begin(); it != providers.end(); it++)
            {
                auto &sub_data = subscribed_data[*it];
                for (unsigned int i = 0; i < sub_data_len[provider_idx]; i++)
                {
                    // debug
                    // printf("offset = %d, cursor = %d\n", offset, cursor);
                    memcpy(&params[p_idx][cursor], sub_data[i].second + offset, type_sizes[p_idx]);
                    cursor += type_sizes[p_idx];
                }
                offset += type_sizes[p_idx];
            }
        }
    }

    if (!providers.empty())
    {
        for (auto it = providers.begin(); it != providers.end(); it++)
        {
            // TODO :  critical to refactorize because very expensive
            auto &sub_data = subscribed_data[*it];
            sub_data.erase(sub_data.begin(), sub_data.begin() + sub_data_len[provider_idx]);
        }
    }

    // build control packet and convert it in buffer element
    ctrl_pkt *pkt = params_to_ctrl_pkt(nb_columns, nb_rows + up_nb_rows, fname, params, sizes);
    uint8_t *p = reinterpret_cast<uint8_t *>(pkt);
    size_t p_size = sizeof(ctrl_pkt) + pkt->size_fname + pkt->size_params;
    buf_elt *elt = new_buf_elt(p, p_size);

    for (unsigned int p_idx = 0; p_idx < nb_columns; p_idx++)
    {
        delete params[p_idx];
    }
    free(pkt);
    return elt;
}