#include "DataTaskCom.h"
#include <string.h>

#if DATA_TASK_TYPE == 1
#include "Wrapper.h"
#endif

//TODO
pkt_type prefilter(uncap_data *udata)
{
    return t_data;
}

uncap_data *uncap_ctrl_pkt(ctrl_pkt *pkt)
{
    char *data = reinterpret_cast<char *>(pkt);
    size_t size = sizeof(pkt->size_fname) + sizeof(pkt->size_params) +\
                  sizeof(pkt->nb_columns) + sizeof(pkt->nb_rows);
    uncap_data *udata = (uncap_data *)malloc(sizeof(uncap_data));
    oe_assert(udata != NULL);
    memset(udata, 0, sizeof(uncap_data));
    udata->fname = (char *)malloc(pkt->size_fname);
    oe_assert(udata->fname != NULL);
    memset(udata->fname, 0, pkt->size_fname);
    memcpy(udata->fname, data + size, pkt->size_fname);
    udata->params = (char *)malloc(pkt->size_params);
    oe_assert(udata->params != NULL);
    memset(udata->params, 0, pkt->size_params);
    memcpy(udata->params, data + size + pkt->size_fname, pkt->size_params);
    udata->size = pkt->size_params;

    PRINT(LOG_LEVEL_DESCRIBE,
        "Data received from Core :\n\
        \tnb_columns : %d\n\
        \tnb_rows : %d\n\
        \tsize_fname : %ld\n\
        \tsize_params : %ld\n\
        \tfname : %s\n",\
        pkt->nb_columns, pkt->nb_rows,
        pkt->size_fname, pkt->size_params,
        udata->fname);

    // debug
    // printf("\tparams : %lf, %lf\n", ((double *)(params))[0], ((double *)(params))[1]);
    // printf("\tparams : %d, %d\n", ((int *)(udata->params))[0], ((int *)(udata->params))[1]);
    // printf("\tparams : %d, %d\n", ((int *)(udata->params))[2], ((int *)(udata->params))[3]);
    // printf("\tparams : %f, %f\n", ((float *)(udata->params))[4], ((float *)(udata->params))[5]);

    return udata;
}

int retrieve_ctrl_pkt(unsigned int sock_id)
{
    PRINT(LOG_LEVEL_DESCRIBE, "Retrieving packet\n");

    // retrieve packet
    secure_socket *sock = get_sec_sock(sock_id);

    if (!sock)
    {
        PRINT(LOG_LEVEL_ERRORS,
            "Error : can't retrieve control packet because\
            secure socket has not been initialized\n");
        return -1;
    }

    buf_elt *encrypted_elt = pull_pkt_who(sock);

    if (encrypted_elt == NULL)
    {
        PRINT(LOG_LEVEL_DEBUG, "No packet has been pulled\n");
        return 0;
    }

    // decrypt packet
    buf_elt *elt = decrypt_pkt(sock_id, encrypted_elt);

    if (elt == NULL)
    {
        PRINT(LOG_LEVEL_ERRORS, "Error during retrieving of control packet -> decryption failed\n");
        return -1;
    }
    else {
        PRINT(LOG_LEVEL_DESCRIBE, "Success to decrypt control packet\n");
    }

    // uncapsulation of data
    ctrl_pkt *pkt = reinterpret_cast<ctrl_pkt *>(elt->data);
    uncap_data *udata = uncap_ctrl_pkt(pkt);

    // filtering
    pkt_type ptype = prefilter(udata);

    // apply the function
    #if DATA_TASK_TYPE == 1
    PRINT(LOG_LEVEL_DESCRIBE, "Calling the wrapper :\n");
    wrapper(sock_id, udata->fname, pkt->nb_columns, pkt->nb_rows, udata->params);
    #endif

    //freeing
    free(encrypted_elt);
    free(udata->fname);
    free(udata->params);
    free(udata);
    free(elt);

    return 1;
}

int submit_res(unsigned int sock_id, ctrl_pkt *pkt)
{
    PRINT(LOG_LEVEL_DESCRIBE, "Submit result in socket %d\n", sock_id);

    // convert control packet to buffer element
    uint8_t *p = reinterpret_cast<uint8_t *>(pkt);
    size_t p_size = sizeof(ctrl_pkt) + pkt->size_fname + pkt->size_params;
    buf_elt *elt = new_buf_elt(p, p_size);
    // encrypt result
    buf_elt *encrypted_elt = encrypt_pkt(sock_id, elt);

    if (encrypted_elt == NULL)
    {
        PRINT(LOG_LEVEL_ERRORS, "Error during submission of data packet -> encryption failed\n");
        return -1;
    }

    // push to socket
    secure_socket *sock = get_sec_sock(sock_id);

    if (!sock)
    {
        PRINT(LOG_LEVEL_ERRORS,
            "Error : can't submit result because\
            secure socket has not been initialized\n");
        return -1;
    }

    push_pkt_who(sock, encrypted_elt);

    free(encrypted_elt);
    free(elt);
    return 1;
}
