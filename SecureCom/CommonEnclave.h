#ifndef COMMON_ENCLAVE_H_
#define COMMON_ENCLAVE_H_

#include <map>
#include <string>
#include <stdarg.h>
#include <tuple>
#include <memory>
#include <functional>
#include <stdexcept>
#include <sstream>
#include <vector>
#include "CircularBuffer.h"
#include "../Include/log.h"

#if defined(__cplusplus)
extern "C" {
#endif


#if defined(__cplusplus)
}
#endif


#define MAX_DATA_TASKS 10

#define print_any(X) _Generic((X),  int: print_int, \
                                    default: print_unknown, \
                                    float: print_float, \
                                    double: print_double)(X)

/* enclave properties */

extern const char *enclave_name;
extern char **target_names;


/* secure socket */
extern secure_socket *sec_socks[MAX_DATA_TASKS];
extern unsigned int nb_sec_socks;
secure_socket *get_sec_sock(unsigned int sock_id);

/* ocall secure functions */

int printf(const char *fmt, ...);

/* serialization */ 

template <typename T>
void serialize_pod_p(char **params, unsigned int param_idx, std::tuple<unsigned int, size_t, T> t) 
{
    static_assert(std::is_pod<T>::value, "T must be POD");
    static_assert(std::is_pointer<T>::value, "T must be a pointer or reference");
    PRINT(LOG_LEVEL_DEBUG, "Allocate %lu byte(s) in params[%d]\n", std::get<1>(t) * std::get<0>(t), param_idx);
    params[param_idx] = (char *)malloc(std::get<1>(t) * std::get<0>(t));
    oe_assert(params[param_idx] != NULL);
    
    char *p1 = reinterpret_cast<char *>(std::get<2>(t));
    size_t offset = 0;
    size_t size = sizeof(*(std::get<2>(t)));
    PRINT(LOG_LEVEL_DEBUG, "Size of an element : %lu\n", size);

    for (unsigned int i = 0; i < std::get<0>(t); i++)
    {
		PRINT(LOG_LEVEL_DEBUG, "Copy %lu byte(s) into params[%d]\n", std::get<1>(t), param_idx);
        auto val = p1;
        memcpy(params[param_idx] + offset, (char *)val, size);
        std::string str_val = std::to_string(*(reinterpret_cast<T>(val)));
		PRINT(LOG_LEVEL_DEBUG, "Parameter added in params[%d] : %s\n", param_idx, str_val.c_str());
        p1 += size;
        //debug
        //printf("Offset = %lu\n", offset);
        offset += size;
    }
}

template<typename T, typename... Args>
void serialize_pod_p(char **params, unsigned int param_idx, std::tuple<unsigned int, size_t, T> t, Args... args)
{
    serialize_pod_p(params, param_idx, t);
    serialize_pod_p(params, ++param_idx, args...);
}


/* control packet */

typedef struct  
{
    size_t size_fname;
    size_t size_params;
    unsigned int nb_columns;
    unsigned int nb_rows;
    char params[];

} ctrl_pkt;

typedef struct  
{
    size_t size_fname;
    size_t size_params;
    unsigned int nb_params;
    char params[];

} untrust_ctrl_pkt;

typedef enum { t_unknown, t_data, t_http_req, t_tls_connect } pkt_type;

// TODO : move from header file
static inline ctrl_pkt *params_to_ctrl_pkt(unsigned int nb_columns, unsigned int nb_rows, char *fname, char **params, std::vector<size_t> sizes)
{
    unsigned int v_length = (unsigned int)sizes.size();
    size_t size_fname = strlen(fname) + 1;
    size_t size = 0;
    unsigned int i;

    // note : accumulate is not present in intel sgx lib
    for (i = 0; i < v_length; i++)
        size += sizes[i];

    ctrl_pkt *pkt = (ctrl_pkt *)malloc(sizeof(ctrl_pkt) + sizeof(char) * (size_fname + size));
    oe_assert(pkt != NULL);
    pkt->size_fname = size_fname;
    pkt->size_params = size;
    pkt->nb_columns = nb_columns;
    pkt->nb_rows = nb_rows;
    size_t offset = 0;
    memcpy(pkt->params, fname, size_fname);
    offset += size_fname;

    for (i = 0; i < v_length; i++)
    {
        memcpy(pkt->params + offset, params[i], sizes[i]);
        offset += sizes[i];
    }

    return pkt;
}

// dummy trick to refactorize
template<typename T>
ctrl_pkt *new_ctrl_pkt(unsigned int nb_columns, unsigned int nb_rows, char *fname, std::vector<size_t> sizes,\
 std::tuple<unsigned int, size_t, T> t)
{
    unsigned int param_idx = 0;
    char **params = (char **)malloc(sizeof(char *) * nb_columns);
    oe_assert(params != NULL);
    serialize_pod_p(params, param_idx, t);
    
    ctrl_pkt * pkt = params_to_ctrl_pkt(nb_columns, nb_rows, fname, params, sizes);

    //free params after their copy
    free(params[0]);
    free(params);

    return pkt;
}

// TODO : to refactorize to avoid redundant memcpy
// TODO : fix recursion call
template<typename T, typename... Args>
ctrl_pkt *new_ctrl_pkt(unsigned int nb_columns, unsigned int nb_rows, char *fname, std::vector<size_t> sizes,\
 std::tuple<unsigned int, size_t, T> t, Args... args)
{
    unsigned int param_idx = 0;
    char **params = (char **)malloc(sizeof(char *) * nb_columns);
    oe_assert(params != NULL);
    serialize_pod_p(params, param_idx, t);
    serialize_pod_p(params, ++param_idx, args...);
    
    ctrl_pkt * pkt = params_to_ctrl_pkt(nb_columns, nb_rows, fname, params, sizes);

    // free params after their copy
    free(params[0]);
    free(params[1]);
    free(params);

    return pkt;
}

/* uncapsulation - filtering */

typedef struct 
{
    char *fname;
    char *params;
    size_t size;
} uncap_data;

void free_uncap_data(uncap_data *udata);
extern uncap_data *uncap_ctrl_pkt(ctrl_pkt *pkt);
extern uncap_data *uncap_data_pkt(ctrl_pkt *pkt);
extern pkt_type prefilter(uncap_data *udata);

/* encryption/decryption */

buf_elt *encrypt_pkt(unsigned int sock_id, buf_elt *pkt);
buf_elt *decrypt_pkt(unsigned int sock_id, buf_elt *pkt);


#endif
