#include <stdarg.h>
#include <string.h>
#include "SecureSocket.h"
#include <map>
#include "CommonEnclave.h"
#include "localattestation.h"


/* global variables */

//secure sockets
secure_socket *sec_socks[MAX_DATA_TASKS] = {NULL};
unsigned int nb_sec_socks = 0;
// attestation
// unsigned int target_enclave_id = 0;
const char *enclave_name;
char **target_names = (char **)malloc(sizeof(char *) * MAX_DATA_TASKS);
std::map<unsigned int, ecall_dispatcher *> dispatchers;


/* secure sockets */

int register_sec_sock(secure_socket *sock)
{
    sec_socks[nb_sec_socks] = sock;
    nb_sec_socks++;
    PRINT(LOG_LEVEL_DESCRIBE, "Secure socket has been register in %s\n", enclave_name);

    return 1;
}

secure_socket *get_sec_sock(unsigned int sock_id)
{
    secure_socket *sock = sec_socks[sock_id];

    if (sock == NULL)
    {
        PRINT(LOG_LEVEL_ERRORS, "Error : no secure socket available on sock_id %d\n", sock_id);
        return NULL;
    }

    return sock;
}


/* attestation */

void add_new_dispatcher(unsigned int target_id, const char *target_enclave_name, size_t target_name_size, const char *public_key, size_t public_key_size)
{   
    PRINT(LOG_LEVEL_DESCRIBE, "Adding a new dispatcher to exchange with target enclave %s\n", target_enclave_name);
    enclave_config_data_t *config_data = (enclave_config_data_t *)malloc(sizeof(enclave_config_data_t));
    config_data->other_enclave_public_key_pem = strndup(public_key, public_key_size);
    config_data->other_enclave_public_key_pem_size = public_key_size;
    std::string target_name = std::string(target_enclave_name);
    std::string dispatcher_name = std::string(target_enclave_name) + "Dispatcher";
    dispatchers[target_id] = new ecall_dispatcher(dispatcher_name.c_str(), config_data);
    target_names[target_id] = strndup(target_enclave_name, target_name_size);
    dispatchers[target_id]->initialize();
    PRINT(LOG_LEVEL_DESCRIBE, "The dispatcher %s has been initialized for target_id %d\n", dispatcher_name.c_str(), target_id);
}


/* enclave attributes */

void set_enclave_name(const char *ename, size_t length)
{
    enclave_name = strndup(ename, length);
    PRINT(LOG_LEVEL_DESCRIBE, "Enclave name : %s\n", enclave_name);
}


/* ocall secure functions */

int printf(const char* fmt, ...)
{
    char buf[BUFSIZ] = { '\0' };
    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);
    ocall_print_string(buf);
    return (int)strnlen(buf, BUFSIZ - 1) + 1;
}

int print_int(int i) { return printf("%d\t", i); }
int print_float(float f) { return printf("%f\t", f); }
int print_double(double d) { return printf("%lf\t", d); }
int print_unknown(...) { return printf("ERROR: Unknown type\n"); }


/* encryption/decryption */

buf_elt *encrypt_pkt(unsigned int sock_id, buf_elt *pkt)
{
    PRINT(LOG_LEVEL_DESCRIBE, "Encryption of packet on sock id %d\n", sock_id);

     if (pkt == NULL)
    {
        PRINT(LOG_LEVEL_ERRORS, "Can't process encryption -> packet is empty\n");
        return NULL;
    }

    uint8_t *encrypted_msg = NULL;
    size_t encrypted_msg_size = 0;

    if (!generate_encrypted_message(sock_id, pkt->data, pkt->data_size, &encrypted_msg, &encrypted_msg_size))
    {
        buf_elt* enc_pkt = new_buf_elt(encrypted_msg, encrypted_msg_size);
        free(encrypted_msg);
        return enc_pkt;
    }
    else
    {
        PRINT(LOG_LEVEL_ERRORS, "Error during encryption of packet\n");
        return NULL;
    }

}

buf_elt *decrypt_pkt(unsigned int sock_id, buf_elt *pkt)
{
    PRINT(LOG_LEVEL_DESCRIBE, "Decryption of packet on sock id %d\n", sock_id);

    if (pkt == NULL)
    {
        PRINT(LOG_LEVEL_ERRORS, "Can't process decryption -> packet is empty\n");
        return NULL;
    }

    size_t decrypted_data_size;
    uint8_t* decrypted_data;
    
    if (!process_encrypted_msg(sock_id, pkt->data, pkt->data_size, &decrypted_data, &decrypted_data_size))
    {
        buf_elt* dec_pkt = new_buf_elt(decrypted_data, decrypted_data_size);
        free(decrypted_data);
        return dec_pkt;
    }
    else
    {
        PRINT(LOG_LEVEL_ERRORS, "Error during decryption of packet\n");
        return NULL;
    }
}


/* control packet */

// TODO : remove columns and rows set to 0
// TODO : need to handle unpacking of arg_tuples 
// build a control packet composed of string arguments
ctrl_pkt *new_request(std::string &req, vector<std::string> &args)
{
    // auto str_to_u8 = [&](std::string &s) -> uint8_t * 
    // {
    //     auto u = const_cast<uint8_t *>(reinterpret_cast<const uint8_t *>(s.c_str()));
    //     return u;
    // };

    // std::vector<size_t> args_sizes;
    // std::vector<std::tuple<unsigned int, size_t, uint8_t *> > arg_tuples;
    // size_t size;

    // for (auto &a : args) 
    // {
    //     size = sizeof(uint8_t) * a.length();
    //     args_sizes.push_back(size);
    //     arg_tuples.push_back(std::tuple<unsigned int, size_t, uint8_t *>(
    //         a.length(),
    //         sizeof(uint8_t),
    //         str_to_u8(a)));
    // }

    // ctrl_pkt *pkt = new_ctrl_pkt(0, 0, req.c_str(), args_sizes, arg_tuples);

    ctrl_pkt *pkt = NULL;
    return pkt;
}


/* uncapsulation - filtering */

void free_uncap_data(uncap_data *udata)
{
    free(udata->fname);
    free(udata->params);
    free(udata);
}