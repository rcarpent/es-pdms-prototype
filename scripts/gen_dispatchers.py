#! /usr/bin/env python3

from os import listdir, getenv
from os.path import isfile, join
from sys import argv

# TODO : use environment variables instead
pubkeys_folder = getenv("PDMS_ROOT_FOLDER") + "/Pubkeys/"
securecom_folder = getenv("PDMS_ROOT_FOLDER") + "/SecureCom/"

def gen_dispatchers_code(target_enclave_name, pubkeys_folder, securecom_folder):
	pem_files = [f for f in listdir(pubkeys_folder) if isfile(join(pubkeys_folder, f))]
	#print("pubkeys files : ", pem_files)
	dispatchers = []
	enclaves_names = []

	dispatcher_cpp_file = open(securecom_folder + target_enclave_name + "Dispatcher.cpp", 'w')
	dispatcher_cpp_file.write('''
#include "dispatcher.h"
#include "localattestation.h"
#include <map>
#include <string.h>\n\n
'''
	)

	for filename in pem_files:
		file = open(pubkeys_folder + filename, 'r')
		enclave_name = filename.replace("_pubkey.pem", "")
		enclaves_names.append(enclave_name)
		#print("Enclave name : ", enclave_name)
		content = file.read()
		pubkey_content = content.replace('\n', '\\n')
		pubkey_content_var = enclave_name + "_pubkey_content"
		dispatcher_cpp_file.write("const char {}[] = \"{}\";\n"\
			.format(pubkey_content_var, pubkey_content))
		dispatcher_config_name = enclave_name + "_dispatcher_config"
		dispatcher_cpp_file.write("enclave_config_data_t  {} = {{{}, strlen({}) + 1}};\n"\
			.format(dispatcher_config_name, pubkey_content_var, pubkey_content_var))
		dispatcher_name = enclave_name + "_dispatcher"
		dispatcher_cpp_file.write("ecall_dispatcher *{} = new ecall_dispatcher(\"{}\", &{});\n"\
			.format(dispatcher_name, dispatcher_name, dispatcher_config_name))
		dispatchers.append("{{\"{}\", {}}}".format(enclave_name, dispatcher_name))

	print("dispatchers : ", ",".join(dispatchers))
	#exit(0)
	dispatcher_cpp_file.write("const char *enclave_name = \"{}\";\n".format(target_enclave_name))
	dispatcher_cpp_file.write(
		"char *target_enclave_ids[] = {{\"{}\"}};\n".format("\",\"".join(enclaves_names)))
	dispatcher_cpp_file.write(
		"std::map<const char *, std::map<const char *, ecall_dispatcher *>> dispatchers = {{{{\"{}\",{{{}}}}}}};\n"
		.format(target_enclave_name, ",".join(dispatchers)))


if __name__ == "__main__":
	if len(argv) != 3:
		print("usage : enclave_name enclave_type(Core/DataTask)")
		exit(0)
	if (argv[2] == "Core"):
		pubkeys_folder += "/DataTaskKeys/"
		securecom_folder += "/CoreCom/"
	elif (argv[2] == "DataTask"):
		pubkeys_folder += "/CoreKeys/"
		securecom_folder += "/DataTaskCom/"
	else:
		print("Unknow type")
		exit(0)
	gen_dispatchers_code(argv[1], pubkeys_folder, securecom_folder)
