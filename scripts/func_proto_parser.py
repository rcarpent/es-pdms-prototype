#! /usr/bin/env python3


# TODO : 
#     - fix indentation in generated code
#     - use variables for string replacement
#     - check consistency between out_nb_args and out_type
#     - check that number of arguments is not null

import json

root_dir = "./"
proto_file = "func_proto.json"
wrapper_cpp_file = root_dir + "Wrapper.cpp"
wrapper_header_file = root_dir + "Wrapper.h"
generated_cpp_code = '''#include "Wrapper.h"
#include "DataTaskCom.h"

void wrapper(unsigned int sock_id, char *fname, unsigned int nb_columns, unsigned int nb_rows, char *params)
{
'''

with open(proto_file, 'r') as json_file:
    data = json.load(json_file)

taskname = data["taskname"]
func_l = [(x["funcname"], x["input"], x["output"]) for x in data["functions"]]
# print("functions : ", func_l)

generated_header_code = '''#ifndef WRAPPER_H_
#define WRAPPER_H_

#include <string.h>
#include "{}"

void wrapper(unsigned int sock_id, char *fname, unsigned int nb_columns, unsigned int nb_rows, char *params);

#endif
'''.format(taskname + ".h")

for i in range(len(func_l)):
    args_name = []
    args = []
    out_params = ""
    sizes = []
    vectors_name = []
    types = []
    cnt = 0
    vectors = ["std::vector<{}> v{}".format(v["type"], str(idx)) for idx, v in enumerate(func_l[i][2]["types"])] 
    vectors_name = ["v{}".format(str(idx)) for idx in range(len(vectors))]
    out_types = [v["type"] for v in func_l[i][2]["types"]]
    out_elt_sizes = ["sizeof({})".format(v["type"]) for v in func_l[i][2]["types"]]

    # TODO : refactorize with a modification in json to hanle single element instead array
    for j, arg in enumerate(func_l[i][1]):
        arg_name = "*arg" + str(j)
        args_name.append("arg" + str(j))
        a = arg["type"] + " " + arg_name 
        args.append(a)
        types.append(arg["type"])
        sizes.append("nb_rows * sizeof({})".format(arg["type"]))
    s = '''\t
    if (!strcmp(fname, "{}")) 
    {{
        //generate struct to cast parameters to past to the wrapper
        size_t size = 0;
        struct wrapper_st {{ {}; }};
        struct {} {{  unsigned int nb_columns; unsigned int nb_rows; {}; }};
        struct wrapper_st *wst = (struct wrapper_st *)malloc(sizeof(struct wrapper_st)); 
        oe_assert(wst != NULL);
        {}
        {}
        {};
        struct {} *res  = (struct {}*){}(nb_columns, nb_rows, {});

        //build control packet to submit
        char funcname[] = "{}";
        {};
        {};
        std::vector<size_t> sizes = {{ {} }};
        ctrl_pkt *pkt = new_ctrl_pkt(res->nb_columns, res->nb_rows, funcname, sizes, {});

        //submit result to socket
        submit_res(sock_id, pkt);

        //freeing
        free(pkt);
        {}
        free(wst);
        {};
        free(res);
    }}
    '''.format(
        func_l[i][0], 
        '; '.join(args), 
        func_l[i][2]["typename"], '; '.join([param["type"] + " *arg" + str(idx) for idx, param in enumerate(func_l[i][2]["types"])]),
        "\n\t".join(["wst->" + s[0] + "=(" + s[1] + " *)" + "malloc(sizeof(" + s[1] + ") * nb_rows);" for s in zip(args_name, types)]),
        "\n\t".join(["oe_assert(wst->" + s + " != NULL);" for s in args_name]),
        ";\n\t".join(["memcpy(wst->" + v[0] + ", params + size, " + v[1] + ");\n\tsize += " + v[1] for v in zip(args_name, sizes)]),
        func_l[i][2]["typename"], func_l[i][2]["typename"], func_l[i][0], 
        ', '.join(["wst->" + s for s in args_name]),
        func_l[i][0], 
        ';\n\t'.join(vectors),
        ";\n\t".join([p[0] + ".assign(res->" + p[1] + ", res->" + p[1] + " + res->nb_rows)" for p in zip(vectors_name, args_name)]),
        ",".join(["res->nb_rows * " + v for v in out_elt_sizes]),
        ",\n\t".join(["std::tuple<unsigned int, size_t, " + v[0] + " *>(res->nb_rows, " + v[1] + ", &" + v[2] + "[0]" + ")" for v in zip(out_types, out_elt_sizes, vectors_name)]),
        "\n\t\t".join(["free(wst->" + s + ");" for s in args_name]),
        "\n\t\t".join(["free(res->arg" + str(idx) + ");" for idx, param in enumerate(func_l[i][2]["types"])]))

    generated_cpp_code += s
    # print(s)
generated_cpp_code += "\n}"

# print(generated_cpp_code)

with open(wrapper_cpp_file, 'w+') as wrapper_c_code:
    wrapper_c_code.write(generated_cpp_code)
with open(wrapper_header_file, 'w') as wrapper_h_code:
    wrapper_h_code.write(generated_header_code)
