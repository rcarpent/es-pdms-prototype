#!/usr/bin/env python3

import requests
import json
from requests.auth import HTTPBasicAuth

hostname = 'https://localhost:4433'
certfile = '../Config/floclient.crt'
keyfile = '../Config/floclient.pem'
# certchain = '../Config//flochain.pem'
auth = ('flo', '1988')

def print_initial_req(response):
    print("Content of initial request :")
    print("\turl : ", response.request.url)
    print("\theaders : ", response.request.headers)
    print("\tbody : ", response.request.body)

session = requests.Session()
session.verify = False
session.cert = (certfile, keyfile)
session.auth = auth
# response = session.get(hostname)
# print_initial_req(response)
# print("data received : ", response.text)
datareq = {'file': 'last_month.csv', 'data' : 'all'}
datareq = json.dumps(datareq)
response = session.post(hostname, data=datareq)
print_initial_req(response)
print("data received: ", response.text)
