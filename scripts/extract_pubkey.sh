#!/bin/bash

if [ "$#" -ne 2 ]
then
	echo "Usage : $0 private_key public_key"
	exit 0
fi

openssl rsa -in $1 -outform PEM -pubout -out $2