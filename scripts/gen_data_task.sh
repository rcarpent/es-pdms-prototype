#!/bin/bash

DATA_TASK_NAME=$1
APP_NAME=$2

echo "Generate enclave $DATA_TASK_NAME and app $APP_NAME"
cp -r ../templates/data_task_templates/ ../$DATA_TASK_NAME
cp -r ../templates/app_templates/ ../$APP_NAME
mv ../$APP_NAME/app.cpp ../$APP_NAME/$APP_NAME.cpp
mv ../$DATA_TASK_NAME/data_task.conf ../$DATA_TASK_NAME/$DATA_TASK_NAME.conf
mv ../$DATA_TASK_NAME/data_task.edl ../$DATA_TASK_NAME/$DATA_TASK_NAME.edl
LIB_PATH=\\.\\.\\/lib\\/
sed -i -e "s/DATA_TASK_NAME/$LIB_PATH$DATA_TASK_NAME\\.so\\.signed/g" ../$APP_NAME/$APP_NAME.cpp
sed -i -e "s/libocall\\.so/libocall_$DATA_TASK_NAME\\.so/" ../$DATA_TASK_NAME/OcallLibrary/Makefile
find ../$DATA_TASK_NAME -type f -exec sed -i -e "s/DATA_TASK_NAME/$DATA_TASK_NAME/g" {} \;
DATA_TASK_NAME_HEADER=\_$DATA_TASK_NAME\_H\_
echo -e "#ifndef $DATA_TASK_NAME_HEADER\n#define $DATA_TASK_NAME_HEADER\n\n" > ../$DATA_TASK_NAME/$DATA_TASK_NAME.h
cat ../templates/func_examples/func_example.h >> ../$DATA_TASK_NAME/$DATA_TASK_NAME.h
echo -e "\n\n#endif" >> ../$DATA_TASK_NAME/$DATA_TASK_NAME.h
echo -e "#include \"$DATA_TASK_NAME.h\"\n#include \"CommonEnclave.h\"\n\n" >> ../$DATA_TASK_NAME/$DATA_TASK_NAME.cpp
cat ../templates/func_examples/func_example.cpp >> ../$DATA_TASK_NAME/$DATA_TASK_NAME.cpp
sed -i -e "s/DATA_TASK_NAME/$DATA_TASK_NAME/" ../$DATA_TASK_NAME/$DATA_TASK_NAME.cpp
