#!/usr/bin/env python3

from sys import argv
from mbedtls import pk

def encrypt_data(keyfile, datafile, outputfile):
	key_file = open(keyfile, "r")
	key_data = key_file.read() + "\0"
	data_file = open(datafile, "r")
	data = data_file.read()
	# debug
	# print("Data to encrypt :\n", data)
	key_file.close(); data_file.close()
	output_file = open(outputfile, "wb")
	rsa = pk.RSA(key_data.encode())
	enc = rsa.encrypt(data.encode())
	output_file.write(enc)
	output_file.close()

# TODO : check if file exist
if __name__ == "__main__":
	if len(argv) != 4:
		print("Usage : %s keyfile datafile outputfile"%(argv[0]))
		exit(0);

	encrypt_data(argv[1], argv[2], argv[3])