#!/usr/bin/env python3

# Based on the code from https://github.com/f47h3r/basic-auth-simple-https-server,
# under the terms of the MIT.

import os
import sys
import ssl
import logging
import http.server
import json
import hashlib
from base64 import b64decode

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
logger = logging.getLogger()

statments_folder = "my_statments"


def read_data(filename, dataname):
    if not os.path.exists(filename):
        raise Exception("File %s doesn't exist"%(filename))
    with open(filename, 'r') as f:
        if dataname == "all":
            lines = f.readlines()
            s = "\n".join(lines[1:])
        else:
            lines = f.read().splitlines()
            idx = lines[0].split(',').index(dataname)
            s = ""
            for l in lines[1:]:
                s += l.split(',')[idx]
    # TODO : dirty replacment            
    s2 = s
    bad_chars = ['\n', ',', ' ']
    for i in bad_chars :
        s2 = s2.replace(i, '')
    # compute hash
    h = (hashlib.sha256(s2.encode('utf-8')).hexdigest())
    print("hash computed on data %s : %s"%(s2, h))

    return s, h

class BasicAuthHandler(http.server.SimpleHTTPRequestHandler):
    key = 'ZmxvOjE5ODg='

    def do_HEAD(self):
        '''Send Headers'''
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_AUTHHEAD(self):
        '''Send Basic Auth Headers'''
        self.send_response(401)
        self.send_header('WWW-Authenticate', 'Basic')
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_AUTH(self, type):
        retcode = 0
        if self.headers.get('Authorization') is None:
            # Send Auth Headers
            self.do_AUTHHEAD()
            logger.debug('Auth Header Not Found')
            self.wfile.write(bytes('Unauthorized', 'utf8'))
        elif self.headers.get('Authorization') == 'Basic ' + self.key:
            # Successful Auth
            if type == "GET":
                http.server.SimpleHTTPRequestHandler.do_GET(self)
            logger.debug("Client authenticated")
            retcode = 1
        else:
            # Bad Credentials Supplied
            self.do_AUTHHEAD()
            auth_header = self.headers.get('Authorization')
            # Log Bad Credentials
            if len(auth_header.split(' ')) > 1:
                logger.debug(auth_header.split(' ')[1])
                logger.debug(b64decode(auth_header.split(' ')[1]))
            logger.debug('Bad Creds')
            self.wfile.write(bytes('Unauthorized', 'utf8'))
        return retcode

    def do_POST(self):
        logging.debug("Handling of POST request :")
        print("Headers : ", self.headers)
        try:
            # if self.do_AUTH("POST"):
            if True:
                content_len = int(self.headers.get('content-length'))
                post_body = self.rfile.read(content_len)
                print("POST content len : ", content_len)
                print("POST body : ", post_body)
                params = json.loads(post_body.decode())
                print("POST body dict : ", params)
                filename = statments_folder + "/" + params["file"]
                logging.debug("Sending data %s from file %s"
                    %(params["data"], filename))
                data, hash_val = read_data(filename, params["data"])
                print("Data sent :\n", data)
                data_len = len(data.encode())
                print("Data length : ", data_len)
                # self.do_HEAD()
                self.send_header('Digest', str(hash_val))
                self.send_header('Content-Length', str(data_len))
                self.end_headers()
                self.wfile.write(data.encode())
        except Exception:
            logger.error("Error in POST Functionality", exc_info=True)

    def do_GET(self):
        '''Handle GET Request'''
        logging.debug("Handling of GET request :")
        try:
            self.do_AUTH("GET")
        except Exception:
            logger.error("Error in GET Functionality", exc_info=True)

    def date_time_string(self, time_fmt='%s'):
        return ''

    def log_message(self, format, *args):
        '''Requests Logging'''
        logger.debug("%s - - [%s] %s" % (
            self.client_address[0],
            self.log_date_time_string(),
            format % args))


if __name__ == '__main__':

    # Create Handler Instance
    handler = BasicAuthHandler
    handler.server_version = '1'
    handler.sys_version = '1'

    # SimpleHTTPServer Setup
    httpd = http.server.HTTPServer(('127.0.0.1', 4433), handler)
    httpd.socket = ssl.wrap_socket(httpd.socket,
        certfile='../Config/floca.crt',
        keyfile='../Config/floca.pem',
        server_side=True)
    try:
        if not os.path.isdir('./files'):
            os.makedirs('./files')
        os.chdir('./files')
        httpd.serve_forever()
    except Exception:
        logger.error("Fatal error in main loop", exc_info=True)
