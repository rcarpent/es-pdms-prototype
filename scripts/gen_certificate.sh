#!/bin/bash

if [ "$#" -ne 3 ]
then
	echo "Usage : $0 target_name ca_name sign_target_only(0 or 1)"
	exit 0
fi

target=$1
target_key=$target".pem"
target_csr=$target".csr"
target_crt=$target".crt"
ca=$2
ca_key=$ca".pem"
ca_crt=$ca".crt"
sign_target=$3


echo "Generate an RSA key for the target"
openssl genrsa -out $target_key 2048
echo "Generate a CSR"
openssl req -new -key $target_key -out $target_csr

if [ $sign_target == 0 ]
then
	echo "Generate a key for the CA"
	openssl genrsa -out $ca_key 2048
	echo "Generate a self signed certificate for the CA"
	openssl req -new -x509 -key $ca_key -out $ca_crt
fi

echo "Sign certificate of target"
openssl x509 -req -in $target_csr -CA $ca_crt -CAkey $ca_key -CAcreateserial -out $target_crt