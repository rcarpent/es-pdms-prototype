#! /usr/bin/env python3


# TODO : 
#     - fix indentation in generated code
#     - use variables for string replacement
#     - check consistency between out_nb_args and out_type
#     - check that number of arguments is not null

import json

root_dir = "./"
proto_file = "func_proto.json"
wrapper_cpp_file = root_dir + "Wrapper.cpp"
wrapper_header_file = root_dir + "Wrapper.h"
generated_cpp_code = '''#include "Wrapper.h"
#include "DataTaskCom.h"

void wrapper(unsigned int sock_id, char *fname, char *params)
{
'''

with open(proto_file, 'r') as json_file:
    data = json.load(json_file)

taskname = data["taskname"]
func_l = [(x["funcname"], x["arguments"], x["out_type"], x["out_nb_args"]) for x in  data["functions"]]
#print("functions : ", func_l)

# format into pointer/reference to match the tuple initialization
def is_pointer(varname, nb_args):
    if nb_args == 0:
        if "*" not in varname:
            return varname + " *"
        else:
            return varname
    elif nb_args == 1:
        return "&" + varname
    else:
        return varname

generated_header_code = '''#ifndef WRAPPER_H_
#define WRAPPER_H_

#include <string.h>
#include "{}"

void wrapper(unsigned int sock_id, char *fname, char *params);

#endif
'''.format(taskname + ".h")

for i in range(len(func_l)):
    #print("test : ", func_l[i][0])
    args_name = []
    args = []
    for j, arg in enumerate(func_l[i][1]):
        arg_name = ""
        if arg["nb_args"] > 1:
            arg_name = "arg" + str(j) + "[" + str(arg["nb_args"]) + "]"
        else:
            arg_name = "arg" + str(j)
        args_name.append("arg" + str(j))
        a = arg["type"] + " " + arg_name 
        args.append(a)
    # print("args_name : ", args_name)
    # print("args : ", args)
    s = '''\
    if (!strcmp(fname, "{}")) 
    {{
        //generate struct to cast parameters to past to the wrapper
        struct wrapper_st {{ {}; }};
        struct wrapper_st *wst = (struct wrapper_st *)params;
        {} res = {}({});

        //build control packet to submit
        char funcname[] = "{}";
        size_t size = sizeof({});
        unsigned int nb_func_args = {};
        unsigned int nb_elts = {};
        std::tuple<unsigned int, size_t, {}> t = {{nb_elts, size, {}}};
        std::vector<size_t> sizes = {{size * nb_elts}};

        ctrl_pkt *pkt = new_ctrl_pkt(nb_func_args, funcname, sizes, t);

        //submit result to socket
        submit_res(sock_id, pkt);
    }}
    '''.format(func_l[i][0], '; '.join(args), func_l[i][2], func_l[i][0], ', '.join(["wst->" + s for s in args_name]),\
        func_l[i][0], func_l[i][2].replace('*', ''), 1, func_l[i][3], is_pointer(func_l[i][2], 0), is_pointer("res", func_l[i][3]))
    generated_cpp_code += s
    #print(s)
generated_cpp_code += "}"

#print(generated_cpp_code)

# for func in data["functions"]:
#   print("funcname : {}, arguments : {}".format(func["funcname"], func["arguments"]))
    # func_l.append(func["funcname"])
    #print("arguments")

with open(wrapper_cpp_file, 'w+') as wrapper_c_code:
    wrapper_c_code.write(generated_cpp_code)
with open(wrapper_header_file, 'w') as wrapper_h_code:
    wrapper_h_code.write(generated_header_code)
