# Copyright (c) Open Enclave SDK contributors.
# Licensed under the MIT License.

# -------------------------------------------------------------------
#  Function : parent-dir
#  Arguments: 1: path
#  Returns  : Parent dir or path of $1, with final separator removed.
# -------------------------------------------------------------------
parent-dir = $(patsubst %/,%,$(dir $(1:%/=%)))

# ------------------------------------------------------------------
#  Macro    : my-dir
#  Returns  : the directory of the current Makefile
#  Usage    : $(my-dir)
# ------------------------------------------------------------------
my-dir = $(realpath $(call parent-dir,$(lastword $(MAKEFILE_LIST))))

ROOT_DIR := $(call my-dir)
ifneq ($(words $(subst :, ,$(ROOT_DIR))), 1)
  $(error main directory cannot contain spaces nor colons)
endif

#-------------------------------------------------------------------
# This is the output folder.
#-------------------------------------------------------------------
BIN_DIR := bin
LIB_DIR := lib
TOPDIR = $(ROOT_DIR)
OUTDIR := $(BIN_DIR)

# ----------------------------------------------------------------
# Perform common configuration for building sample enclaves and hosts.
# ----------------------------------------------------------------

# Detect compiler.
ifneq ($(CC),cc)
        # CC explicitly specified.
else ifneq ($(shell $(CC) --version | grep clang),)
        # CC is default (cc), and aliases to clang.
else
        # CC is default (cc), and does not alias to clang.
        CLANG_VERSION = $(shell for v in "9" "8" "7"; do \
                                        if [ -n "$$(command -v clang-$$v)" ]; then \
                                                echo $$v; \
                                                break; \
                                        fi; \
                                done)

        ifneq ($(CLANG_VERSION),)
                CC = clang-$(CLANG_VERSION)
                CXX = clang++-$(CLANG_VERSION)
        endif
endif

# Choose the right pkg-config based on CC.
C_COMPILER = clang
CXX_COMPILER = clang++
ifeq ($(shell $(CC) --version | grep clang),)
        C_COMPILER = gcc
        CXX_COMPILER = g++
endif

# Define COMPILER for samples that use only C.
COMPILER = $(C_COMPILER)

# Define common compile flags used for GCC and G++ 
ifeq ($(COMPILER),gcc) 

	COMMON_FLAGS = -ffunction-sections -fdata-sections

	COMMON_FLAGS += -g -Wall -Wextra -Wchar-subscripts -Wno-coverage-mismatch -Winit-self \
			-Wpointer-arith -Wreturn-type -Waddress -Wsequence-point -Wformat-security \
			-Wmissing-include-dirs -Wfloat-equal -Wundef -Wshadow \
			-Wcast-align -Wconversion -Wredundant-decls

	CFLAGS = $(COMMON_FLAGS)
	CXXFLAGS = $(COMMON_FLAGS)

	# additional warnings flags for C
	CFLAGS += -Wjump-misses-init -Wstrict-prototypes -Wunsuffixed-float-constants

	# additional warnings flags for C++
	CXXFLAGS += -Wnon-virtual-dtor -std=c++11

	#  common link options
	COMMON_LDFLAGS := -Wl,-z,relro,-z,now,-z,noexecstack

# Define common compile flags used for Clang
else 
	COMMON_FLAGS += -g -Wall -Wextra

	CFLAGS = $(COMMON_FLAGS)
	CXXFLAGS = $(COMMON_FLAGS)

	# additional warnings flags for C
	# CFLAGS +=

	# additional warnings flags for C++
	CXXFLAGS += -Wnon-virtual-dtor -std=c++17

	#  common link options

endif
