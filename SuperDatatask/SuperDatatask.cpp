#include "SuperDatatask.h"
#include "CommonEnclave.h"


funcres_t *func1db(unsigned int nb_columns, unsigned int nb_rows, int *a, int *b, int *c, int* d)
{
    PRINT(LOG_LEVEL_DESCRIBE, "SuperDatatask : in function func1db\n");
    PRINT(LOG_LEVEL_DESCRIBE, "nb_columns = %d ; nb_rows = %d\n", nb_columns, nb_rows);
    PRINT(LOG_LEVEL_DESCRIBE, "Arguments : %d, %d, %d, %d\n", a[0], a[1], b[0], b[1]);
    PRINT(LOG_LEVEL_DESCRIBE, "Processing the sum of the two first tuple : %d and %d\n", b[0], b[1]);
    funcres_t *res = (funcres_t *)malloc(sizeof(funcres_t));
    oe_assert(res != NULL);
    res->nb_columns = 1;
    res->nb_rows = 1;
    res->a = (int *)malloc(sizeof(int));
    oe_assert(res->a != NULL);
    res->a[0] = b[0] + b[1];
    PRINT(LOG_LEVEL_DESCRIBE, "Result of func1db to submit : [%d]\n", res->a[0]);

    return res;
}

average_res_t *average_column(unsigned int nb_columns, unsigned int nb_rows, int *a)
{
    PRINT(LOG_LEVEL_DESCRIBE, "SuperDatatask : in function average_column\n");
    PRINT(LOG_LEVEL_DESCRIBE, "nb_columns = %d ; nb_rows = %d\n", nb_columns, nb_rows);
    int64_t sum = 0;
    for(int i = 0 ; i < nb_rows ; i++)
    {
        sum += a[i];
    }
    average_res_t *res = (average_res_t *)malloc(sizeof(average_res_t));
    oe_assert(res != NULL);
    res->nb_columns = 1;
    res->nb_rows = 1;
    res->a = (double *)malloc(sizeof(double));
    oe_assert(res->a != NULL);
    res->a[0] = sum/nb_rows;
    PRINT(LOG_LEVEL_DESCRIBE, "Average = %lf\n", res->a[0]);
    return res;
}

sum_electric_conso_res_t *sum_electric_conso(unsigned int nb_columns, unsigned int nb_rows, int* timestamp, double* consumption)
{
    PRINT(LOG_LEVEL_DESCRIBE, "SuperDatatask : in function sum_electric_conso\n");
    PRINT(LOG_LEVEL_DESCRIBE, "nb_columns = %d ; nb_rows = %d\n", nb_columns, nb_rows);
    double sum = 0;
    for(int i = 0 ; i < nb_rows ; i++)
    {
        sum += consumption[i];
    }

    sum_electric_conso_res_t *res = (sum_electric_conso_res_t *)malloc(sizeof(sum_electric_conso_res_t));
    oe_assert(res != NULL);
    res->nb_columns = 1;
    res->nb_rows = 1;
    res->a = (double *)malloc(sizeof(double));
    oe_assert(res->a != NULL);
    res->a[0] = sum;
    PRINT(LOG_LEVEL_DESCRIBE, "Sum = %lf\n", res->a[0]);
    return res;
}

sum_distance_res_t *sum_distance(unsigned int nb_columns, unsigned int nb_rows, int* id, int* timestamp, double* latitude, double* longitude, int* altitude)
{
    double sum = 0;
    double currentLatitude = latitude[0], currentLongitude = longitude[0];

    for(unsigned int rowNumber = 1 ; rowNumber < nb_rows ; rowNumber++)
    {
        double nextLatitude = latitude[rowNumber], nextLongitude = longitude[rowNumber];

        sum += sqrt(pow(currentLatitude-nextLatitude, (double)2.0) + pow(currentLongitude-nextLongitude, (double)2.0));
        currentLatitude = nextLatitude;
        currentLongitude = nextLongitude;
    }

    sum_distance_res_t *res = (sum_distance_res_t *)malloc(sizeof(sum_distance_res_t));
    oe_assert(res != NULL);
    res->nb_columns = 1;
    res->nb_rows = 1;
    res->a = (double *)malloc(sizeof(double));
    oe_assert(res->a != NULL);
    res->a[0] = sum;
    PRINT(LOG_LEVEL_DESCRIBE, "Sum = %lf\n", res->a[0]);
    return res;
}

int trusted_main(unsigned int sock_id)
{
    PRINT(LOG_LEVEL_DESCRIBE, "Nothing to do in main of SuperDatatask\n");
    return 1;
}