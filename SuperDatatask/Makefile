# Copyright (c) Open Enclave SDK contributors.
# Licensed under the MIT License.

include ../buildenv.mk

CFLAGS += $(shell pkg-config oeenclave-$(C_COMPILER) --cflags)
CXXFLAGS += $(shell pkg-config oeenclave-$(CXX_COMPILER) --cflags)
LDFLAGS += $(shell pkg-config oeenclave-$(CXX_COMPILER) --libs)
INCDIR = $(shell pkg-config oeenclave-$(COMPILER) --variable=includedir)
OE_CRYPTO_LIB =  -lmbedx509 -lmbedtls -lmbedcrypto -loecryptombedtls
OE_NET_LIB = -loehostsock -loehostresolver
OE_CRYPTO_LIB = mbedtls
CRYPTO_LDFLAGS = $(shell pkg-config oeenclave-$(COMPILER) --variable=${OE_CRYPTO_LIB}libs)

ENCLAVE_NAME = SuperDatatask
ENCLAVE_LIBNAME = $(ENCLAVE_NAME).so
SIGNED_ENCLAVE = $(TOPDIR)/$(LIB_DIR)/$(ENCLAVE_LIBNAME).signed
ENCLAVE_TRUSTED_C = $(ENCLAVE_NAME)_t.c
ENCLAVE_TRUSTED_HEADER = $(ENCLAVE_NAME)_t.h
ENCLAVE_SRC_CPP = $(ENCLAVE_NAME).cpp
ENCLAVE_SRC_DEPENDS = $(patsubst %.cpp,%.d,$(ENCLAVE_SRC_CPP))
ENCLAVE_EDL = $(ENCLAVE_NAME).edl
ENCLAVE_CONF = $(ENCLAVE_NAME).conf
ENCLAVE_OBJ = $(ENCLAVE_NAME).o
ENCLAVE_TRUSTED_OBJ = $(ENCLAVE_NAME)_t.o
INCLUDES = -I. -I../SecureCom -I../SecureCom/DataTaskCom -I../Attestation
COMMON_COM_SRC_CPP = $(wildcard ../SecureCom/*.cpp)
DATA_TASK_COM_SRC_CPP = ../SecureCom/DataTaskCom/DataTaskCom.cpp
SECURE_COM_SRC_CPP = $(COMMON_COM_SRC_CPP) $(DATA_TASK_COM_SRC_CPP)
ATTESTATION_SRC_CPP = $(wildcard ../Attestation/*.cpp)
COMMON_COM_OBJ = $(subst ../SecureCom/,,$(COMMON_COM_SRC_CPP:.cpp=.o))
DATA_TASK_COM_OBJ = $(subst ../SecureCom/DataTaskCom/,,$(DATA_TASK_COM_SRC_CPP:.cpp=.o))
SECURE_COM_OBJ = $(COMMON_COM_OBJ) $(DATA_TASK_COM_OBJ)
SECURE_COM_SRC_DEPENDS = $(patsubst %.o,%.d,$(SECURE_COM_OBJ))
ATTESTATION_OBJ = $(subst ../Attestation/,,$(ATTESTATION_SRC_CPP:.cpp=.o))
ATTESTATION_SRC_DEPENDS = $(patsubst %.o,%.d,$(ATTESTATION_OBJ))
WRAPPER_OBJ = Wrapper.o

.PHONY: all
all: sub_make_target = all
all: ulib $(SIGNED_ENCLAVE) exportkey

.PHONY: benchmark
benchmark: CXXFLAGS += -O2
benchmark: CFLAGS += -O2
benchmark: sub_make_target = benchmark
benchmark: ulib $(SIGNED_ENCLAVE) exportkey

$(SIGNED_ENCLAVE): $(ENCLAVE_LIBNAME) $(ENCLAVE_CONF) private.pem
	oesign sign -e $(ENCLAVE_LIBNAME) -c $(ENCLAVE_CONF) -k private.pem -o $@

$(ENCLAVE_LIBNAME): $(WRAPPER_OBJ) $(SECURE_COM_OBJ) $(ATTESTATION_OBJ) $(ENCLAVE_OBJ) $(ENCLAVE_TRUSTED_OBJ)
	@echo "Compilers used: $(CC), $(CXX)"
	$(CXX) -o $(ENCLAVE_LIBNAME) $(SECURE_COM_OBJ) $(ATTESTATION_OBJ) $(WRAPPER_OBJ) $(ENCLAVE_OBJ) $(ENCLAVE_TRUSTED_OBJ) $(LDFLAGS) $(CRYPTO_LDFLAGS)

-include $(ENCLAVE_SRC_DEPENDS) $(SECURE_COM_SRC_DEPENDS) $(ATTESTATION_SRC_DEPENDS)


$(ENCLAVE_OBJ) $(WRAPPER_OBJ):  %.o: %.cpp $(ENCLAVE_TRUSTED_HEADER)
$(COMMON_COM_OBJ): %.o: ../SecureCom/%.cpp $(ENCLAVE_TRUSTED_HEADER)
$(DATA_TASK_COM_OBJ): %.o: ../SecureCom/DataTaskCom/%.cpp $(ENCLAVE_TRUSTED_HEADER)
$(ATTESTATION_OBJ): %.o: ../Attestation/%.cpp $(ENCLAVE_TRUSTED_HEADER)

$(ENCLAVE_OBJ) $(WRAPPER_OBJ) $(SECURE_COM_OBJ) $(ATTESTATION_OBJ):
	$(CXX) -o $@ -c $(CXXFLAGS) $(INCLUDES) -MMD -MP -DOE_API_VERSION=2 -include $(ENCLAVE_TRUSTED_HEADER) $<



$(ENCLAVE_TRUSTED_OBJ): %.o: %.c
	$(CC) -c $(CFLAGS) $(INCLUDES) -DOE_API_VERSION=2 $<

$(ENCLAVE_TRUSTED_HEADER) $(ENCLAVE_TRUSTED_C) &: $(ENCLAVE_EDL) $(PDMS_ROOT_FOLDER)/SecureCom/CommonEnclave.edl $(PDMS_ROOT_FOLDER)/SecureCom/DataTaskCom/DataTaskCom.edl ../Attestation/localattestation.edl
	oeedger8r $(ENCLAVE_EDL) --trusted --search-path $(INCDIR) --search-path $(INCDIR)/openenclave/edl/sgx --search-path $(PDMS_ROOT_FOLDER)/SecureCom --search-path $(PDMS_ROOT_FOLDER)/SecureCom/DataTaskCom --search-path $(PDMS_ROOT_FOLDER)/Attestation

Wrapper.cpp: ../scripts/func_proto_parser.py func_proto.json
	# generation of wrapper code
	python ../scripts/func_proto_parser.py

private.pem:
	openssl genrsa -out $@ -3 3072

public.pem: private.pem
	openssl rsa -in $< -out $@ -pubout

.PHONY: exportkey
exportkey: public.pem
	@cp $^ ../Pubkeys/$(ENCLAVE_NAME)_pubkey.pem

.PHONY: ulib
ulib: OcallLibrary
	# build untrusted library
	@$(MAKE) -f Makefile_ulib $(sub_make_target)

.PHONY: OcallLibrary
OcallLibrary:
	# build ocall library
	@cd OcallLibrary && $(MAKE) $(sub_make_target)

.PHONY: clean
clean:
	@rm -f *.o  *_t.c *_t.h *_u.c *_u.h *_args.h Wrapper.* $(ENCLAVE_LIBNAME) $(SIGNED_ENCLAVE) $(ENCLAVE_NAME)_t.* *.pem ../Pubkeys/$(ENCLAVE_NAME)_pubkey.pem
	@rm -f $(DATA_TASK_COM_OBJ) $(ENCLAVE_OBJ) $(ENCLAVE_TRUSTED_OBJ) $(SECURE_COM_OBJ)
	@rm -f $(ENCLAVE_SRC_DEPENDS) $(SECURE_COM_SRC_DEPENDS) $(ATTESTATION_SRC_DEPENDS)
	@cd OcallLibrary && $(MAKE) clean
	$(MAKE) -f Makefile_ulib clean
