#ifndef _SuperDatatask_H_
#define _SuperDatatask_H_
#include <math.h>

typedef struct {
	unsigned int nb_columns;
	unsigned int nb_rows;
	int *a;
} funcres_t;

funcres_t *func1db(unsigned int nb_columns, unsigned int nb_rows, int *a, int *b, int* c, int* d);

typedef struct {
	unsigned int nb_columns;
	unsigned int nb_rows;
	double *a;
} average_res_t;

average_res_t *average_column(unsigned int nb_columns, unsigned int nb_rows, int *a);

typedef struct {
	unsigned int nb_columns;
	unsigned int nb_rows;
	double *a;
} sum_electric_conso_res_t;

sum_electric_conso_res_t *sum_electric_conso(unsigned int nb_columns, unsigned int nb_rows, int* timestamp, double* consumption);

typedef struct {
	unsigned int nb_columns;
	unsigned int nb_rows;
	double *a;
} sum_distance_res_t;

sum_distance_res_t *sum_distance(unsigned int nb_columns, unsigned int nb_rows, int* id, int* timestamp, double* latitude, double* longitude, int* altitude);
#endif
