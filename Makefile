.SILENT:

RED='\033[1;31m'
NC='\033[0m'

all:
	@mkdir -p bin lib Pubkeys
	FAIL=0; for i in `cat compile_order.txt`; do if test -d $$i; then make -C $$i 2>&1 ; if test ! $$? -eq "0"; then echo "${RED}FAILED $$i ${NC}";FAIL=1; fi; fi; done; exit $$FAIL

benchmark:
	@mkdir -p bin lib Pubkeys
	FAIL=0; for i in `cat compile_order.txt`; do if test -d $$i; then make -C $$i benchmark 2>&1 ; if test ! $$? -eq "0"; then echo "${RED}FAILED $$i ${NC}";FAIL=1; fi; fi; done; exit $$FAIL

clean:
	FAIL=0; for i in `cat compile_order.txt`; do if test -d $$i; then make -C $$i clean 2>&1 ;  if test ! $$? -eq "0"; then echo "${RED}FAILED $$i ${NC}";FAIL=1; fi; fi; done; exit $$FAIL

run:
	@cd bin && ./engine

# install:
# 	if test -f install.sh; then ./install.sh; fi
# 	FAIL=0; for i in `cat compile_order.txt`; do if test -d $$i; then make -C $$i install 2>&1 ;  if test ! $$? -eq "0"; then echo "FAILED $$i";FAIL=1; fi; fi; done; exit $$FAIL

# uninstall:
# 	if test -f uninstall.sh; then ./uninstall.sh; fi
# 	FAIL=0; for i in `cat compile_order.txt`; do if test -d $$i; then make -C $$i uninstall 2>&1 ;  if test ! $$? -eq "0"; then echo "FAILED $$i";FAIL=1; fi; fi; done; exit $$FAIL

# unit_test:
# 	FAIL=0; for i in `cat compile_order.txt`; do if test -d $$i; then make -C $$i unit_test 2>&1 ;  if test ! $$? -eq "0"; then echo "FAILED $$i";FAIL=1;exit 1; fi; fi; done; exit $$FAIL
