#ifndef _LOCAL_ATTESTATION_H_
#define _LOCAL_ATTESTATION_H_

#include "dispatcher.h"
#include "../Include/log.h"
#include <map>
#include <utility>


extern std::map<unsigned int, ecall_dispatcher *> dispatchers;

int generate_encrypted_message(unsigned int target_id, uint8_t* enclave_secret_data, size_t enclave_secret_data_size, uint8_t** data, size_t* size);
int process_encrypted_msg(unsigned int target_id, uint8_t* encrypted_data, size_t encrypted_data_size, uint8_t** decrypted_data, size_t *decrypted_data_size);


#endif
