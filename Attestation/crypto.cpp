// Copyright (c) Open Enclave SDK contributors.
// Licensed under the MIT License.

#include "crypto.h"
#include <mbedtls/pk.h>
#include <mbedtls/rsa.h>
#include <openenclave/enclave.h>
#include <stdlib.h>
#include <string.h>

Crypto::Crypto()
{
    m_initialized = init_mbedtls();
}

Crypto::~Crypto()
{
    cleanup_mbedtls();
}

/**
 * init_mbedtls initializes the crypto module.
 * mbedtls initialization. Please refer to mbedtls documentation for detailed
 * information about the functions used.
 */
bool Crypto::init_mbedtls(void)
{
    bool ret = false;
    int res = -1;

    mbedtls_ctr_drbg_init(&m_ctr_drbg_contex);
    mbedtls_entropy_init(&m_entropy_context);
    mbedtls_pk_init(&m_pk_context);

    // Initialize entropy.
    res = mbedtls_ctr_drbg_seed(
        &m_ctr_drbg_contex,
        mbedtls_entropy_func,
        &m_entropy_context,
        nullptr,
        0);
    if (res != 0)
    {
        TRACE_ENCLAVE(LOG_LEVEL_ERRORS, "mbedtls_ctr_drbg_seed failed.");
        goto exit;
    }

    // Initialize RSA context.
    res = mbedtls_pk_setup(
        &m_pk_context, mbedtls_pk_info_from_type(MBEDTLS_PK_RSA));
    if (res != 0)
    {
        TRACE_ENCLAVE(LOG_LEVEL_ERRORS, "mbedtls_pk_setup failed (%d).", res);
        goto exit;
    }

    // Generate an ephemeral 2048-bit RSA key pair with
    // exponent 65537 for the enclave.
    res = mbedtls_rsa_gen_key(
        mbedtls_pk_rsa(m_pk_context),
        mbedtls_ctr_drbg_random,
        &m_ctr_drbg_contex,
        2048,
        65537);
    if (res != 0)
    {
        TRACE_ENCLAVE(LOG_LEVEL_ERRORS, "mbedtls_rsa_gen_key failed (%d)\n", res);
        goto exit;
    }

    // Write out the public key in PEM format for exchange with other enclaves.
    res = mbedtls_pk_write_pubkey_pem(
        &m_pk_context, m_public_key, sizeof(m_public_key));
    if (res != 0)
    {
        TRACE_ENCLAVE(LOG_LEVEL_ERRORS, "mbedtls_pk_write_pubkey_pem failed (%d)\n", res);
        goto exit;
    }


    // Initialize AES context for data exchanges
    mbedtls_gcm_init(&m_gcm_context);

    /* TODO Generate one key for each datatask */
    uint8_t aes_key[16];
    memset(aes_key, 4, sizeof(aes_key));
    res = mbedtls_gcm_setkey(&m_gcm_context, MBEDTLS_CIPHER_ID_AES, aes_key, 128);
    if (res != 0)
    {
        TRACE_ENCLAVE(LOG_LEVEL_ERRORS, "mbedtls_gcm_setkey failed (%d)\n", res);
        goto exit;
    }

    ret = true;
    TRACE_ENCLAVE(LOG_LEVEL_DESCRIBE, "mbedtls initialized.");
exit:
    return ret;
}

/**
 * mbedtls cleanup during shutdown.
 */
void Crypto::cleanup_mbedtls(void)
{
    mbedtls_pk_free(&m_pk_context);
    mbedtls_entropy_free(&m_entropy_context);
    mbedtls_ctr_drbg_free(&m_ctr_drbg_contex);
    mbedtls_gcm_free(&m_gcm_context);

    TRACE_ENCLAVE(LOG_LEVEL_DESCRIBE, "mbedtls cleaned up.");
}

/**
 * Get the public key for this enclave.
 */
void Crypto::retrieve_public_key(uint8_t pem_public_key[512])
{
    memcpy(pem_public_key, m_public_key, sizeof(m_public_key));
}

// Compute the sha256 hash of given data.
int Crypto::Sha256(const uint8_t* data, size_t data_size, uint8_t sha256[32])
{
    int ret = 0;
    mbedtls_sha256_context ctx;

    mbedtls_sha256_init(&ctx);

    ret = mbedtls_sha256_starts_ret(&ctx, 0);
    if (ret)
        goto exit;

    ret = mbedtls_sha256_update_ret(&ctx, data, data_size);
    if (ret)
        goto exit;

    ret = mbedtls_sha256_finish_ret(&ctx, sha256);
    if (ret)
        goto exit;

exit:
    mbedtls_sha256_free(&ctx);
    return ret;
}

/**
 * Encrypt encrypts the given data using the given public key.
 * Used to encrypt data using the public key of another enclave.
 */
bool Crypto::Encrypt(
    const uint8_t* data,
    size_t data_size,
    uint8_t** encrypted_data,
    size_t* encrypted_data_size)
{
    bool ret = false;
    int res = -1;
    int offset = 0;

    unsigned char* tag = (unsigned char*)malloc(TAG_SIZE);
    oe_assert(tag != NULL);
    unsigned char* iv = (unsigned char*)malloc(IV_SIZE);
    oe_assert(iv != NULL);
    size_t ciphertext_size = data_size;
    unsigned char* ciphertext = (unsigned char*)malloc(ciphertext_size);
    oe_assert(ciphertext != NULL);
    
    if (!m_initialized)
        goto exit;

    // Generate random IV
    res = mbedtls_ctr_drbg_random(&m_ctr_drbg_contex, iv, IV_SIZE);
    if (res != 0)
    {
        TRACE_ENCLAVE(LOG_LEVEL_ERRORS, "mbedtls_ctr_drbg_random failed with %d\n", res);
        goto exit;
    }

    // Encrypt the data.
    res = mbedtls_gcm_crypt_and_tag(
        &m_gcm_context,
        MBEDTLS_GCM_ENCRYPT,
        data_size,
        iv,
        IV_SIZE,
        NULL,
        0,
        data,
        ciphertext,
        TAG_SIZE,
        tag);

    if (res != 0)
    {
        TRACE_ENCLAVE(LOG_LEVEL_ERRORS, "mbedtls_gcm_crypt_and_tag failed with %d\n", res);
        goto exit;
    }

    *encrypted_data_size = TAG_SIZE + IV_SIZE + sizeof(ciphertext_size) + ciphertext_size;
    *encrypted_data = (uint8_t*)malloc(*encrypted_data_size);
    oe_assert(encrypted_data != NULL);
    // Copy tag, iv and ciphertext in encrypted_data buffer (in that order).
    memcpy(*encrypted_data, tag, TAG_SIZE);
    offset += TAG_SIZE;
    memcpy(*encrypted_data + offset, iv, IV_SIZE);
    offset += IV_SIZE;
    memcpy(*encrypted_data + offset, &ciphertext_size, sizeof(ciphertext_size));
    offset += sizeof(ciphertext_size);
    memcpy(*encrypted_data + offset, ciphertext, ciphertext_size);

    

    ret = true;
exit:
    free(tag);
    free(iv);
    free(ciphertext);
    return ret;
}

/**
 * decrypt the given data using current enclave's private key.
 * Used to receive encrypted data from another enclave.
 */
bool Crypto::decrypt(
    const uint8_t* encrypted_data,
    size_t encrypted_data_size,
    uint8_t** data,
    size_t* data_size)
{
    bool ret = false;
    int res = -1;
    const unsigned char* tag = encrypted_data;
    const unsigned char* iv = encrypted_data + TAG_SIZE;
    const size_t ciphertext_size = *((size_t*)(encrypted_data + TAG_SIZE + IV_SIZE));
    const unsigned char* ciphertext = encrypted_data + TAG_SIZE + IV_SIZE + sizeof(ciphertext_size);

    if (!m_initialized)
        goto exit;
    
    *data_size = ciphertext_size;
    *data = (uint8_t*)malloc(*data_size);
    oe_assert(data != NULL);
    res = mbedtls_gcm_auth_decrypt(
        &m_gcm_context,
        ciphertext_size,
        iv,
        IV_SIZE,
        NULL,
        0,
        tag,
        TAG_SIZE,
        ciphertext,
        *data);
    if (res != 0)
    {
        TRACE_ENCLAVE(LOG_LEVEL_ERRORS, "mbedtls_gcm_auth_decrypt failed with %d\n", res);
        goto exit;
    }

    ret = true;

exit:
    return ret;
}
