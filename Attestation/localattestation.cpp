// Copyright (c) Open Enclave SDK contributors.
// Licensed under the MIT License.
#include "localattestation.h"
#include <openenclave/enclave.h>
#include <string.h>


int get_enclave_format_settings(
    unsigned int target_id,
    uint8_t** format_settings,
    size_t* format_settings_size)
{
    PRINT(LOG_LEVEL_DEBUG, "Getting enclave format settings for target_id %d\n", target_id);
    return dispatchers[target_id]->get_enclave_format_settings(
        format_settings, format_settings_size);
}

/**
 * Return the public key of this enclave along with the enclave's report.
 * Another enclave can use the report to attest the enclave and verify
 * the integrity of the public key.
 */
int get_targeted_evidence_with_public_key(
    unsigned int target_id,
    uint8_t* target_info_buffer,
    size_t target_info_size,
    uint8_t** pem_key,
    size_t* pem_key_size,
    uint8_t** evidence_buffer,
    size_t* evidence_buffer_size)
{
    return dispatchers[target_id]->get_targeted_evidence_with_public_key(
        target_info_buffer,
        target_info_size,
        pem_key,
        pem_key_size,
        evidence_buffer,
        evidence_buffer_size);
}

// Attest and store the public key of another enclave.
int verify_evidence_and_set_public_key(
    unsigned int target_id,
    uint8_t* pem_key,
    size_t pem_key_size,
    uint8_t* evidence,
    size_t evidence_size)
{
    return dispatchers[target_id]->verify_evidence_and_set_public_key(
        pem_key, pem_key_size, evidence, evidence_size);
}

// Encrypt message for another enclave using the public key stored for it.
int generate_encrypted_message(unsigned int target_id, uint8_t* enclave_secret_data, size_t enclave_secret_data_size, uint8_t** data, size_t* size)
{
    return dispatchers[target_id]->generate_encrypted_message(enclave_secret_data, enclave_secret_data_size, data, size);
}

// Process encrypted message
int process_encrypted_msg(unsigned int target_id, uint8_t* encrypted_data, size_t encrypted_data_size, uint8_t** decrypted_data, size_t *decrypted_data_size)
{
    return dispatchers[target_id]->process_encrypted_msg(encrypted_data, encrypted_data_size, decrypted_data, decrypted_data_size);
}


