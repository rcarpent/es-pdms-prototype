#ifndef _ENCLAVE_H_
#define _ENCLAVE_H_

#include <stdlib.h>
#include <string>
#include <cstring>
#include "DatabaseWrapper.h"
#include "SecureDB.h"
#include "CommonEnclave.h"

const unsigned char core_private_key[] = 
"-----BEGIN RSA PRIVATE KEY-----\n"
"MIIEogIBAAKCAQEAnXOhe1o8E3/bY+Z+SrNjdESv+bqBi20Bj0i66/OFfYraidZE\n"
"n9Ds7bURu/oT8yKGQpkvXN/UPSqzwUd0LvGBd55fzxczw092BEKwTtpRCXhCcAD+\n"
"MTvzfs3u0mZ9++kiE2pAQL7x+PP5l61TLLAsQAhHt7VIULdP0eG9+7lA5q8Ypfdn\n"
"LeIrO/EgPj1AYlydBptrDUc1lpX6g56EYBA7P+1kFRjbdWMZkTCsI0DdiLcaWnkU\n"
"qVWf5pnlXxA6ziP8zatAzMWwgaMTmMzG6oEAxFIEwnNUjDDNfXtg35ClGLCWTKlE\n"
"29uMy670qbdYnss6z4gRBm6zWP63EHM5kSs23wIDAQABAoIBABQkdsQRBx53fNmK\n"
"4x0rVc3J+7k24tuzVj++TithO/KTkOGRR7YqRCUkxOAHXgX8pLBgaG65M62fiN9x\n"
"BKv8uVNUL22OylByGebxoRx1UWikudsaRWqHAN93+NVxxywy5ukU7qNZyhrPG18M\n"
"DVfqMb7/UC1wQ5AP42Y2Ote5G1RnXqGCwH4G2eXUQNDJD4F67BZnggSoz4e4hpLk\n"
"MzywoP5x1ggO0CsSfJmV8egTUDKCSUY9h+6XdxsEX0zimJXlbJGVJNPsp16bxL4Y\n"
"XXvpI9VenWDbudQcTKYemHtGHoiagm7LwGmuur1NoZ0xXqEXw9dWh/41lm9c55ql\n"
"v9WTAAECgYEAyXC4P8as34a6Swnk8zSdYNxbWFg5ksJ7NEt0w7y+LlSK8oFjqJQn\n"
"xREjajid8mgpcgdL3OFQH6edMwUh6EqXU/KED2R0C5tZCzeFzJFLQWVlvrQFIqWx\n"
"syAPfoJx3iChgCD/LIe+QtomIlDEiI6Yb9s9520Q1eUzFWMhpWsvyAECgYEAyBje\n"
"XCSKjMlNGmy3EjsbpSmm5l3CwpFqdb+molIabTEpJYy4Xg8TCOUalgHtafjnre1Z\n"
"u1HPliesv4D0/rZrXOq8AUCzf20IdvrFKKFQIxSWsY2p9dBTHD7Jun1oP64DBkC6\n"
"PcftRqUQq+j6wdH1FbrQC6ces0kwjHBUrbIb/t8CgYBLZ1pQ5TnI4lHRzP5a25Md\n"
"st6ujzfd69bamHQ44On/xb3NUQnRDMRmTCgJlu/o0XzSJlELeXvYnZGGi6FJpsJG\n"
"tj0g1eVqgtguDraTAMA+1onjfNd/RK7j8lhabtHY6DYMFE3wiU97DtW/eqRWyane\n"
"FR9zBayLpKQemWD63sigAQKBgCHVGpvHzelaDABgy194wQbfEQo4ZjrRrYd1PS6K\n"
"5U77FfL0ZSP6NoS1gBZHAyZ84GdnD1n9zjScRrgmjMYYf/S498rabr8n51t+sSFa\n"
"jRsxWv1wb4FRlQvcje1LUPobaQWAojzZ9gOQ203F2cg1wNPIv26I9kboJB5Ky/aG\n"
"e/dPAoGAP3sEI5vGg/0ctHIhCf6wqlPofe4Fo6foiTVNMG3GEdtJKYL7mS1UOxP8\n"
"4n5P+44YJGdhbLZ3aiqPRz4lh0P15g2mH00GLe0HNJ6Br/EffBsRzqKwyZTIHF4H\n"
"WrOEF/zQpWZ8Y7zzIBtSayISF5QPb6YQ9EVe4xAPEDK0dRAu5Ms=\n"
"-----END RSA PRIVATE KEY-----";


#endif
