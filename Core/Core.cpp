#include "Core.h"
#include "Core_t.h"
#include "SecureDB.h"
#include "DataSubscription.h"
#include "mbedtls/pk.h"
#include <mbedtls/ctr_drbg.h>
#include <mbedtls/entropy.h>
#include "fast_double_parser.h"

/* global variables */

//db
sqlite3 *db = nullptr;
const char *global_dbname = ":memory:";
char core_tablename[] = "CoreTable";
char core_table_format[] = "id int,data1 int,data2 decimal";
char *core_table_types[] = {"int", "int", "float"};
std::unordered_map<std::string, std::vector<db_types> > tables_schemes;
std::unordered_map<std::string, std::vector<std::string> > data_providers;
std::unordered_map<std::string, std::vector<std::string> > data_subscribers;
std::unordered_map<std::string, std::vector<std::pair <unsigned int, char *> > > subscribed_data;
std::unordered_map <std::string, std::pair<std::string, std::string> > credentials;
std::unordered_map<std::string, unsigned int> db_access_rights;
std::unordered_map<std::string, std::string> data_table_digests;

//encryption
mbedtls_pk_context *core_pk = nullptr;
mbedtls_ctr_drbg_context *core_ctr_drbg = nullptr;
mbedtls_entropy_context *entropy = nullptr;


/* tools */

std::vector<std::string> split_str(std::string s, char delim) {
    std::string line;
    std::vector<std::string> vec;
    std::stringstream ss(s);

    while(std::getline(ss, line, delim)) {
        vec.push_back(line);
    }
    return std::move(vec);
}

char ***split_and_convert_str(
    std::string sdata,
    int nb_rows,
    int nb_columns,
    char delim1,
    char delim2)
{
    int i, j;
    auto vec_rows = split_str(sdata, '\n');
    std::vector<std::vector<std::string> > vec_data;
    for (auto &vr : vec_rows)
        vec_data.push_back(split_str(vr, ','));

    char ***data = (char ***)malloc(sizeof(char **) * nb_rows);
    for (i = 0; i < nb_rows; i++)
    {
        data[i] = (char **)malloc(sizeof(char *) * nb_columns);
        for (j = 0; j < nb_columns; j++)
        {
            data[i][j] = strdup(vec_data[i][j].c_str());
            // debug
            // printf("data[%d][%d] : %s\n", i, j, data[i][j]);
        }
    }

    return data;
}


/* ocalls */

// TODO : add return code
void get_file_size(const char *filename, int *size)
{
    ocall_get_file_size(filename, size);
}

// TODO : add return code
void read_enc_data(const char *filename, char *data, int dsize)
{
    ocall_read_enc_data(filename, data, dsize);
}


/* scheduler */

// TODO : view should be created when a new data task is registered
// and a check should be done when the associated app try to 
// retrieve data from db while it is not yet registered
void launch_core_scheduler() 
{
    // TODO : schema should be generated during the loading
    // of the associated table in db
    set_table_scheme("DT1", {int_db, int_db, float_db});
    set_table_scheme("DT2", {int_db, int_db, float_db});

    std::string sql1 = "CREATE VIEW DT1 AS SELECT * FROM " +
        std::string(core_tablename) + " WHERE ID=1;";

    std::string sql2 = "CREATE VIEW DT2 AS SELECT * FROM " +
        std::string(core_tablename) + " WHERE ID=2;";

    execute_sql(sql1.c_str(), nullptr);
    execute_sql(sql2.c_str(), nullptr);

    std::vector<std::string> subscribers = {"DT2"};
    add_data_subscriber("DT1", subscribers);

    std::vector<std::string> providers = {"DT1"};
    add_data_provider("DT2", providers);
}


/* database */

int init_core_db()
{
    PRINT(LOG_LEVEL_DESCRIBE, "Initialize the core database\n");

    int res = opendb(global_dbname);

    if (!res)
    {
        PRINT(LOG_LEVEL_ERRORS, "Initialization of core database failed.\n");
    }
    else
    {
        PRINT(LOG_LEVEL_DESCRIBE, "Core database has been initialized\n");
    }

    return res;
}

// load a csv table into db
int load_table_db(
    char *tablename,
    char *format,
    char **types,
    char ***data,
    unsigned int size_tablename,
    unsigned int size_format,
    unsigned int nb_rows,
    unsigned int nb_columns)
{
    int res = 0;

    // debug
    // int i, j;
    // printf("tablename = %s, format = %s\n", tablename, format);
    // printf("\ttypes : ");
    // for (i = 0; i < nb_columns; i++)
    //     printf("%s ", types[i]);
    // printf("\n");

    // printf("Data to load in db : \n");
    // for (i = 0; i < nb_rows; i++)
    // {
    //     for (j = 0; j < nb_columns; j++)
    //         printf("\tdata[%d][%d] : %s\n", i, j, data[i][j]);
    // }

    if (db)
    {
        res = load_data_to_db(
                    db, tablename, format, types,
                    data, nb_rows, nb_columns);
        // temporary
        // launch_core_scheduler();
    }

    return res;
}

// load an encrypted csv file into db
int load_enc_table_db(const char *filename)
{
    int res = 0;
    int i, j;

    if (core_pk == nullptr)
    {
        PRINT(LOG_LEVEL_ERRORS, "Can't load encrypted table -> PK context has not been initialized\n");
        return 0;
    }

    int to_decrypt_len;
    get_file_size(filename, &to_decrypt_len);
    PRINT(LOG_LEVEL_DEBUG, "Size of data to decrypt : %d\n", to_decrypt_len);
    char *to_decrypt = (char *)malloc(sizeof(char) * to_decrypt_len);
    read_enc_data(filename, to_decrypt, to_decrypt_len);

    unsigned char result[MBEDTLS_MPI_MAX_SIZE];
    size_t olen = 0;

    if((res = mbedtls_pk_decrypt(
                core_pk, 
                reinterpret_cast<const unsigned char *>(to_decrypt),
                to_decrypt_len,
                result,
                &olen,
                sizeof(result),
                mbedtls_ctr_drbg_random,
                core_ctr_drbg)) != 0 )
    {
        PRINT(LOG_LEVEL_ERRORS, "Failed : mbedtls_pk_decrypt returned -0x%04x\n", -res);
    }
    PRINT(LOG_LEVEL_DEBUG, "Data decrypted :\n%s", result);

    std::string sdata = std::string(reinterpret_cast<char *>(result));
    std::string sformat = std::string(core_table_format);
    int nb_columns = 
        std::count(sformat.begin(), sformat.end(), ',') + 1;
    int nb_rows = std::count(sdata.begin(), sdata.end(), '\n');

    char ***data = 
        split_and_convert_str(sdata, nb_rows, nb_columns, '\n', ',');

    res = load_data_to_db(
            db, core_tablename, core_table_format, core_table_types,
            data, nb_rows, nb_columns);

    return res;
}


int trusted_main(unsigned int sock_id)
{
    int res = 0;

    // initialiaze decryption module
    core_pk = (mbedtls_pk_context *)malloc(sizeof(mbedtls_pk_context));
    core_ctr_drbg = 
    (mbedtls_ctr_drbg_context *)malloc(sizeof(mbedtls_ctr_drbg_context));
    mbedtls_entropy_context *core_entropy = 
    (mbedtls_entropy_context *)malloc(sizeof(mbedtls_entropy_context));

    mbedtls_pk_init(core_pk);
    mbedtls_ctr_drbg_init(core_ctr_drbg);
    mbedtls_entropy_init(core_entropy);

    if((res = mbedtls_pk_parse_key(
                core_pk,
                core_private_key,
                sizeof(core_private_key), NULL, 0)) != 0)
    {
        PRINT(LOG_LEVEL_ERRORS, "Failed : mbedtls_pk_parse_keyfile returned -0x%04x\n", -res);
        return res;
    }

    if((res = mbedtls_ctr_drbg_seed(
                core_ctr_drbg,
                mbedtls_entropy_func, 
                core_entropy,
                NULL,
                0)) != 0)
    {
        PRINT(LOG_LEVEL_ERRORS, "Failed : mbedtls_ctr_drbg_seed returned %d\n", res);
        return res;
    }

    // initialize database
    res = init_core_db();

    if (!res)
        PRINT(LOG_LEVEL_ERRORS, "Error during loading of Core Database -> can't launch the scheduler");
    // else
    //     launch_core_scheduler();

    // temporary initialization until integration of manifests

    // set tables schemes
    set_table_scheme(core_tablename, {int_db, int_db, float_db});
    //set_table_scheme("HouseholdPowerConsumption", {int_db, double_db});
    set_table_scheme("GeolifeTrajectoriesMerged", {int_db, int_db, double_db, double_db, int_db});

    // set db access rights : 0 for reading, 1 for writing
    db_access_rights["DC1"] = 1;
    // set credentials
    credentials["DC1"] = std::make_pair("flo", "1988");

    // set scheduling
    std::vector<std::string> subscribers = {"DT2"};
    add_data_subscriber("DT1", subscribers);
    std::vector<std::string> providers = {"DT1"};
    add_data_provider("DT2", providers);

    return res;
}

std::vector<std::string> convert_params(char* params, unsigned int size_params)
{
    std::string args(params, size_params);
    std::istringstream ss(args);
	std::string token;
	std::vector<std::string> vec;
	while(std::getline(ss, token, '|')) {
		vec.push_back(token);
	}
    return vec;
}

int fast_atoi( const char * str )
{
    int val = 0;
    while( *str ) {
        val = val*10 + (*str++ - '0');
    }
    return val;
}
double average_column(std::vector<std::string>& data)
{
    int sum = 0;
    for(auto& value: data)
    {
        sum += fast_atoi(value.c_str());
    }
    return sum/data.size();
}

double average_columns(std::vector<std::vector<std::string>>& data)
{
    int64_t sum = 0;
    int nb_values = 0;
    for(auto& column: data)
    {
        for(auto& value: column)
        {
            sum += stoi(value);
            nb_values += 1;
        }
    }
    return sum/nb_values;
}

double sum_electric_conso(std::vector<std::vector<std::string>>& data)
{
    double sum = 0;
    for(auto& value: data[1])
    {
        double x;
        fast_double_parser::parse_number(value.c_str(), &x);
        sum += x;
    }
    return sum;
}

double sum_distance(std::vector<std::vector<std::string>>& data, unsigned int nbRows)
{
    long double sum = 0;
    double currentLatitude, currentLongitude;
    fast_double_parser::parse_number(data[2][0].c_str(), &currentLatitude);
    fast_double_parser::parse_number(data[3][0].c_str(), &currentLongitude);

    for(unsigned int rowNumber = 1 ; rowNumber < nbRows ; rowNumber++)
    {
        double nextLatitude, nextLongitude;
        fast_double_parser::parse_number(data[2][rowNumber].c_str(), &nextLatitude);
        fast_double_parser::parse_number(data[3][rowNumber].c_str(), &nextLongitude);

        sum += sqrt(pow(currentLatitude-nextLatitude, (double)2.0) + pow(currentLongitude-nextLongitude, (double)2.0));
        currentLatitude = nextLatitude;
        currentLongitude = nextLongitude;
    }

    return sum;
}

DataDB* storedData = NULL;

int exec_sql_and_store(
    char *params,
    unsigned int size_params)
{
    // Convert parameters
    std::vector<std::string> args = convert_params(params, size_params); // [tablename, db_request_mode, db_param1, db_param2]
    std::string tablename = args.at(0);
    int db_request_mode = stoi(args.at(1));
    int db_param1 = stoi(args.at(2));
    int db_param2 = stoi(args.at(3));

    // Prepare SQL request
    char* sql_params = (char*)malloc(size_params);
    oe_assert(sql_params != NULL);
    memcpy(sql_params, &db_request_mode, sizeof(int));
    memcpy(sql_params + sizeof(int), &db_param1, sizeof(int));
    memcpy(sql_params + sizeof(int)*2, &db_param2, sizeof(int));
    const char* sql_req = convert_params_to_sql(sql_params, tablename.c_str());
    
    storedData = new DataDB();

    // Make sql request
    if(execute_sql(sql_req, storedData) != 0)
    {
        PRINT(LOG_LEVEL_ERRORS, "Error during core computation -> Cannot retrieve data from Database\n");
        return -1;
    }

    free(sql_params);
    return storedData->get_nb_rows();
}

int free_stored_sql()
{
    if (storedData != NULL)
    {
        PRINT(LOG_LEVEL_DESCRIBE, "Freeing %u rows in Core.\n", storedData->get_nb_rows());
        delete storedData;
        storedData = NULL;
    }
    return 1;
}

int compute_on_stored_data(
    char *params,
    unsigned int size_params)
{
    int return_value = 0;
    if (storedData == NULL)
    {
        PRINT(LOG_LEVEL_ERRORS, "No stored Data to compute on, aborting.\n");
        return -1;
    }
    std::vector<std::string> args = convert_params(params, size_params); // [func_name, func_param]
    std::string func_name = args.at(0);
    std::string func_param = args.at(1);

    std::vector<std::vector<std::string> >& data = storedData->get_db_data();

    if (func_name == "average_column")
    {
        //func_param is column number
        int column_number = stoi(func_param);
        double average = average_column(data[column_number]);
        PRINT(LOG_LEVEL_DEBUG, "Average = %lf\n", average);
        return_value = (int)average;
    }
    else if (func_name == "average_columns")
    {
	    //no params, compute average of all columns returned by SQL
        double average = average_columns(data);
        PRINT(LOG_LEVEL_DEBUG, "Average = %lf\n", average);
        return_value = (int)average;
    }
    else if (func_name == "sum_electric_conso")
    {
        return_value = (int)sum_electric_conso(data);
    }
    else if (func_name == "sum_distance")
    {
        int max_row = stoi(func_param);
        max_row = (max_row == -1 ? storedData->get_nb_rows() : max_row);
        return_value = (int)sum_distance(data, max_row);
    }
    else if (func_name == "do_nothing")
    {}
    else
    {
        PRINT(LOG_LEVEL_ERRORS, "Unknown func_name = %s\n", func_name.c_str());
    }
    return return_value;
}


int exec_core_computation(
    char *params,
    unsigned int size_params)
{
    int return_value = 0;
    // params = tablename|db_request_mode|db_param1|db_param2|func_name|func_param

    // split SQL params & func params
    int cpt_delim = 0;
    unsigned int size_dt_params = 0;
    for(unsigned int i = 0 ; i < size_params ; i++)
    {
        if (params[i] == '|')
        {
            cpt_delim++;
            if (cpt_delim == 4)
            {
                size_dt_params = i;
                break;
            }
        }
    }

    return_value = exec_sql_and_store(params, size_dt_params);
    if (return_value == -1)
        return return_value;

    
    return_value = compute_on_stored_data(params + size_dt_params + 1, size_params - size_dt_params - 1);

    free_stored_sql();
    return return_value;
}

int custom_data_load(
    char ***data,
    unsigned int nb_rows,
    unsigned int nb_columns)
{
    if (storedData != NULL)
    {
        PRINT(LOG_LEVEL_ERRORS, "Some Data is already loaded, aborting.\n");
        return -1;
    }

    storedData = new DataDB();

    for(unsigned int column = 0 ; column < nb_columns ; column++)
    {
        for(unsigned int row = 0 ; row < nb_rows ; row++)
        {
            std::string converted = std::string(data[row][column]);
            storedData->add_data(converted, column);
        }
    }

    PRINT(LOG_LEVEL_DESCRIBE, "Loaded %u tuples in Core. \n", storedData->get_nb_rows());

    return 0;
}