#ifndef OCALL_LIB_H_
#define OCALL_LIB_H_

#if defined(__cplusplus)
extern "C" {
#endif

void ocall_print_string(const char *str);
void ocall_print_error(const char *str);
void ocall_get_file_size(const char *filename, int *dsize);
void ocall_read_enc_data(const char *filename, char *data, int dsize);

#if defined(__cplusplus)
}
#endif

#endif
