// This is a real implementation of ocalls
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include "ocall_db.h"


int ocall_lstat(const char *path, struct stat* buf){
    PRINT(LOG_LEVEL_DEBUG, "Entering %s\n", __func__);
    return lstat(path, buf);
}

int ocall_stat(const char *path, struct stat* buf){
    PRINT(LOG_LEVEL_DEBUG, "Entering %s\n", __func__);
    return stat(path, buf);
}

int ocall_fstat(int fd, struct stat* buf){
    PRINT(LOG_LEVEL_DEBUG, "Entering %s\n", __func__);
    return fstat(fd, buf);
}

int ocall_ftruncate(int fd, off_t length){
    PRINT(LOG_LEVEL_DEBUG, "Entering %s\n", __func__);
    return ftruncate(fd, length);
}

char *ocall_getcwd(char *buf, size_t size){
    PRINT(LOG_LEVEL_DEBUG, "Entering %s\n", __func__);
    return getcwd(buf, size);
}

int ocall_getpid(void){
    PRINT(LOG_LEVEL_DEBUG, "Entering %s\n", __func__);
    return getpid();
}

int ocall_open64(const char *filename, int flags, mode_t mode){
    PRINT(LOG_LEVEL_DEBUG, "Entering %s\n", __func__);
    return open(filename, flags, mode); // redirect it to open() instead of open64()
}

off_t ocall_lseek64(int fd, off_t offset, int whence){
    PRINT(LOG_LEVEL_DEBUG, "Entering %s\n", __func__);
    return lseek(fd, offset, whence); // redirect it to lseek() instead of lseek64()
}

int ocall_read(int fd, void *buf, size_t count){
    PRINT(LOG_LEVEL_DEBUG, "Entering %s\n", __func__);
    return read(fd, buf, count);
}

int ocall_write(int fd, const void *buf, size_t count){
    PRINT(LOG_LEVEL_DEBUG, "Entering %s\n", __func__);
    return write(fd, buf, count);
}

int ocall_fcntl(int fd, int cmd, void* arg, size_t size){
    PRINT(LOG_LEVEL_DEBUG, "Entering %s\n", __func__);
    return fcntl(fd, cmd, arg);
}

int ocall_close(int fd){
    PRINT(LOG_LEVEL_DEBUG, "Entering %s\n", __func__);
    return close(fd);
}

int ocall_unlink(const char *pathname){
    PRINT(LOG_LEVEL_DEBUG, "Entering %s\n", __func__);
    return unlink(pathname);
}

int ocall_getuid(void){
    PRINT(LOG_LEVEL_DEBUG, "Entering %s\n", __func__);
    return getuid();
}

char *ocall_getenv(const char *name){
    PRINT(LOG_LEVEL_DEBUG, "Entering %s\n", __func__);
    return getenv(name);
}

int ocall_fsync(int fd){
    PRINT(LOG_LEVEL_DEBUG, "Entering %s\n", __func__);
    return fsync(fd);
}

time_t ocall_time(time_t *t){
    return time(t);
}