#ifndef OCALL_DB_H_
#define OCALL_DB_H_

#include <string>
#include <vector>
#include "../../Include/log.h"

#if defined(__cplusplus)
extern "C" {
#endif

int ocall_lstat(const char *path, struct stat* buf);
int ocall_stat(const char *path, struct stat* buf);
int ocall_fstat(int fd, struct stat* buf);
int ocall_ftruncate(int fd, off_t length);
char *ocall_getcwd(char *buf, size_t size);
int ocall_getpid(void);
int ocall_open64(const char *filename, int flags, mode_t mode);
off_t ocall_lseek64(int fd, off_t offset, int whence);
int ocall_read(int fd, void *buf, size_t count);
int ocall_write(int fd, const void *buf, size_t count);
int ocall_fcntl(int fd, int cmd, void* arg, size_t size);
int ocall_close(int fd);
int ocall_unlink(const char *pathname);
int ocall_getuid(void);
char *ocall_getenv(const char *name);
int ocall_fsync(int fd);
time_t ocall_time(time_t *t);


#if defined(__cplusplus)
}
#endif

#endif
