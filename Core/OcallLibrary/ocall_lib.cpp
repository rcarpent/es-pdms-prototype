#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>
#include <map>
#include <algorithm>
#include "ocall_lib.h"

void ocall_print_string(const char *str)
{
    //std::cout << str;
    std::cout << "\033[1;31m" << str << "\033[0m";
}


void ocall_print_error(const char *str)
{
    std::cerr << "\033[1;31m" << str << "\033[0m" << std::endl;
}

// TODO : add return code
void ocall_get_file_size(const char *filename, int *dsize)
{
    std::ifstream in(filename, std::ifstream::ate | std::ifstream::binary);
    int size = in.tellg();
    in.close();
    *dsize = size;
    std::cout << "Size of file " << filename << ": " << size << " bytes" << std::endl;
}

// TODO : add return code
void ocall_read_enc_data(const char *filename, char *data, int dsize)
{
    std::ifstream in(filename, std::ios::binary);
    in.read(data, dsize);
    in.close();
}