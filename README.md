# SGX-ES-PDMS

**SGX-ES-PDMS** is software implementation of an **E**xtensible and **S**ecure **P**ersonal **D**ata **M**anagement **S**ystem (**ES-PDMS**) using Intel **S**oftware **G**uard e**X**tensions, and developped with Open Enclave SDK.<br/>
This implementation is based on the journal "*Personal Data Management Systems: The security and functionality standpoint*", see References.

Contact : iulian.sandu-popa@uvsq.fr

This is an ongoing research project. Various features are under development and there are several known bugs. Performances are measured in the [benchmark](../benchmark) branch.

Gitbook available at [https://petrus.gitlabpages.inria.fr/sgx-es-pdms](https://petrus.gitlabpages.inria.fr/sgx-es-pdms).

## References

- **Information Systems**: Nicolas Anciaux, Philippe Bonnet, Luc Bouganim, Benjamin Nguyen, Philippe Pucheral, Iulian Sandu-Popa, Guillaume Scerri, "Personal Data Management Systems: The security and functionality standpoint". Information Systems, Elsevier, 2019, 80, pp.13-35. 10.1016/j.is.2018.09.002, [hal-01898705](https://hal.inria.fr/hal-01898705)

- **PVLDB Tutorial**: Nicolas Anciaux, Luc Bouganim, Philippe Pucheral, Iulian Sandu Popa, and Guillaume Scerri, "Personal Database Security and Trusted Execution Environments: A Tutorial at the Crossroads", Proceedings of the VLDB Endowment (PVLDB), VLDB Endowment, 2019, 10.14778/3352063.3352118, [hal-02269292](https://hal.inria.fr/hal-02269292)

- **EDBT demonstration**: 
Robin Carpentier, Floris Thiant, Iulian Sandu Popa, Nicolas Anciaux, Luc Bouganim. "An Extensive and Secure Personal Data Management System using SGX", 25th International Conference on Extending Database Technology, Mar 2022, Edinburgh, United Kingdom. [hal-03580286](https://hal.inria.fr/hal-03580286)

---

