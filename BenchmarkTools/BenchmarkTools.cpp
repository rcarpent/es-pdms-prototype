#include "BenchmarkTools.h"
#include <string>
#include <mutex>
std::mutex m;

void Timer::set_label(std::string label)
{
    this->label = label;
}

void Timer::start()
{
    this->epoch = Clock::now();
}

void Timer::lap()
{
    auto now = Clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(now - this->epoch).count();
    laps.push_back(duration);
}

std::vector<int64_t> Timer::get_laps()
{
    return laps;
}

Timer* TimerManager::create_new_timer()
{
    Timer* newTimer = new Timer();
    m.lock();
    timers.push_back(newTimer);
    m.unlock();
    return newTimer;
}

void TimerManager::create_shared_timer()
{
    sharedTimer = new Timer();
    m.lock();
    timers.push_back(sharedTimer);
    m.unlock();
}

void TimerManager::start_shared_timer()
{
    sharedTimer->start();
}

void TimerManager::lap_shared_timer()
{
    sharedTimer->lap();
}

void TimerManager::save_results()
{
    m.lock();
    std::ofstream outfile;
    outfile.open("results.csv", std::ios::trunc);

    bool mustContinue;
    int numlap = 0;
    do
    {
        mustContinue = false;
        for (Timer* t : timers)
        {
            auto laps = t->get_laps();
            if (laps.size() > numlap)
            {
                outfile << laps[numlap] << "|";
                mustContinue = true;
            }
            else
            {
                outfile << "|";
            }
        }
        outfile << std::endl;
        ++numlap;
    } while (mustContinue);
    
    outfile.close();
    for (Timer* t : timers)
    {
        delete t;
    }

    m.unlock();
}