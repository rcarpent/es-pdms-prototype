#include <chrono>
#include <vector>
#include <fstream>
#include <map>

using Clock = std::chrono::steady_clock;

class Timer
{
    private:
        std::chrono::time_point<Clock> epoch;
        std::vector<int64_t> laps;
        std::string label;
    public:
    Timer() {}
        void set_label(std::string label);
        void start();
        void lap();
        std::vector<int64_t> get_laps();
          
};

class TimerManager // Singleton
{
    private:
        std::vector<Timer*> timers;
        Timer* sharedTimer;
        TimerManager() {}
    public:
        static TimerManager& get_instance()
        {
            static TimerManager instance;
            return instance;
        }
        TimerManager(TimerManager const&) = delete;
        void operator=(TimerManager const&) = delete;
        Timer* create_new_timer();
        void create_shared_timer();
        void start_shared_timer();
        void lap_shared_timer();
        void save_results();
};