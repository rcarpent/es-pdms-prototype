#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "usage : $0 install_folder_path(absolute)"
    exit 1
fi

path=$1
last_char="${path: -1}"

if [[ "$last_char" == "/" ]]; then
    path="${path::-1}"
    echo "path = $path"
fi

echo -e "source /opt/openenclave/share/openenclave/openenclaverc
export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:$path/es-pdms/lib/
export PDMS_ROOT_FOLDER=$path/es-pdms
export PDMS_PUBKEYS_FOLDER=\$PDMS_ROOT_FOLDER/Pubkeys" > env.sh
