#include <string>
#include <iostream>
#include "CoreExchange.h"
#include "AcqServer.h"

using namespace std;


extern vector<CoreExchange *> cores;
extern int core_target_id;
extern map<string, pair<int, int>> dt_acqunits_ports;

// TODO : refactorize and unificate acqunit functions


void new_acqunit(int &port_acq, string dt_name) 
{
    // TODO : move in dedicated function
    // initialize acq server
    zmqpp::context context_acq;
    zmqpp::socket socket_acq(context_acq, zmqpp::socket_type::pull);
    const string endpoint = "tcp://*:*";
    socket_acq.bind(endpoint);
    string endpoint_infos;
    socket_acq.get(zmqpp::socket_option::last_endpoint, endpoint_infos);
    port_acq = std::stoi(endpoint_infos.substr(endpoint_infos.rfind(":") + 1));

    // connect to data task
    zmqpp::context context_dt;
    zmqpp::socket socket_dt(context_dt, zmqpp::socket_type::push);
    int port_dt = dt_acqunits_ports[dt_name].first;
    // debug
    //cout << "The port for data collector is : " << port;
    connect_to_server(socket_dt, port_dt);

    // listen for incoming messages
    while (1)
    {
        // relay data packet
        zmqpp::message message;
        socket_acq.receive(message);
        if(socket_dt.send(message))
        {
            socket_acq.close();
            socket_dt.close();
            break;
        }
    }
}

void new_dt_acqunit(int &port, string dt_name)
{
    // TODO : move in dedicated function
    // initialize acq server
    zmqpp::context context;
    zmqpp::socket socket(context, zmqpp::socket_type::pull);
    const string endpoint = "tcp://*:*";
    socket.bind(endpoint);
    string endpoint_infos;
    socket.get(zmqpp::socket_option::last_endpoint, endpoint_infos);
    port = std::stoi(endpoint_infos.substr(endpoint_infos.rfind(":") + 1));

     while (1)
    {
        // acquire incoming packet
        //acquire_pkt(context, socket, pkt);
        //temporary fix
        zmqpp::message message;
        socket.receive(message);
        const char *data = static_cast<const char *>(message.raw_data(1));

        //debug
        // ctrl_pkt_to_send *pkt = (ctrl_pkt_to_send *)data;
        // std::cout << "Data received : " << std::endl;
        // std::cout << "\tsize_fname : " << pkt->size_fname << std::endl;
        // std::cout << "\tsize_params : " << pkt->size_params << std::endl;
        // std::cout << "\tnb_params : " << pkt->nb_params << std::endl;
        // size_t size = sizeof(pkt->size_fname) + sizeof(pkt->size_params) + sizeof(pkt->nb_params);
        // char *params = (char *)malloc(pkt->size_fname + pkt->size_params);
        // memcpy(params, data + size, pkt->size_fname);
        // std::cout << "\tfname1: " << params << std::endl;
        // memcpy(params + pkt->size_fname, data + size + pkt->size_fname, pkt->size_params);
        // std::cout << "\tparams : " << ((int *)(params + pkt->size_fname))[0] << std::endl;

        // convert and store the control packet in circular buffer
        ctrl_pkt_to_send *pkt = (ctrl_pkt_to_send *)data;
        uint8_t *p = reinterpret_cast<uint8_t *>(pkt);
        size_t p_size = sizeof(ctrl_pkt_to_send) + pkt->size_fname + pkt->size_params;
        buf_elt *elt = new_buf_elt(p, p_size);
        unsigned int dt_id = dt_acqunits_ports[dt_name].second;
        cores[core_target_id]->push_pkt_to_buf(dt_id, elt);
    }
}
