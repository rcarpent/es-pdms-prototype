#include <string>
#include <iostream>
#include "CoreExchange.h"
#include "Engine.h"
#include "../BenchmarkTools/BenchmarkTools.h"


using namespace std;


vector <CoreExchange *> cores;
int core_target_id = 0;
vector<pthread_t> dt_acqunit_threads(MAX_NB_ENCLAVES);
extern map<string, pair<int, int>> dt_acqunits_ports;
extern vector<unsigned int> dt_ids;

enum config_status
{
    INIT = 0,
    READY = 1,
    CONFIGURED = 2 
};

config_status module_status = INIT;

int launch_control_module(int port) {
    // initialize acq server
    zmqpp::context context;
    zmqpp::socket socket(context, zmqpp::socket_type::reply);
    init_acq_server(socket, port);

    // listen for incoming messages
    while (1)
    {
        
        if (module_status == CONFIGURED)
            return 1;

        // acquire incoming packet
        ctrl_pkt pkt;
        acquire_pkt(socket, pkt);

        if (!pkt.cmd.compare("new_acqunit"))
        {
            int acqunit_port;
            string dt_name = pkt.arg1;
            PRINTCPP(LOG_LEVEL_DESCRIBE, "Request to start a new acqunit for data task " << dt_name << endl);
            // TODO : implement global structure to handle threads
            std::thread acqunit_th(new_acqunit, std::ref(acqunit_port), dt_name);
            acqunit_th.detach();
            // TODO : use mutex instead dummy sleep
            std::chrono::duration<int, std::milli> timespan(200);
            std::this_thread::sleep_for(timespan);

            PRINTCPP(LOG_LEVEL_DESCRIBE, "An acquisiton unit has been launched on port " << acqunit_port << " for data task " << pkt.arg2 << endl);
            
            // TODO : fix unnecessary int member of ctrl_pkt  
            ctrl_pkt reply = { "port", to_string(acqunit_port), "0"};
            zmqpp::message *reply_msg = pack_data(reply);
            socket.send(*reply_msg);
            delete(reply_msg);
        }

        // check the command type
        else if (!pkt.cmd.compare("cmd_init_core"))
        {
            string ename = pkt.arg1;
            int nb_cores = stoi(pkt.arg2); 
            PRINTCPP(LOG_LEVEL_DESCRIBE, "Initialize " << nb_cores << " core(s)" << endl);

            for (int i = 0; i < nb_cores; i++)
            {
                EnclaveHandler *ehandler = new EnclaveHandler(ename, NULL);
                CoreExchange *core = new CoreExchange(ehandler);
                core->init();
                cores.push_back(core);
            }
            module_status = READY;

            // TODO : to refactorize
            // reply to the app
            // TODO : fix unnecessary int member of ctrl_pkt
            ctrl_pkt reply = { "retcode", "1", "0"};
            zmqpp::message *reply_msg = pack_data(reply);
            socket.send(*reply_msg);
            delete(reply_msg);
        }

        else if (!pkt.cmd.compare("cmd_init_data_task") || !pkt.cmd.compare("cmd_init_data_collector"))
        {
            ctrl_pkt reply;
            string dt_type = pkt.cmd.substr(9).replace(4, 1, "");
            dt_type[0] = toupper(dt_type[0]);
            dt_type[4] = toupper(dt_type[4]);
            string dt_name = pkt.arg1;
            PRINTCPP(LOG_LEVEL_DESCRIBE, "Initialize " << dt_type << " " << dt_name << endl);
            int dt_id = get_available_id(dt_ids);

            if (dt_id < 0)
            {
                PRINTCPP(LOG_LEVEL_ERRORS, "Cant instantiate enclave : " << dt_name <<
                 "no id available" << endl);
                reply = { "retcode", "0", "0"};
            }

            else 
            {
                int res = cores[core_target_id]->init_data_task(dt_id, pkt.arg1, dt_type);

                if (res)
                {
                    int dt_acqunit_port;
                    std::thread dt_acqunit_th(new_dt_acqunit, std::ref(dt_acqunit_port), dt_name);
                    dt_acqunit_threads[dt_id] = dt_acqunit_th.native_handle();
                    dt_acqunit_th.detach();
                    // TODO : use mutex instead dummy sleep
                    std::chrono::duration<int, std::milli> timespan(200);
                    std::this_thread::sleep_for(timespan);
                    dt_acqunits_ports[dt_name] = make_pair(dt_acqunit_port, dt_id);
                    PRINTCPP(LOG_LEVEL_DESCRIBE, dt_type << " " << dt_name << " has been registered on port " << dt_acqunit_port << endl);

                    dt_ids[dt_id] = 1;
                    reply = { "retcode", "1", "0"};
                }
                else
                {
                    PRINTCPP(LOG_LEVEL_ERRORS, "Error during init_data_task" << endl);
                    reply = { "retcode", "0", "0"};
                }
            }

            // if (module_status == READY)
            //     module_status = CONFIGURED;

            // TODO : to refactorize
            // reply to the app
            // TODO : fix unnecessary int member of ctrl_pkt
            zmqpp::message *reply_msg = pack_data(reply);
            socket.send(*reply_msg);
            delete(reply_msg);
        }
        else if (!pkt.cmd.substr(0, 4).compare("cmd_"))
        {
            ctrl_pkt reply;
            PRINTCPP(LOG_LEVEL_DESCRIBE, "Command " << pkt.cmd << " has been received" << endl);
            string ename = pkt.arg1;
            string cmd = pkt.cmd;
            // temporary trick until handling of multiple cores
            int id;
            if (cmd.find("core") != string::npos)
                id = core_target_id;
            else
                id = get_id_by_name(dt_acqunits_ports, ename);

            if (id < 0)
            {
                PRINTCPP(LOG_LEVEL_ERRORS, "Command " << cmd <<
                 " can't be executed : no id for enclave " << ename << endl);
                reply = { "retcode", "0", "0"};
            }

            else
            {
                reply = { "retcode", "1", "0"};

                if (!cmd.compare("cmd_load_table_core")) 
                {
                    cores[core_target_id]->load_table_db(pkt.arg2.c_str());
                }
                else if (!cmd.compare("cmd_load_enc_table_core")) 
                {
                    cores[core_target_id]->load_enc_table_db(pkt.arg2.c_str());
                }
                else if (!cmd.compare("cmd_deinit_data_task") || 
                    !cmd.compare("cmd_deinit_data_collector"))
                {
                    cores[core_target_id]->deinit_data_task(id);
                    pthread_cancel(dt_acqunit_threads[id]);
                }

                else if (!cmd.compare("cmd_start_data_task") || 
                    !cmd.compare("cmd_start_data_collector"))
                {
                    cores[core_target_id]->start_data_task(id);
                }

                else if (!cmd.compare("cmd_stop_data_task") || 
                    !cmd.compare("cmd_stop_data_collector"))
                {
                    cores[core_target_id]->stop_data_task(id);
                }

                else if (!cmd.compare("cmd_compute_core"))
                {
                    cores[core_target_id]->exec_core_computation(pkt.arg2.c_str());
                }

                else if (!cmd.compare("cmd_exec_sql_core"))
                {
                    cores[core_target_id]->exec_sql_and_store(pkt.arg2.c_str());
                }

                else if (!cmd.compare("cmd_free_sql_core"))
                {
                    cores[core_target_id]->free_stored_sql();
                }

                else if (!cmd.compare("cmd_compute_stored_core"))
                {
                    cores[core_target_id]->compute_on_stored_data(pkt.arg2.c_str());
                }

                else if (!cmd.compare("cmd_custom_load_core"))
                {
                    cores[core_target_id]->custom_data_load(pkt.arg2);
                }

                else if (!cmd.compare("cmd_save_benchmark_results_core"))
                {
                    TimerManager::get_instance().save_results();
                }
                else
                {
                    PRINTCPP(LOG_LEVEL_ERRORS, "Unknow command" << endl);
                    reply = { "retcode", "0", "0"};
                }
            }

            zmqpp::message *reply_msg = pack_data(reply);
            socket.send(*reply_msg);
            delete(reply_msg);
        }
        // default reply for command received
        else
        {
            ctrl_pkt reply = { "retcode", "0", "0"};
            zmqpp::message *reply_msg = pack_data(reply);
            socket.send(*reply_msg);
            delete(reply_msg);
        }
    }
}
