#include <thread>
#include <iostream>
#include "CoreExchange.h"
#include "Engine.h"


int main()
{
	PRINTCPP(LOG_LEVEL_DESCRIBE, "Launching of ControlModule" << std::endl);
	std::thread ctrl_module_th(launch_control_module, CTRL_MODULE_PORT);
	ctrl_module_th.join();
	return 1;
}