#ifndef _ENGINE_H_
#define _ENGINE_H_

#include "AcqServer.h"


int launch_control_module(int port);
void new_acqunit(int &port, std::string dt_name);
void new_dt_acqunit(int &port, std::string dt_name);

#endif