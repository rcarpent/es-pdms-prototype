#include "App.h"
#include <zmqpp/zmqpp.hpp>
#include "AcqServer.h"
#include <cstdlib>
#include <random>
#include "ControlPacket.h"
#include <sstream>
#include <algorithm>


using namespace std;

//TODO : python wrapper + top level functions for user App

// deprecated
// string generate(int max_length){
//     string possible_characters = "abcdef";
//     random_device rd;
//     mt19937 engine(rd());
//     uniform_int_distribution<> dist(0, possible_characters.size()-1);
//     string ret = "";
//     for(int i = 0; i < max_length; i++){
//         int random_index = dist(engine); //get index between 0 and possible_characters.size()-1
//         ret += possible_characters[random_index];
//     }
//     return ret;
// }

int main(int argc, char *argv[])
{
    // temporary trick to avoid warning
    (void)argv;

    if (argc != 2)
    {
        PRINTCPP(LOG_LEVEL_ERRORS, "Missing the number of control packets to send" << endl);
        return 1;
    }
    
    int nb_ctrl_pkts = atoi(argv[1]);

    // register acqunit for this app
    zmqpp::context context;
    zmqpp::socket socket(context, zmqpp::socket_type::push);
    register_new_acqunit(socket, "../lib/DataTask.so.signed");

    // TODO : generate nb_ctrl_pkts random packets 
    for (int i = 0; i < nb_ctrl_pkts; i++)
    {
        // generate a control packet with random tuples
        char fname[] = "func3";
        unsigned int nb_func_args = 3;
        unsigned int v1_nb_elts = 2, v2_nb_elts = 3, v3_nb_elts = 4;
        auto v1 = build_random_t<double *>("double", v1_nb_elts, 1, 200);
        auto v2 = build_random_t<float *>("float", v2_nb_elts, 1, 200);
        auto v3 = build_random_t<int *>("int", v3_nb_elts, 1, 200);

        std::vector<size_t> sizes = {std::get<0>(v1) * std::get<1>(v1),\
             std::get<0>(v2) * std::get<1>(v2), std::get<0>(v3) * std::get<1>(v3)};

        ctrl_pkt_to_send *pkt = new_ctrl_pkt(nb_func_args, fname, sizes, v1, v2, v3);

        // TODO : debug cast to struct in enclave when uint8_t is past as argument -> a memory corruption seems present
        // char fname[] = "func4";
        // unsigned int nb_func_args = 4;
        // unsigned int v1_nb_elts = 2, v2_nb_elts = 3, v3_nb_elts = 4, v4_nb_elts = 5;
        // auto v1 = build_random_t<int *>("int", v1_nb_elts, 1, 200);
        // auto v2 = build_random_t<unsigned char *>("uchar", v2_nb_elts, 1, 255);
        // auto v3 = build_random_t<float *>("float", v3_nb_elts, 1, 200);
        // //auto v4 = build_random_t<double *>("double", v4_nb_elts, -100, 100);
        // auto v4 = build_random_t<int *>("int", v4_nb_elts, 1, 200);

        // std::vector<size_t> sizes = {std::get<0>(v1) * std::get<1>(v1),\
        //     std::get<0>(v2) * std::get<1>(v2), std::get<0>(v3) * std::get<1>(v3),\
        //     std::get<0>(v4) * std::get<1>(v4)};

        // ctrl_pkt_to_send *pkt = new_ctrl_pkt(nb_func_args, fname, sizes, v1,  v3, v4);

        //debug
        // std::cout << "size_fname : " << pkt->size_fname << std::endl;
        // std::cout << "size_params : " << pkt->size_params << std::endl;
        // std::cout << "nb_params : " << pkt->nb_params << std::endl;
        // std::cout << "params : " << ((int *)(pkt->params + pkt->size_fname))[0] << std::endl;

        // send the packet to acqunit
        // cout << "Sending control packed " << i + 1 << endl;
        //size_t size_pkt = sizeof(ctrl_pkt_to_send) + sizeof(char) * (size_fname + size);
        size_t size_pkt = sizeof(ctrl_pkt_to_send) + strlen(fname) + std::accumulate(sizes.begin(), sizes.end(), 0);
        zmqpp::message *msg = new zmqpp::message(size_pkt);
        msg->add_raw(reinterpret_cast<char *>(pkt), size_pkt);
        socket.send(*msg);
        delete msg;
    }

    PRINTCPP(LOG_LEVEL_DESCRIBE, "App successfully returned." << endl);
    return 0;
}

