#include "DatabaseWrapper.h"


const char *convert_params_to_sql(char *params, const char *tablename)
{
    int req_mode = ((int *)(params))[0];
    std::string sql;

    if (req_mode == 0) // get all data
    {
        sql = "SELECT * FROM " + std::string(tablename) + ";";
    }
    else if (req_mode == 1) // get data with id < parameter
    {
        int upperBoundID = ((int *)(params))[1];
        sql = "SELECT * FROM " + std::string(tablename) + " WHERE id < " + std::to_string(upperBoundID) + ";";
    }
    else if (req_mode == 2) // get 10% of tuples
    {
        sql = "SELECT * FROM " + std::string(tablename) + " WHERE col10 == 1;";
    }
    else if (req_mode == 3) // get 25% of tuples
    {
        sql = "SELECT * FROM " + std::string(tablename) + " WHERE col25 == 1;";
    }
    else if (req_mode == 4) // get 50% of tuples
    {
        sql = "SELECT * FROM " + std::string(tablename) + " WHERE col25 == 1 OR col25 == 2;";
    }
    else if (req_mode == 5) // get 75% of tuples
    {
        sql = "SELECT * FROM " + std::string(tablename) + " WHERE col25 == 1 OR col25 == 2 OR col25 == 3;";
    }
    else if (req_mode == 6) // get data with id < parameter1 in column named col{parameter2}
    {
	    int upperBoundID = ((int*)(params))[1];
	    int col_number = ((int*)(params))[2];
	    sql = "SELECT col" + std::to_string(col_number) + " FROM " + std::string(tablename) + " WHERE id < " + std::to_string(upperBoundID) + ";";
    }
    else if (req_mode == 7)
    {
	    int upperBoundID = ((int*)(params))[1];
        int col_number = ((int*)(params))[2];
	    sql = "SELECT AVG(col" + std::to_string(col_number) + ") FROM " + std::string(tablename) + " WHERE id < " + std::to_string(upperBoundID) + ";";
    }
    else if (req_mode == 8) // With LIMIT
    {
        int rowcount = ((int*)(params))[1];
        sql = "SELECT * FROM " + std::string(tablename) + " LIMIT " + std::to_string(rowcount) + ";";
    }
    else if (req_mode == 9) // Special timestamp for HouseholdPowerConsumption
    {
        unsigned long nb_rows = ((int*)(params))[1];
        unsigned long timestamp = (unsigned long)1166289840+(unsigned long)(60*nb_rows);
        sql = "SELECT * FROM " + std::string(tablename) + " WHERE timestamp < " + std::to_string(timestamp) + ";";
    }
    else {
        PRINT(LOG_LEVEL_ERRORS, "Unknown mode %d\n", req_mode);
        return NULL;
    }

    PRINT(LOG_LEVEL_DESCRIBE, "Sql request built : %s\n", sql.c_str());
    return strdup(sql.c_str());
}
