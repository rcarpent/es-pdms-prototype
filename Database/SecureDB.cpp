#include "SecureDB.h"
#include <iostream>
#include "CommonEnclave.h"


// TODO : catch errors + pass db, db_data and db_data_vect_size as argument

extern sqlite3 *db;


/* DataDB object to handle data and associated informations retrieved from database */

DataDB::DataDB()
{
    this->db_data.reserve(DEFAULT_NB_COLUMNS);
}

void DataDB::add_data(std::string d, unsigned int column)
{
    // debug
    // printf("Adding data %s at column %d\n", d.c_str(), column);
    if(column == db_data.size())
    {
        std::vector<std::string> newColumn = std::vector<std::string>();
        newColumn.reserve(DEFAULT_NB_ROWS);
        db_data.push_back(newColumn);
    }
    db_data.at(column).push_back(d);
}

void DataDB::display_db_data()
{
    PRINT(LOG_LEVEL_FULL, "Content of db_data :\n");
    for (unsigned int i = 0; i < get_nb_columns(); i++)
    {
        for (unsigned int j = 0; j < get_nb_rows(); j++)
        {
            PRINT(LOG_LEVEL_FULL, "\tcolumn %d - row %d : %s\n", i, j, (db_data[i][j]).c_str());
        }
    }
}


/* Data conversion */

void convert_db_data(std::string data, db_types type, void *result) 
{
    if (type == int_db)
    {
        auto val = std::stoi(data);
        memcpy(result, &val, sizeof(int));

    }
    else if (type == float_db) 
    {
        auto val = std::stof(data);
        memcpy(result, &val, sizeof(float));
    }
    else if (type == double_db)
    {
        auto val = std::stod(data);
        memcpy(result, &val, sizeof(double));
    }
    else
    {
        PRINT(LOG_LEVEL_ERRORS, "Unknow database type\n");
    }
}

std::vector<size_t> get_db_type_size(std::vector<db_types> db_t)
{
    size_t size = 0;
    std::vector<size_t> sizes;

    for (unsigned int i = 0; i < db_t.size(); i++)
    {

        if (db_t[i] == int_db)
        {
            size = sizeof(int);
        }
        else if (db_t[i] == float_db)
        {
            size = sizeof(float);
        }
        else if (db_t[i] == double_db)
        {
            size = sizeof(double);
        }
        else
        {
            PRINT(LOG_LEVEL_ERRORS, "Unknow data type for type %d\n", db_t[i]);
            return {};
        }
        sizes.push_back(size);
    }

    return sizes;
}


/* Database functions */

// SQLite callback function for printing results
static int callback(void *NotUsed, int argc, char **argv, char **azColName)
{
    int i;

    for(i = 0; i < argc; i++){
        std::string azColName_str = azColName[i];
        std::string argv_str = (argv[i] ? argv[i] : "NULL");
        // debug
        // printf("i = %d, %s = %s", i, azColName_str.c_str(), argv_str.c_str());
        // db_data[i].push_back(argv[i]);
        
        if (NotUsed == nullptr) {
            continue;
        }
        else
        {
            DataDB *d_info = static_cast<DataDB *>(NotUsed);
            // debug
            // printf("d_info : columns = %d, rows = %d\n",  d_info->get_nb_columns(), d_info->get_nb_rows());
            d_info->add_data(argv[i], i);
        }
    }

    return 0;
}

int execute_sql(const char *sql, DataDB *d_info)
{
    int rc;
    char *zErrMsg = 0;
    rc = sqlite3_exec(db, sql, callback, (void *)d_info, &zErrMsg);
    
    if (rc) {
        PRINT(LOG_LEVEL_ERRORS, "SQLite error: %s\n", sqlite3_errmsg(db));
    }

    return rc;
}

int opendb(const char *dbname)
{
    int rc;
    rc = sqlite3_open(dbname, &db);
    if (rc) {
        PRINT(LOG_LEVEL_ERRORS, "SQLite error - can't open database connection : %s\n", sqlite3_errmsg(db));
        return 0;
    }

    PRINT(LOG_LEVEL_DESCRIBE, "Enclave: Created database connection to %s\n", dbname);
    return 1;
}

void closedb()
{
    sqlite3_close(db);
    PRINT(LOG_LEVEL_DESCRIBE, "Enclave: Closed database connection\n");
}

void set_table_scheme(std::string name, std::vector<db_types> scheme)
{
    tables_schemes[name] = scheme;
}

int call_bind(std::string type, sqlite3_stmt *stmt, int pos, std::string val)
{
    if (!type.compare("int"))
    {
        int x;
        try {
            x = stoi(val);
        } catch(...){
            PRINT(LOG_LEVEL_ERRORS, "Cannot convert %s to an integer\n", val.c_str())
            return -1;
        }
        return sqlite3_bind_int(stmt, pos, x);
    }
        
    else if(!type.compare("double") || !type.compare("float"))
    {
        double x;
        try {
            x = stod(val);
        } catch(...){
            PRINT(LOG_LEVEL_ERRORS, "Cannot convert %s to a double\n", val.c_str())
            return -1;
        }
        return sqlite3_bind_double(stmt, pos, x);
    }
    else if (!type.compare("text"))
        return sqlite3_bind_text(stmt, pos, val.c_str(), -1, 0);
    else
    {
        PRINT(LOG_LEVEL_ERRORS, "Unknown data type: Cannot bind to database format\n");
        return -1;
    }

    return 1;
}

int load_data_to_db(
    sqlite3 *database, 
    char *tablename,
    char *format,
    char **types,
    char ***data,
    int nb_rows,
    int nb_columns)
{
    sqlite3_stmt *stmt;
    int i, j, rc;
    char *err = 0;
    std::string tablename_str = std::string(tablename);
    std::string format_str = std::string(format);
    std::string sql_req1 = "CREATE TABLE IF NOT EXISTS " +
        tablename_str + " (" + format_str + ");";

    rc = sqlite3_exec(database, sql_req1.c_str(), 0, 0, &err);
    
    if (rc != SQLITE_OK) {
        PRINT(LOG_LEVEL_ERRORS, "SQL error : can't create %s : %s\n", tablename, err);
        sqlite3_free(err);
        return -1;
    }

    std::string bind_idx = "";
    for (int idx = 1; idx < nb_columns + 1; idx++)
    {
        bind_idx += "?" + std::to_string(idx);
        if (idx != nb_columns)
            bind_idx += " ,";
    }

    std::string format_idx = "(" + bind_idx + ")";
    PRINT(LOG_LEVEL_DEBUG, "Format to bind : %s\n", format_idx.c_str());

    std::string sql_req2 = "INSERT INTO " + tablename_str +
        " VALUES " + format_idx;
    rc = sqlite3_prepare_v2(database, sql_req2.c_str(), sql_req2.length(), &stmt, 0);
    rc = sqlite3_exec(database, "BEGIN TRANSACTION", 0, 0, 0);    

    PRINT(LOG_LEVEL_DEBUG, "Fill table %s with the format %s\n", tablename, format);

    for (i = 0; i < nb_rows; i++)
    {
        for (j = 0; j < nb_columns; j++)
        {
            //printf("Bind value %s of type %s\n", data[i][j], types[j]);
            call_bind(types[j], stmt, j + 1, data[i][j]);
        }

        if (sqlite3_step(stmt) != SQLITE_DONE)
        {
            PRINT(LOG_LEVEL_ERRORS, "Commit failed : %s\n", sqlite3_errmsg(database));
        }
 
        sqlite3_clear_bindings(stmt);
        sqlite3_reset(stmt);
    }

    rc = sqlite3_exec(database, "END TRANSACTION", 0, 0, &err);
    rc = sqlite3_finalize(stmt);

    PRINT(LOG_LEVEL_DESCRIBE, "Database has been filled with the table %s\n", tablename);

    return 1;
}

//TODO : handle type text
char ***convert_serial_data(int nb_rows, int nb_columns, char **types, char *sdata)
{
    int i, j;
    char ***data = (char ***)malloc(sizeof(char **) * nb_rows);
    std::ostringstream ss;

    for (i = 0; i < nb_rows; i++)
        data[i] = (char **)malloc(sizeof(char *) * nb_columns);

    for (i = 0; i < nb_columns; i++)
    {
        for (j = 0; j < nb_rows; j++)
        {
            ss.str("");
            ss.clear();

            if (!strcmp(types[i], "int"))
            {
                int offset = 0;

                if (i == 0)
                    offset = j * sizeof(int);
                else
                    offset = (nb_rows - 1) * i * sizeof(int) + sizeof(int) * j;

                auto d = ((int *)(sdata + offset))[i];
                ss << d;
                data[j][i] = strdup(ss.str().c_str());

                // debug
                // printf("Binding value %d / %s\n", d, ss.str().c_str());
                // printf("Binded value %s\n", data[j][i]);
            }

            if (!strcmp(types[i], "float"))
            {
                int offset = 0;

                if (i == 0)
                    offset = j * sizeof(float);
                else
                    offset = (nb_rows - 1) * i * sizeof(float) + sizeof(float) * j;

                auto d = ((float *)(sdata + offset))[i];
                ss << d;
                data[j][i] = strdup(ss.str().c_str());

                // debug
                // printf("Binding value %f / %s\n", d, ss.str().c_str());
                // printf("Binded value %s\n", data[j][i]);
            }

            if (!strcmp(types[i], "double"))
            {
                int offset = 0;

                if (i == 0)
                    offset = j * sizeof(float);
                else
                    offset = (nb_rows - 1) * i * sizeof(double) + sizeof(double) * j;
  
                auto d = ((double *)(sdata + offset))[i];
                ss << d;
                data[j][i] = strdup(ss.str().c_str());

                // debug
                // printf("Binding value %lf / %s\n", d, ss.str().c_str());
                // printf("Binded value %s\n", data[j][i]);
            }
        }
    }

    return data;
}