#ifndef SECURE_DB_H
#define SECURE_DB_H

#include <sqlite3.h>
#include <utility>
#include <string>
#include <vector>
#include <unordered_map>

#define DEFAULT_NB_COLUMNS 4
#define DEFAULT_NB_ROWS 10000


/* class to handle data retrieved from database */

class DataDB
{
	std::vector<std::vector<std::string> > db_data;

	public:
		DataDB();
		unsigned int get_nb_columns() { return this->db_data.size(); };
		unsigned int get_nb_rows() { return get_nb_columns() == 0 ? 0 : this->db_data.at(0).size(); };
		std::vector<std::vector<std::string> >& get_db_data() {return this->db_data; };
		void display_db_data();
		void add_data(std::string d, unsigned int column);
};


/* global variables */

enum db_types { int_db = 0, float_db = 1, double_db = 2};
extern std::unordered_map<std::string, std::vector<db_types> > tables_schemes;


/* prototypes of functions */

void convert_db_data(std::string data, db_types type, void *result);
std::vector<size_t> get_db_type_size(std::vector<db_types> db_t);
static int callback(void *NotUsed, int argc, char **argv, char **azColName);
int execute_sql(const char *sql, DataDB *d_info);
int opendb(const char *dbname);
void closedb();
void set_table_scheme(std::string name, std::vector<db_types> scheme);
int call_bind(std::string type, sqlite3_stmt *stmt, int pos, std::string val);
int load_data_to_db(
    sqlite3 *database, 
    char *tablename,
    char *format,
    char **types,
    char ***data,
    int nb_rows,
    int nb_columns);
char ***convert_serial_data(
	int nb_rows,
	int nb_columns,
	char **types,
	char *sdata);

#endif
