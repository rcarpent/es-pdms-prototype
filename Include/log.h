#pragma once
#include <stdio.h>

#define LOG_LEVEL_NOTHING 0
#define LOG_LEVEL_ERRORS 1
#define LOG_LEVEL_DESCRIBE 2
#define LOG_LEVEL_DEBUG 3
#define LOG_LEVEL_FULL 4

#define CURRENT_LOG_LEVEL LOG_LEVEL_DEBUG

#define PRINT(level, fmt, ...) \
            if (level <= CURRENT_LOG_LEVEL) printf(fmt, ##__VA_ARGS__);

extern const char* enclave_name;

#define TRACE_ENCLAVE(level, fmt, ...)     \
    if (level <= CURRENT_LOG_LEVEL) \
    printf(                         \
        "%s: ***%s(%d): " fmt "\n", \
        enclave_name,               \
        __FILE__,                   \
        __LINE__,                   \
        ##__VA_ARGS__)


#define PRINTCPP(level, stdstring) \
            if (level <= CURRENT_LOG_LEVEL) std::cout << stdstring;