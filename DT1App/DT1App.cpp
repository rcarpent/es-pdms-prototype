#include <zmqpp/zmqpp.hpp>
#include "AcqServer.h"
#include "ControlPacket.h"


int main(int argc, char *argv[])
{
    // register acqunit for this app
    zmqpp::context context;
    zmqpp::socket socket(context, zmqpp::socket_type::push);
    register_new_acqunit(socket, "../lib/DT1.so.signed");

    // initialize function arguments
    char fname1[] = "func1db";
    unsigned int nb_func1_args = 1;

    // build control packet
    // availables db request modes :
    //     - get all data : 0
    //     - get data1 : 1
    //     - get data2 : 2
	//     - get all data with a condition : 3
    auto v1 = build_random_t<int *>("int", 1, 0, 0); // db request mode -> use of vector only to be compatible with serialization
    ctrl_pkt_to_send *pkt1 = new_ctrl_pkt(nb_func1_args, fname1, {sizeof(int)}, v1);
    size_t size_pkt1 = sizeof(ctrl_pkt_to_send) + strlen(fname1) + sizeof(int);

    // send packet
    zmqpp::message *msg1 = new zmqpp::message(size_pkt1);
    msg1->add_raw(reinterpret_cast<char *>(pkt1), size_pkt1);
    socket.send(*msg1);
    delete msg1;

    char fname2[] = "func2db";
    unsigned int nb_func_args2 = 1;

    auto v2 = build_random_t<int *>("int", 1, 1, 1); // db request mode -> use of vector only to be compatible with serialization
    ctrl_pkt_to_send *pkt2 = new_ctrl_pkt(nb_func_args2, fname2, {sizeof(int)}, v2);
    size_t size_pkt2 = sizeof(ctrl_pkt_to_send) + strlen(fname2) + sizeof(int);

    // send packet
    // zmqpp::message *msg2 = new zmqpp::message(size_pkt2);
    // msg2->add_raw(reinterpret_cast<char *>(pkt2), size_pkt2);
    // socket.send(*msg2);
    // delete msg2;

    char fname3[] = "func3db";
    unsigned int nb_func_args3 = 2;

    auto v3 = build_random_t<int *>("int", 1, 2, 2); // db request mode -> use of vector only to be compatible with serialization
    auto v4 = build_random_t<int *>("int", 1, 8, 8);
    ctrl_pkt_to_send *pkt3 = new_ctrl_pkt(nb_func_args3, fname3, {sizeof(int), sizeof(int)}, v3, v4);
    size_t size_pkt3 = sizeof(ctrl_pkt_to_send) + strlen(fname3) + sizeof(int) + sizeof(int);

    // send packet
    // zmqpp::message *msg3 = new zmqpp::message(size_pkt3);
    // msg3->add_raw(reinterpret_cast<char *>(pkt3), size_pkt3);
    // socket.send(*msg3);
    // delete msg3;

	socket.close();

    return 0;
}

