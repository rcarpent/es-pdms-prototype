#!/bin/bash

echo "ES-PDMS software installation script for Ubuntu 18.04"

echo "1. Installation of the Open Enclave SDK"
echo "1.1 Configure the Intel and Microsoft APT Repositories"
echo 'deb [arch=amd64] https://download.01.org/intel-sgx/sgx_repo/ubuntu bionic main' | sudo tee /etc/apt/sources.list.d/intel-sgx.list
wget -qO - https://download.01.org/intel-sgx/sgx_repo/ubuntu/intel-sgx-deb.key | sudo apt-key add -

echo "deb http://apt.llvm.org/bionic/ llvm-toolchain-bionic-7 main" | sudo tee /etc/apt/sources.list.d/llvm-toolchain-bionic-7.list
wget -qO - https://apt.llvm.org/llvm-snapshot.gpg.key | sudo apt-key add -

echo "deb [arch=amd64] https://packages.microsoft.com/ubuntu/18.04/prod bionic main" | sudo tee /etc/apt/sources.list.d/msprod.list
wget -qO - https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -

#echo "1.2 Installation of the Intel SGX DCAP Driver"
#echo "This may not be the latest Intel SGX DCAP driver. Please check with Intel's SGX site if a more recent SGX DCAP driver exists."
sudo apt update
sudo apt -y install dkms
cd /tmp/
wget https://download.01.org/intel-sgx/latest/dcap-latest/linux/distro/ubuntu18.04-server/sgx_linux_x64_driver_1.36.2.bin -O sgx_linux_x64_driver.bin
chmod +x sgx_linux_x64_driver.bin
sudo ./sgx_linux_x64_driver.bin
sudo modprobe -i isgx

echo "1.3 Install the Intel and Open Enclave packages and dependencies"
sudo apt -y install clang-7 libssl-dev gdb libsgx-enclave-common libsgx-enclave-common-dev libprotobuf10 open-enclave=0.13.0

echo "2. Installation of dependencies for ES-PDMS software"
cd /tmp/
sudo apt -y install cmake
sudo apt -y install libboost-all-dev
git clone https://github.com/msgpack/msgpack-c.git
cd msgpack-c
git checkout cpp_master
cmake -DMSGPACK_CXX[17]=ON .
sudo make install
sudo apt -y install libzmqpp-dev libsodium-dev
